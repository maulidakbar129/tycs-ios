//
//  GameSDK.h
//  xgame
//
//  Created by tthw on 17/4/2.
//
//

#ifndef SystemTool_h
#define SystemTool_h

#import <UIKit/UIKit.h>


class SystemUtilsCocoCallback;

@interface SystemUtils_ios : NSObject
{
	SystemUtilsCocoCallback* _cocosCallback;
}

-(void) setViewController:(UIViewController*)viewController;

-(UIView*)getUIView;

-(UIViewController*)getUIViewController;

-(void)initSystemUtils_ios;

+(SystemUtils_ios *)shareInstance;

-(void) startVibrate;
-(void) stopVibrate;

-(NSString*) getSystemLanguage;
-(NSString*) getSystemLanguag;
-(NSString*) getSystemVersio;
-(NSString*) getSystemModel;
-(NSString*) getDeviceBrand;
-(NSString*) getIDFA;
-(NSString*) getPhoneUDID;
// 获取电量
-(float) getBatteryQuantity;
// 获取电池状态
-(int) getBatteryStauts;


// 获取屏幕亮度
-(float) getScreenBrightness;

// 设置屏幕亮度
-(void) setScreenBrightness:(float)brightness;

-(void)localPhoto;
-(void)takePhoto;

-(void)startLocation;

-(void)saveImageToPhotoWithPath:(NSString*)imagepath;
-(void)saveImageToPhoto:(UIImage *)image;

-(NSString*) getConfigValue:(NSString*)key;

-(void) copyToClipboard:(NSString*)text;

-(void) setCocos2dCallback:(SystemUtilsCocoCallback*)callback;

@end



#endif /* SystemTool */
