//
//  SystemUtilsIOS.cpp
//  quick_libs
//
//  Created by tthw on 17/10/26.
//  Copyright © 2017年 chukong. All rights reserved.
//

#include "SystemUtilsIOS.h"
#include "SystemUtils_ios.h"

std::string SystemUtilsIOS::getSystemLanguage()
{
    return std::string([[[SystemUtils_ios shareInstance] getSystemLanguage] UTF8String]);
}

std::string SystemUtilsIOS::getSystemVersion()
{
    return std::string([[[SystemUtils_ios shareInstance] getSystemVersion] UTF8String]);
}

std::string SystemUtilsIOS::getSystemModel()
{
    return std::string([[[SystemUtils_ios shareInstance] getSystemModel] UTF8String]);
} 

std::string SystemUtilsIOS::getDeviceBrand()
{
    return std::string([[[SystemUtils_ios shareInstance] getDeviceBrand] UTF8String]);
} 

std::string SystemUtilsIOS::getIDFA()
{
    return std::string([[[SystemUtils_ios shareInstance] getIDFA] UTF8String]);
}

std::string SystemUtilsIOS::getPhoneUDID()
{
    return std::string([[[SystemUtils_ios shareInstance] getPhoneUDID] UTF8String]);
}

void SystemUtilsIOS::stopVibrate()
{
    [[SystemUtils_ios shareInstance] stopVibrate];
}

void SystemUtilsIOS::startVibrate()
{
    [[SystemUtils_ios shareInstance] startVibrate];
}

float SystemUtilsIOS::getBatteryQuantity()
{
    return [[SystemUtils_ios shareInstance] getBatteryQuantity];
}


int SystemUtilsIOS::getBatteryStauts()
{
    return [[SystemUtils_ios shareInstance] getBatteryStauts];
}


float SystemUtilsIOS::getScreenBrightness()
{
    return [[SystemUtils_ios shareInstance] getScreenBrightness];
}

void SystemUtilsIOS::setScreenBrightness(float brightness)
{
    [[SystemUtils_ios shareInstance] setScreenBrightness:brightness];
}

void SystemUtilsIOS::resquestLocalPhoto()
{
    [[SystemUtils_ios shareInstance] localPhoto];
}

void SystemUtilsIOS::resquestTakePhoto()
{
    [[SystemUtils_ios shareInstance] takePhoto];
}

void SystemUtilsIOS::resquestStartLocation()
{  
    [[SystemUtils_ios shareInstance] startLocation];
}

void SystemUtilsIOS::resquestSaveImageToPhotoWithPath(const char* path)
{
    [[SystemUtils_ios shareInstance] saveImageToPhotoWithPath:[NSString stringWithCString:path encoding:NSUTF8StringEncoding]];
}


void SystemUtilsIOS::copyToClipboard(const char* text)
{
    [[SystemUtils_ios shareInstance] copyToClipboard:[NSString stringWithCString:text encoding:NSUTF8StringEncoding]];
}


