#include "HiGameSDK_Android.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include "json/rapidjson.h"
#include "json/writer.h"
#include "json/document.h"
#include "json/prettywriter.h"
#include "json/stringbuffer.h"
#include <stdlib.h>
#include<iostream>
#include<sstream>
#include "base/ccUTF8.h"

#include "HiGameSDK.h"
#include "CCLuaEngine.h"
#include "platform/android/jni/JniHelper.h"
#include <jni.h>
using namespace rapidjson;
using namespace cocos2d;

extern "C" {

#define sdkClassName "com/u8/sdk/cocos2d/U8JNIHelper"

JNIEXPORT void JNICALL Java_com_u8_sdk_cocos2d_U8JNIHelper_CocosCallbackHelper(JNIEnv *, jclass, jint jcallbckType, jint jresultCode, jstring jdata)
{

	std::string data = StringUtils::getStringUTFCharsJNI(JniHelper::getEnv(), jdata);
	int callbackType = jcallbckType;
	int resultCode = jresultCode;
	HiGameSdkAndroid::getInstance()->onCallback(callbackType, resultCode, data);
}



static HiGameSdkAndroid* sharedSDK = nullptr;
HiGameSdkAndroid* HiGameSdkAndroid::getInstance()
{
	if (!sharedSDK) {
		sharedSDK = new (std::nothrow) HiGameSdkAndroid();
	}
	return sharedSDK;
}

void HiGameSdkAndroid::destroyInstance()
{
    if(sharedSDK != nullptr)
    {
        CC_SAFE_DELETE(sharedSDK);
    }
}

HiGameSdkAndroid::HiGameSdkAndroid():
	_sdkCallback(nullptr)
{

}

HiGameSdkAndroid::~HiGameSdkAndroid()
{

}


void HiGameSdkAndroid::initSDK()
{
	CCLOG("HiGameSdkAndroid::initSDK() 1");
	JniHelper::callStaticVoidMethod(sdkClassName, "initSDK");
	CCLOG("HiGameSdkAndroid::initSDK() 2");

}

void HiGameSdkAndroid::setSDKCallback(SDKCallback _callback)
{
	_sdkCallback = _callback;
}

void HiGameSdkAndroid::onCallback(int callbackType, int resutlCode, std::string& data)
{
	if (_sdkCallback)
	{
		Scheduler *sched = Director::getInstance()->getScheduler();
		sched->performFunctionInCocosThread([=]() {
			std::string data1 = data;
			CCLOG("onCallback %d %d %s", callbackType, resutlCode, data.c_str());
			_sdkCallback(callbackType, resutlCode, data1);
		});
	}
}

void HiGameSdkAndroid::login(std::string& params)
{
	CCLOG("HiGameSdkAndroid::login() 1");
	JniHelper::callStaticVoidMethod(sdkClassName, "login", params);
	CCLOG("HiGameSdkAndroid::login() 2");
}

void HiGameSdkAndroid::logout(std::string& params)
{
	CCLOG("HiGameSdkAndroid::logout() 1");
	JniHelper::callStaticVoidMethod(sdkClassName, "logout", params);
	CCLOG("HiGameSdkAndroid::logout() 2");
}

void HiGameSdkAndroid::switchAccount(std::string& params)
{
	CCLOG("HiGameSdkAndroid::switchAccount() 1");
	JniHelper::callStaticVoidMethod(sdkClassName, "switchAccount", params);
	CCLOG("HiGameSdkAndroid::switchAccount() 2");
}

void HiGameSdkAndroid::exit()
{
	CCLOG("HiGameSdkAndroid::exit() 1");
	JniHelper::callStaticVoidMethod(sdkClassName, "exit");
	CCLOG("HiGameSdkAndroid::exit() 2");
}

void HiGameSdkAndroid::deal(std::string& params)
{
	CCLOG("HiGameSdkAndroid::deal() 1");
	JniHelper::callStaticVoidMethod(sdkClassName, "deal", params);
	CCLOG("HiGameSdkAndroid::deal() 2");
}

void HiGameSdkAndroid::share(std::string& params)
{
	
	JniHelper::callStaticVoidMethod(sdkClassName, "share", params);
}

void HiGameSdkAndroid::callOtherFunction(int funcType, std::string& params)
{
	CCLOG("HiGameSdkAndroid::callOtherFunction() 1");
	JniHelper::callStaticVoidMethod(sdkClassName, "callOtherFunction", funcType, params);
	CCLOG("HiGameSdkAndroid::callOtherFunction() 2");
}

void HiGameSdkAndroid::submitExtendData(std::string& params)
{
	CCLOG("HiGameSdkAndroid::submitExtendData() 1");
	JniHelper::callStaticVoidMethod(sdkClassName, "submitExtendData", params);
	CCLOG("HiGameSdkAndroid::submitExtendData() 2");
}

std::string HiGameSdkAndroid::getValue(std::string& key)
{
	CCLOG("HiGameSdkAndroid::getValue() 1");
	return JniHelper::callStaticStringMethod(sdkClassName, "getValue", key);
}

bool HiGameSdkAndroid::isLogined()
{

	CCLOG("HiGameSdkAndroid::isLogined() 1");
	return JniHelper::callStaticBooleanMethod(sdkClassName, "isLogined");
}

bool HiGameSdkAndroid::isInitSDK()
{
	CCLOG("HiGameSdkAndroid::isInitSDK() 1");
	return JniHelper::callStaticBooleanMethod(sdkClassName, "isInitSDK");
}

std::string HiGameSdkAndroid::getUserID()
{
	CCLOG("HiGameSdkAndroid::getUserID() 1");
	return JniHelper::callStaticStringMethod(sdkClassName, "getUserID");
}

std::string HiGameSdkAndroid::getAppInfo()
{
	return JniHelper::callStaticStringMethod(sdkClassName, "getAppInfo");
}

std::string HiGameSdkAndroid::getMainChannel()
{
	return  JniHelper::callStaticStringMethod(sdkClassName, "getMainChannel");
}

std::string HiGameSdkAndroid::getSubChannel()
{
	return  JniHelper::callStaticStringMethod(sdkClassName, "getSubChannel");
}

int HiGameSdkAndroid::getLogicChannel()
{
	return  JniHelper::callStaticIntMethod(sdkClassName, "getLogicChannel");
}

int HiGameSdkAndroid::getAppID()
{
	return  JniHelper::callStaticIntMethod(sdkClassName, "getAppID");
}
}
#endif /* #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) */
