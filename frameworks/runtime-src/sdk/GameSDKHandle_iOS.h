#ifndef _GameSDKHandleiOS_H_
#define _GameSDKHandleiOS_H_

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//接口定义
#include "cocos2d.h"
#include "HiGameSDK.h"

class GameSDKHandleiOS
{

public:

	static void initSDK();
	static void setSDKCalllback(CocosGameCallback* callback);
	static void login(std::string& params);
	static void logout(std::string& params);
	static void switchAccount(std::string& params);
	static void exit();
	static void deal(std::string& params);
	static void share(std::string& params);
	static void callOtherFunction(int type, std::string& params);
	static void submitExtendData(std::string& params);
	static std::string getValue(std::string& key);
	static bool isLogined();
	static bool isInitSDK();
	static std::string getUserID();
	static std::string getAppInfo();

	static std::string getMainChannel();
	static std::string getSubChannel();
	static int getLogicChannel();
	static int getAppID();


};

#endif /* #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) */
#endif // _GameSDKHandleiOS_H_
