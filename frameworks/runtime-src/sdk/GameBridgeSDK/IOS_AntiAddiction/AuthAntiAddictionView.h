//

//
//  Created by XingJie Liang on 14-7-9.
//  Copyright (c) 2014年 x. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSTimer+HYBExtension.h"

typedef void (^ClickBtnBlock)(void);

@interface AuthAntiAddictionView : UIView

@property(nonatomic,strong) NSTimer* countdownTimer;
@property (nonatomic, strong) NSDictionary *weighDict;
@property (nonatomic, assign) float  audioValue;

//-----------------property-----------
@property(nonatomic,assign) long long leftTime;
@property(nonatomic,strong) ClickBtnBlock clickBlock;

+(AuthAntiAddictionView*) getLibView;
+(void) showAuthAntiAddictionInfo:(UIView *)view age:(int)age leftTime:(long long)leftTime leftRecharge:(long)leftRecharge;
+(void) showTipDialog:(UIView *)view msg:(NSString*)tip;
+(AuthAntiAddictionView *) showGotoAuthView:(UIView *)view click:(ClickBtnBlock)clickbc;
+(void) showForceQuitGameView:(UIView *)view click:(ClickBtnBlock)clickbc;

@end
