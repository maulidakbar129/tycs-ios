//
//  LanguageTools.m
//  BridgeSDK_Plugin_iOS_AntiAddition
//
//  Created by higame on 2020/2/24.
//  Copyright © 2020 higame. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LanguageTools : NSObject

@property(nonatomic,copy) NSDictionary* textData;

//-----------------property-----------

+ (LanguageTools *)getInstance;

+ (NSString *)getTextWithKey:(NSString*)key;
@end
