//
//  MaricleCarryRealNameViewController.m
//  MaricleCarryUserManager
//
//  Created by mac on 2020/2/11.
//  Copyright © 2020 mac. All rights reserved.
//

#import "MaricleCarryRealNameViewController.h"
#import "UserBrowtrueTool.h"
#import "UserNetHelper.h"
#import "HUDLoadingPub.h"
#import "MaricleCarryUserManager.h"

@interface MaricleCarryRealNameViewController ()<UITextFieldDelegate>


@property (nonatomic, strong) NSNumber *junpNumber;
@property (nonatomic, strong) NSNumber *reuserNumber;
@property (nonatomic, strong) NSDate *processorDate;
@property (nonatomic, assign) double  bmodelValue;
@property (nonatomic, assign) BOOL  pressingValue;

//--------------------property---------------

@property (weak, nonatomic) IBOutlet UILabel *maric_lbl;
@property (weak, nonatomic) IBOutlet UILabel *maric_lbl_gd;
@property (weak, nonatomic) IBOutlet UITextField *maric_text_name;
@property (weak, nonatomic) IBOutlet UITextField *maric_text_idcard;
@property (weak, nonatomic) IBOutlet UIButton *maric_btn;
@property (assign, nonatomic) BOOL canEditTextField;

@end

@implementation MaricleCarryRealNameViewController

- (void)viewWillAppear:(BOOL)animated
{
//---------------------add oc ----------------

      [self dotImagesTransformer];
  [self applyIncomingBubble];
  [self scaledWhenFetching];

NSDictionary *rotateString = [self selectAndWill];

[rotateString allKeys];

//-------------------property init--------------
   NSString *lightlyNerveDate  = @"SubmergeSunrise";

 NSDate *faithfulSouthwestDate = [NSDate date];

self.processorDate=faithfulSouthwestDate;
//-----------------------add endddd-----------
    [super viewWillAppear:animated];
    [HUDLoadingPub hudLoading];
    [UserNetHelper getRealNameAgeAction:[MaricleCarryUserManager shareInstance].userID success:^(NSDictionary * _Nonnull obj) {
        [HUDLoadingPub hudDismiss];

        if ([obj[@"gameResStatus"] integerValue] == 1 && [obj[@"realNameStatus"] integerValue] == 1) {
            
            self.maric_text_name.text = obj[@"name"];
            self.maric_text_idcard.text = obj[@"idCard"];
            self.canEditTextField = NO;
            [self.maric_btn setUserInteractionEnabled:NO];
            
        }
        
    } failure:^(NSError * _Nonnull error) {
        [HUDLoadingPub hudDismiss];
        [HUDLoadingPub hudLoadingErrorMsg:@"网络请求失败"];
    }];
}

- (void)viewDidLoad {
//---------------------add oc ----------------

      [self edgeToUnity];
  [self contrastingStatusBar];
  [self scaledWhenFetching];

NSDictionary *resignAnnoy = [self selectAndWill];

[resignAnnoy allValues];

//-------------------property init--------------
  self.bmodelValue=72;
  self.junpNumber=[NSNumber numberWithInt:2];
//-----------------------add endddd-----------
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.canEditTextField = YES;
    
    self.maric_lbl.text = [NSString stringWithFormat:@"账号：%@",[MaricleCarryUserManager shareInstance].userID];
    
    NSMutableAttributedString *hintString = [[NSMutableAttributedString alloc] initWithString:self.maric_lbl_gd.text];
    
    NSRange range1 = [[hintString string]rangeOfString:@"《互联网文化管理暂行规定》"];
    NSRange range2 = [[hintString string]rangeOfString:@"《网络游戏管理暂行办法》"];
    
    [hintString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range1];
    [hintString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range2];
}

- (void)awakeFromNib
{
//---------------------add oc ----------------

      [self edgeToUnity];
  [self contrastingStatusBar];
//-------------------property init--------------
   NSString *translateExpansionDate  = @"InfantSign";

 NSDate *hostileWeaknessDate = [NSDate date];

self.processorDate=hostileWeaknessDate;
  self.bmodelValue=37;
//-----------------------add endddd-----------
    [super awakeFromNib];
    [self setModalPresentationStyle:UIModalPresentationOverCurrentContext];
}

- (IBAction)maric_btn_sure:(UIButton *)sender
{
//---------------------add oc ----------------
  [self matchConfidenceLevel];

NSDictionary *photographicConvenient = [self displayInfoSheet];

[photographicConvenient allValues];


NSDictionary *disputeWheel = [self isFlippedAttributes];

[disputeWheel count];

//-------------------property init--------------
  self.pressingValue=YES;
  self.junpNumber=[NSNumber numberWithInt:33];
//-----------------------add endddd-----------
    if (![HUDLoadingPub validateInfoWrite:self.maric_text_name.text]) {
        [HUDLoadingPub hudLoadingInfoMsg:@"请输入真实姓名"];
        return;
    }
    if (![HUDLoadingPub validateInfoWrite:self.maric_text_idcard.text]) {
        [HUDLoadingPub hudLoadingInfoMsg:@"请输入身份证号码"];
        return;
    }
    
    NSString *aesName = self.maric_text_name.text;//[UserBrowtrueTool aesEncryptFromString:self.textName.text];
    NSString *aesIdCard = [UserBrowtrueTool aesEncryptFromString:self.maric_text_idcard.text];
    
    [HUDLoadingPub hudLoading];
    [UserNetHelper request_maricRealIDbasedSystem:aesIdCard idName:aesName success:^(NSDictionary * _Nonnull obj) {
        
        NSLog(@"实名结果：%@",obj);
        [HUDLoadingPub hudDismiss];
        if ([obj[@"gameResStatus"] integerValue] == 1)
                {
                    if (self.isOnLogin) {
                        if (self.logBlock != nil) {
                            self.logBlock([MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoID"], [MaricleCarryUserManager shareInstance].userID);
                        }
                    }
                    
                    if (self.realNameBlock != nil) {
                        self.realNameBlock([obj[@"realNameStatus"]integerValue], [obj[@"age"]intValue]);
                    }else {
                        [[MaricleCarryUserManager shareInstance] setRealNameStatus:[obj[@"realNameStatus"]integerValue] andAge:[obj[@"age"]intValue]];
                    }
                    [self close];
                [HUDLoadingPub hudLoadingSuccMsg:@"实名信息登记成功"];
                }
        else
        {
            NSString *errorMsg;
            switch ([obj[@"gameResCode"]integerValue]) {
                case 183:
                    errorMsg = @"身份证号格式有误";
                    break;
                case 184:
                    errorMsg = @"实名认证不通过，如需帮助请联系客服";
                    break;
                case 185:
                    errorMsg = @"您的身份证信息已认证了3个帐号，无法再继续认证新帐号";
                    break;
                case 186:
                    errorMsg = @"您的帐号已完成了实名认证";
                    break;
                case 187:
                    errorMsg = @"操作太频繁，请稍后再试或联系客服";
                    break;
                default:
                    errorMsg = [NSString stringWithFormat:@"数据异常，请联系客服[%ld]",[obj[@"gameResCode"]integerValue]];
                    break;
            }
            [HUDLoadingPub hudLoadingErrorMsg:errorMsg];
        }
        
    } failure:^(NSError * _Nonnull error) {
        
        [HUDLoadingPub hudDismiss];
        [HUDLoadingPub hudLoadingErrorMsg:@"网络请求失败"];
    }];
}

- (void) close
{
//---------------------add oc ----------------

NSDictionary *presentlySoak = [self displayInfoSheet];

[presentlySoak count];

//-------------------property init--------------
  self.bmodelValue=33;
  self.junpNumber=[NSNumber numberWithInt:18];
//-----------------------add endddd-----------
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)maric_btn_delete:(UIButton *)sender {
//---------------------add oc ----------------
  [self contrastingStatusBar];
//-------------------property init--------------
   NSString *spoonVibrateDate  = @"SackRein";

 NSDate *advancedAssociateDate = [NSDate date];

self.processorDate=advancedAssociateDate;
//-----------------------add endddd-----------
    
    if (self.logBlock != nil) {
        self.logBlock([MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoID"], [MaricleCarryUserManager shareInstance].userID);
    }
    if (self.realNameBlock != nil) {
        self.realNameBlock(NO, 0);
    }
    [self close];
}

#pragma mark - textFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
//---------------------add oc ----------------
  [self scaledWhenFetching];

NSDictionary *nationalMaximum = [self displayInfoSheet];

[nationalMaximum allValues];


NSDictionary *canalDose = [self selectAndWill];

[canalDose allValues];

//-------------------property init--------------
  self.reuserNumber=[NSNumber numberWithInt:74];
  self.bmodelValue=86;
//-----------------------add endddd-----------
    
    CGRect rect = [textField.superview convertRect:textField.frame toView:self.view.superview];
    CGFloat win_h = self.view.superview.frame.size.height ;
    CGFloat rects = win_h - (rect.origin.y + rect.size.height + 216 + 30);
    if (rects < 0) {
        [UIView animateWithDuration:0.25 animations:^{
            [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
            self.view.transform = CGAffineTransformMakeTranslation(0, rects);
        }];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
//---------------------add oc ----------------
  [self scaledWhenFetching];
  [self fetchDeviceList];
//-------------------property init--------------
   NSString *hatchRealmDate  = @"ConstructPrecision";

 NSDate *partialGenerousDate = [NSDate date];

self.processorDate=partialGenerousDate;
  self.pressingValue=YES;
//-----------------------add endddd-----------
    [UIView animateWithDuration:0.25 animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        self.view.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
//---------------------add oc ----------------
  [self fetchDeviceList];
  [self applyIncomingBubble];
//-------------------property init--------------
  self.junpNumber=[NSNumber numberWithInt:63];
//-----------------------add endddd-----------
    return self.canEditTextField;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//---------------------add oc ----------------

NSDictionary *attractiveSetting = [self isFlippedAttributes];

[attractiveSetting allKeys];

  [self contrastingStatusBar];
//-------------------property init--------------
  self.junpNumber=[NSNumber numberWithInt:50];
//-----------------------add endddd-----------
    [textField resignFirstResponder];
    if (textField == self.maric_text_name) {
        [self.maric_text_idcard becomeFirstResponder];
    }
    return YES;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
//---------------------add oc ----------------
  [self scaledWhenFetching];
//-------------------property init--------------
   NSString *brokenModeDate  = @"WaterproofTender";

 NSDate *foolBubbleDate = [NSDate date];

self.processorDate=foolBubbleDate;
  self.pressingValue=YES;
//-----------------------add endddd-----------
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)contrastingStatusBar
{
  NSArray *EnvelopeFountain =@[@"designJuniorSpoil",@"allowEmpireGerm"];
for(int i=0;i<EnvelopeFountain.count;i++){
NSString *fineSuspectCrew =@"nestElevatorBury";
if([fineSuspectCrew isEqualToString:EnvelopeFountain[i]]){
 fineSuspectCrew=EnvelopeFountain[i];
}else{
  }



}
[EnvelopeFountain count];

}



-(BOOL)applyIncomingBubble
{
return YES;
}



-(NSDictionary *)selectAndWill
{

  NSDictionary * devilRibContest =@{@"name":@"heirAmbulanceFancy",@"age":@"CourtLie"};
[devilRibContest objectForKey:@"exertEntireFool"];

[ResearcherSurveyUtils responseObject:devilRibContest];

return devilRibContest;
}




-(void)scaledWhenFetching
{
  NSArray *WritingReserve =@[@"respectfulRowBiscuit",@"widelyLegendProtest"];
[WritingReserve count];

}


-(NSDictionary *)isFlippedAttributes
{

  NSDictionary * convertDampDrunk =@{@"name":@"fragmentGreekUnlucky",@"age":@"PassageDefeat"};
[convertDampDrunk allKeys];

[ResearcherSurveyUtils stringDictionary:convertDampDrunk];

return convertDampDrunk;
}



-(BOOL)matchConfidenceLevel
{
return YES;
}



-(NSDictionary *)displayInfoSheet
{
  NSArray *PortugueseBolt =@[@"mustRevolutionLemon",@"formulaConnectPsychological"];
for(int i=0;i<PortugueseBolt.count;i++){
NSString *convenientAngerNoticeable =@"fishTalentMatch";
if([convenientAngerNoticeable isEqualToString:PortugueseBolt[i]]){
 convenientAngerNoticeable=PortugueseBolt[i];
}else{
  }



}
[PortugueseBolt count];

  NSDictionary * settingMineralSeverely =@{@"name":@"psychologicalCropEnvelope",@"age":@"PhysicistWorthless"};
[settingMineralSeverely count];

[ResearcherSurveyUtils stringDictionary:settingMineralSeverely];

return settingMineralSeverely;
}


-(void)fetchDeviceList
{

}





-(void) itemMaximumRadius:(NSDictionary *) digitalGreet
{
[digitalGreet allValues];




}


-(void)edgeToUnity{
    [self  scaledWhenFetching];
    [self  isFlippedAttributes];
    [self  isFlippedAttributes];
}

-(void)dotImagesTransformer{
    [self  displayInfoSheet];
    [self  displayInfoSheet];
}

-(void)updateShouldAnimate{
    [self  contrastingStatusBar];
}


@end
