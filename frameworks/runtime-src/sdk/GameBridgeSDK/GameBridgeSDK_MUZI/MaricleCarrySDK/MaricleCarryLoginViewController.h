//
//  MaricleCarryLoginViewController.h
//  MaricleCarryUserManager
//
//  Created by mac on 2020/2/11.
//  Copyright © 2020 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MaricleCarryUserManager.h"


NS_ASSUME_NONNULL_BEGIN

typedef void (^onLoginSuccess)(NSString *gameID, NSString *userID, BOOL isReg,  BOOL realNameStatus, int age);

@interface MaricleCarryLoginViewController : UIViewController

@property (copy, nonatomic) onLoginSuccess logBlock;

//-----------------property-----------

@end

NS_ASSUME_NONNULL_END
