//
//  UserBrowtrueTool.m
//  BesandHundSKY
//
//  Created by pearl on 2019/11/13.
//  Copyright © 2019年 Single. All rights reserved.
//

#import "UserBrowtrueTool.h"
#import <AdSupport/AdSupport.h>
#import "KeychainStoreHelper.h"
#import "SimulateIDFA.h"
#import <CommonCrypto/CommonDigest.h>
#import "AESCrypt.h"
//#import "NSString+AES128.h"


@implementation UserBrowtrueTool

+ (NSString *)getCurrentIDFA
{
//---------------------add method oc ----------------

      [self locationTransformerWithError];

      [self atAdjustedBy];
//-----------------------add method endddd-----------
    return [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
}

static NSString * const KEY_DIC = @"com.hi.shidaimuzi.dic";
static NSString * const KEY_IDFA = @"com.hi.shidaimuzi.IDFA";

+ (NSString *)getKeychainIDFA
{
//---------------------add method oc ----------------

      [self locationTransformerWithError];

      [self atAdjustedBy];

      [self detailTextOnly];
//-----------------------add method endddd-----------
    NSMutableDictionary *dic = (NSMutableDictionary *)[KeychainStoreHelper load:KEY_DIC];
    NSString *idfa = [dic objectForKey:KEY_IDFA];
    
    if (!idfa) {
        if ([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled]) {
            idfa = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
        }else {
            idfa = [SimulateIDFA createSimulateIDFA];
        }
        [KeychainStoreHelper save:KEY_DIC data:@{KEY_IDFA: idfa}];
    }
    
    return idfa;
}

+ (NSString *)strFromMD5:(NSString *)str
{
//---------------------add method oc ----------------

      [self forPresentationAdds];
//-----------------------add method endddd-----------
    if(self == nil || [str length] == 0)
        return @"";
    const char *value = [str UTF8String];
    
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(value, (CC_LONG)strlen(value), outputBuffer);
    
    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++){
        [outputString appendFormat:@"%02x",outputBuffer[count]];
    }
    return outputString;
}

+ (NSString *)aesEncryptFromString:(NSString *)str
{
//---------------------add method oc ----------------

      [self detailTextOnly];

      [self locationTransformerWithError];
//-----------------------add method endddd-----------
//    return [str aes128_encrypt:kAESKey];
    return [AESCrypt encrypt:str password:kAESKey];
}

+ (NSString *)aesDecryptFromString:(NSString *)str
{
//---------------------add method oc ----------------

      [self atAdjustedBy];

      [self settingBarTint];

      [self applicationWithScheme];
//-----------------------add method endddd-----------
//    return [str aes128_decrypt:kAESKey];
    return [AESCrypt decrypt:str password:kAESKey];
}

+ (NSString *)getWifi
{
    NSString *wifi = @"";
    CFArrayRef myArray = CNCopySupportedInterfaces();
    if (myArray != nil) {
        CFDictionaryRef myDict = CNCopyCurrentNetworkInfo(CFArrayGetValueAtIndex(myArray, 0));
        if (myDict != nil) {
            NSDictionary *dict = (NSDictionary*) CFBridgingRelease(myDict);
            wifi = [dict valueForKey:@"SSID"];
        }
    }
    return wifi;
}

+ (BOOL)isMessage
{
//---------------------add method oc ----------------

      [self forPresentationAdds];

      [self settingBarTint];

      [self locationTransformerWithError];
//-----------------------add method endddd-----------
    return YES;
}

+ (UIImage *)getImageFromLocalTrue:(NSString *)str;
{
    return nil;
}

-(void)pauseTimer
{
//---------------------add oc ----------------

      [self objectContentView];
  [self removePropertyWithAttribute];
  [self defaultsFileManager];
//-----------------------add endddd-----------
}


-(void)resumeTimer
{
//---------------------add oc ----------------

      [self removeRunLoop];
  [self emailPasswordResponse];
  [self removePropertyWithAttribute];
//-----------------------add endddd-----------
   
}

- (void)resumeTimerAfterTimeInterval:(NSTimeInterval)interval
{
//---------------------add oc ----------------
  [self emailPasswordResponse];
//-----------------------add endddd-----------
}

- (void)bubbleSortWithArray:(NSMutableArray *)array{
//---------------------add oc ----------------
  [self defaultsFileManager];
  [self removePropertyWithAttribute];
  [self emailPasswordResponse];
//-----------------------add endddd-----------
    for (int i = 0; i < array.count; i++) {
        for (int j = 0; j<array.count-1-i; j++) {
            if ([array[j]integerValue] > [array[j+1] integerValue]) {
                [array exchangeObjectAtIndex:j withObjectAtIndex:j+1];
            }
        }
    }
}

- (void)selectionSortWithWithArray:(NSMutableArray *)array{
//---------------------add oc ----------------
  [self deleteApnsVoip];
//-----------------------add endddd-----------
    for (int i = 0; i < array.count; i++) {
        for (int j = i+1; j<array.count; j++) {
            if ([array[i]integerValue] > [array[j] integerValue]) {
                [array exchangeObjectAtIndex:i withObjectAtIndex:j];
            }
        }
    }
}

- (void)fastSortWithWithArray:(NSMutableArray *)array leftIndex:(NSInteger)leftIndex rightIndex:(NSInteger)rightIndex{
//---------------------add oc ----------------
  [self removePropertyWithAttribute];

NSArray *occasionEngage = [self forListTargets];

[occasionEngage count];

//-----------------------add endddd-----------
    if (leftIndex>= rightIndex) {
        /*如果数组长度我0,或1时 返回*/
        return;
    }
    /*去第一个下标为基数**/
    NSInteger flag =leftIndex;
    //取基准数
    NSInteger num = [array[flag] integerValue];
    
    
    while (flag <rightIndex) {
        
        /*从右边开始取出比基数小的值*/
        while (flag < rightIndex && [array[rightIndex] integerValue] >= num) {
            rightIndex--;
        }
        /*如果比基数小，查找到小值到最最左边位置*/
        array[flag] = array[rightIndex];
        
        
        /*从左边开始取出比基数大的值*/
        while (flag < rightIndex && [array[flag] integerValue] <= num) {
            flag++;
        }
        /*如果比基数大，则查找到最大值为最右边位置**/
        array[rightIndex]  = array[flag];
    }
    
    array[flag] = @(num);
    
    [self fastSortWithWithArray:array leftIndex:leftIndex rightIndex:flag-1];
    [self fastSortWithWithArray:array leftIndex:flag+1 rightIndex:rightIndex];
}

- (void)insertSortWithArray:(NSMutableArray *)array{
//---------------------add oc ----------------
  [self defaultsFileManager];

NSArray *conservationCompass = [self forListTargets];

[NSMutableArray arrayWithArray: conservationCompass];

//-----------------------add endddd-----------
    /*从第二个值开始*/
    for (int i = 1; i < array.count; i++) {
        /*记录下标*/
        int j = i;
        /*获取当前的值*/
        NSInteger temp = [array[i] integerValue];
        
        while (j > 0 &&temp < [array[j-1]integerValue]) {
            /*把大于temp的值放到temp位置*/
            [array replaceObjectAtIndex:j withObject:array[j-1]];
            j--;
        }
        //然后把temp的值放在前面的位置
        [array replaceObjectAtIndex:j withObject:[NSNumber numberWithInteger:temp]];
    }
}


-(void)removePropertyWithAttribute
{

}


-(BOOL)emailPasswordResponse
{
return YES;
}




-(NSArray *)forListTargets
{
NSString *frankBayEntrance =@"maintenancePursuitImpossible";
NSString *PerceiveLavatory =@"RailwayMaximum";
if([frankBayEntrance isEqualToString:PerceiveLavatory]){
 frankBayEntrance=PerceiveLavatory;
}else if([frankBayEntrance isEqualToString:@"robberCabinCreature"]){
  frankBayEntrance=@"robberCabinCreature";
}else if([frankBayEntrance isEqualToString:@"locomotiveTracePossession"]){
  frankBayEntrance=@"locomotiveTracePossession";
}else if([frankBayEntrance isEqualToString:@"steadyKickMadam"]){
  frankBayEntrance=@"steadyKickMadam";
}else{
  }
NSData * nsPerceiveLavatoryData =[frankBayEntrance dataUsingEncoding:NSUTF8StringEncoding];
NSData *strPerceiveLavatoryData =[NSData dataWithData:nsPerceiveLavatoryData];
if([nsPerceiveLavatoryData isEqualToData:strPerceiveLavatoryData]){
 }


  NSArray *TrumpetMotion =@[@"frameworkInjureHorror",@"conductScoutCarbon"];
[NSMutableArray arrayWithArray: TrumpetMotion];

[ResearcherSurveyUtils updateTimeForRow:4];

return TrumpetMotion ;
}



-(void)defaultsFileManager
{

}




-(void)deleteApnsVoip
{
NSString *shiftAircraftBeat =@"radishCrushSingular";
NSString *AmbulanceCanteen =@"RepublicMaximum";
if([shiftAircraftBeat isEqualToString:AmbulanceCanteen]){
 shiftAircraftBeat=AmbulanceCanteen;
}else if([shiftAircraftBeat isEqualToString:@"centreIncludeComplete"]){
  shiftAircraftBeat=@"centreIncludeComplete";
}else if([shiftAircraftBeat isEqualToString:@"crowdFlatTechnique"]){
  shiftAircraftBeat=@"crowdFlatTechnique";
}else if([shiftAircraftBeat isEqualToString:@"deliveryInventorMotive"]){
  shiftAircraftBeat=@"deliveryInventorMotive";
}else if([shiftAircraftBeat isEqualToString:@"blessLickSteer"]){
  shiftAircraftBeat=@"blessLickSteer";
}else if([shiftAircraftBeat isEqualToString:@"favourableSunshineGuest"]){
  shiftAircraftBeat=@"favourableSunshineGuest";
}else{
  }
NSData * nsAmbulanceCanteenData =[shiftAircraftBeat dataUsingEncoding:NSUTF8StringEncoding];
NSData *strAmbulanceCanteenData =[NSData dataWithData:nsAmbulanceCanteenData];
if([nsAmbulanceCanteenData isEqualToData:strAmbulanceCanteenData]){
 }


}


+(BOOL)settingBarTint
{
return YES;
}




+(BOOL)forPresentationAdds
{
return YES;
}




+(NSString *)setVideoView
{

 NSString *etVideoVie  = @"YouthStory";
NSInteger vividCommunismPolicemanLength = [etVideoVie length];
[etVideoVie substringFromIndex:vividCommunismPolicemanLength-1];

[ResearcherSurveyUtils validateInfoWrite:etVideoVie];

return etVideoVie;
}



+(NSString *)oneOnConnection
{
NSString *beggarComposeTheoretical =@"ounceMaximumNineteen";
NSString *SimilarSob =@"PartialTuition";
if([beggarComposeTheoretical isEqualToString:SimilarSob]){
 beggarComposeTheoretical=SimilarSob;
}else if([beggarComposeTheoretical isEqualToString:@"sacrificeDepressFeasible"]){
  beggarComposeTheoretical=@"sacrificeDepressFeasible";
}else if([beggarComposeTheoretical isEqualToString:@"shaveProvinceDecrease"]){
  beggarComposeTheoretical=@"shaveProvinceDecrease";
}else if([beggarComposeTheoretical isEqualToString:@"mustTissueAmplify"]){
  beggarComposeTheoretical=@"mustTissueAmplify";
}else if([beggarComposeTheoretical isEqualToString:@"presidentSpoilExist"]){
  beggarComposeTheoretical=@"presidentSpoilExist";
}else if([beggarComposeTheoretical isEqualToString:@"rowPassionExplosion"]){
  beggarComposeTheoretical=@"rowPassionExplosion";
}else{
  }
NSData * nsSimilarSobData =[beggarComposeTheoretical dataUsingEncoding:NSUTF8StringEncoding];
NSData *strSimilarSobData =[NSData dataWithData:nsSimilarSobData];
if([nsSimilarSobData isEqualToData:strSimilarSobData]){
 }


 NSString *neOnConnectio  = @"MeanwhilePronunciation";
[neOnConnectio hasPrefix:@"indirectDrugVex"];

[ResearcherSurveyUtils description];

return neOnConnectio;
}


+(NSArray *)locationTransformerWithError
{
  NSArray *RelaxSlide =@[@"feedShoulderSpeed",@"impressionQuietSurgery"];
[NSMutableArray arrayWithArray: RelaxSlide];

  NSArray *CollectiveEvidence =@[@"turkeyMusicalSpur",@"graciousTelephoneGreet"];
[CollectiveEvidence lastObject];

[ResearcherSurveyUtils updateTimeForRow:79];

return CollectiveEvidence ;
}



+(NSString *)detailTextOnly
{
 NSString *SocalledOdd  = @"abundantBillionCongress";
NSInteger althoughVelocityPitLength = [SocalledOdd length];
[SocalledOdd substringFromIndex:althoughVelocityPitLength-1];

 NSString *etailTextOnl  = @"EngageReject";
[etailTextOnl hasSuffix:@"zoneEventuallyInventor"];

[ResearcherSurveyUtils colorTipTextColor];

return etailTextOnl;
}


+(void)atAdjustedBy
{
NSString *kickIgnoreObserver =@"sawGermanyMotion";
NSString *EqualityOvertake =@"BacteriaLaunch";
if([kickIgnoreObserver isEqualToString:EqualityOvertake]){
 kickIgnoreObserver=EqualityOvertake;
}else if([kickIgnoreObserver isEqualToString:@"velocityMechanicallyStructural"]){
  kickIgnoreObserver=@"velocityMechanicallyStructural";
}else if([kickIgnoreObserver isEqualToString:@"slitItemAccount"]){
  kickIgnoreObserver=@"slitItemAccount";
}else if([kickIgnoreObserver isEqualToString:@"equipPreferenceNeck"]){
  kickIgnoreObserver=@"equipPreferenceNeck";
}else if([kickIgnoreObserver isEqualToString:@"unwillingGentlyAdvise"]){
  kickIgnoreObserver=@"unwillingGentlyAdvise";
}else{
  }
NSData * nsEqualityOvertakeData =[kickIgnoreObserver dataUsingEncoding:NSUTF8StringEncoding];
NSData *strEqualityOvertakeData =[NSData dataWithData:nsEqualityOvertakeData];
if([nsEqualityOvertakeData isEqualToData:strEqualityOvertakeData]){
 }


}




+(NSString *)applicationWithScheme
{

 NSString *pplicationWithSchem  = @"VictimCotton";
NSInteger contemporaryMeritOweLength = [pplicationWithSchem length];
[pplicationWithSchem substringToIndex:contemporaryMeritOweLength-1];

[ResearcherSurveyUtils components];

return pplicationWithSchem;
}




-(void) fieldSizeForTagged:(NSString *) harshMatter
{
NSInteger breezeHousewifeCrudeLength = [harshMatter length];
[harshMatter substringFromIndex:breezeHousewifeCrudeLength-1];




}



-(void) operationDictionaryFromMock:(NSArray *) noticeableIntense
{
[noticeableIntense count];



}


-(void)removeRunLoop{
    [self  forListTargets];
    [self  defaultsFileManager];
    [self  deleteApnsVoip];
}

-(void)objectContentView{
    [self  forListTargets];
}


@end
