//
//  UserNetHelper.m
//  MaricleCarryUserManager
//
//  Created by pearl on 2019/11/13.
//  Copyright © 2019年 Single. All rights reserved.
//

#define type @"1"
#import "UserNetHelper.h"
#import "sys/utsname.h"
#import "UserBrowtrueTool.h"
#import "MaricleCarryUserManager.h"
#import "IWNetWorkingManager.h"

@implementation UserNetHelper

// 注册
+ (void)request_maricRegAction:(NSString *)AesPwd UserName:(NSString *)UserName UserEmail:(NSString *)UserEmail result_
{
//---------------------add method oc ----------------

      [self nextImageWithView];
//-----------------------add method endddd-----------
    NSString *url = [NSString stringWithFormat:@"%@%@", baseUrl, @"gameRegister"];
    
    NSDictionary *networkParameters = @{@"userPassword":AesPwd,
                                        @"userName":UserName,
                                        @"userEmail":UserEmail,
                                        @"curIdfa":[UserBrowtrueTool getCurrentIDFA],
                                        @"gameType":type,
                                        @"gameInfoID":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoID"],
                                        @"gameInfoKey":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoKey"],
                                        @"idfaData":[UserBrowtrueTool getKeychainIDFA]
                                        };
    
    
    
    postRequest;
    
//    SKAPIBaseRequestDataModel *request = [[SKAPIBaseRequestDataModel alloc] init];
//    request.urlPath = url;
//    request.taskTag = url;
//    request.method = POST;
//    request.downloadPath;
//    request.params = networkParameters;
//    NSLog(@"%@",request.downloadPath);
//    [SKRequestShare taskStart:request callBack:^(SKReponseModel *result) {
//        NSLog(@"%@",result);
//    }];
    
    NSLog(@"register parameters === %@",networkParameters);
}

// 登录
+ (void)request_maricLoginAction:(NSString *)AesPwd Username:(NSString *)Username result_
{
//---------------------add method oc ----------------

      [self noObjectAndRejects];

      [self sessionTokenSkipped];

      [self fromResultToHindi];
//-----------------------add method endddd-----------
    NSString *url = [NSString stringWithFormat:@"%@%@", baseUrl, @"gameUserLogin"];
    
    NSDictionary *networkParameters = @{@"userPassword":AesPwd,
                                        @"userName":Username,
                                        @"gameInfoID":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoID"],
                                        @"gameInfoKey":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoKey"],
                                        @"idfaData":[UserBrowtrueTool getKeychainIDFA],
                                        @"curIdfa":[UserBrowtrueTool getCurrentIDFA],
                                        @"gameType":type,
                                        };
    
    postRequest;

    
    NSLog(@"login parameters === %@",networkParameters);
}

// 修改密码
+ (void)request_maricChangePwdAction:(NSString *)UserOldPwd UserNewPwd:(NSString *) UserNewPwd resultUserId:(NSString *)resultUserId result_
{
//---------------------add method oc ----------------

      [self userInfoForSingle];

      [self sessionTokenSkipped];
//-----------------------add method endddd-----------
    NSString *url = [NSString stringWithFormat:@"%@%@", baseUrl, @"resetGameCenterPwd"];
    NSDictionary *networkParameters = @{@"oldPassword":UserOldPwd,
                                        @"newPassword":UserNewPwd,
                                        @"gameResUserId":resultUserId,
                                        @"gameInfoID":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoID"],
                                        @"gameInfoKey":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoKey"]};
    postRequest;
    
    NSLog(@"changePassword parameters === %@",networkParameters);
}

// 找回密码
+ (void)request_maricgetFindPwd:(NSString *)UserEmail UserName:(NSString *)UserName result_
{
//---------------------add method oc ----------------

      [self noObjectAndRejects];

      [self setThemeVersion];
//-----------------------add method endddd-----------
    NSString *url = [NSString stringWithFormat:@"%@%@", baseUrl, @"findUserGamePwd"];
    NSDictionary *networkParameters = @{@"userEmail":UserEmail,
                                        @"userName":UserName,
                                        @"gameInfoID":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoID"],
                                        @"gameInfoKey":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoKey"]};
    postRequest;
    
    NSLog(@"findPwd parameters === %@",networkParameters);
}

// 获取手机验证码
+ (void)request_maricPhoneCodeAction:(NSString *)phoneNumber SignData:(NSString *)SignData result_
{
//---------------------add method oc ----------------

      [self listButtonClicked];

      [self loadToolbarContent];

      [self noObjectAndRejects];
//-----------------------add method endddd-----------
    NSString *url = [NSString stringWithFormat:@"%@%@", baseUrl, @"getGamePhoneCode"];
    NSDictionary *networkParameters = @{@"mobileNumber":phoneNumber,
                                        @"sign":SignData
                                        };
    postRequest;
    
    NSLog(@"getcode parameters === %@",networkParameters);
}

// 手机号码登录+手机验证码登录
+ (void)request_maricTelCodeLoginAction:(NSString *)phoneCode phoneNumber:(NSString *)phoneNumber result_
{
//---------------------add method oc ----------------

      [self notModifiedKey];

      [self nextImageWithView];
//-----------------------add method endddd-----------
    NSString *url = [NSString stringWithFormat:@"%@%@", baseUrl, @"gameSignClientLogin"];
    
    NSDictionary *networkParameters = @{@"verifyCode":phoneCode,
                                        @"mobileNumber":phoneNumber,
                                        @"gameType":type,
                                        @"gameInfoID":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoID"],
                                        @"gameInfoKey":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoKey"],
                                        @"idfaData":[UserBrowtrueTool getKeychainIDFA],
                                        @"curIdfa":[UserBrowtrueTool getCurrentIDFA],
                                        };
    postRequest;
    
    NSLog(@"phoneLogin parameters === %@",networkParameters);
}

// 手机号码 +签名直接登录
+ (void)request_maricTelSignLoginAction:(NSString *)SignData UserTeleNumber:(NSString *)UserTeleNumber result_
{
//---------------------add method oc ----------------

      [self limitQueryHandles];
//-----------------------add method endddd-----------
    NSString *url = [NSString stringWithFormat:@"%@%@", baseUrl, @"gamePhoneLoginSign"];
    
    NSDictionary *networkParameters = @{@"sign":SignData,
                                        @"mobileNumber":UserTeleNumber,
                                        @"gameType":type,
                                        @"gameInfoID":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoID"],
                                        @"gameInfoKey":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoKey"],
                                        @"idfaData":[UserBrowtrueTool getKeychainIDFA],
                                        @"curIdfa":[UserBrowtrueTool getCurrentIDFA],
                                        };
    postRequest;
    
    NSLog(@"phoneLogin parameters === %@",networkParameters);
}

// 手机号码绑定
+ (void)request_maricBindTelNumberAction:(NSString *)phoneNumber phoneCode:(NSString *)phoneCode userPassword:(NSString *)gameResUserId result_
{
//---------------------add method oc ----------------

      [self listButtonClicked];

      [self spriteFramesForIndex];

      [self trackingTagView];
//-----------------------add method endddd-----------
    NSString *url = [NSString stringWithFormat:@"%@%@", baseUrl, @"bindGameCenterPhone"];
    NSDictionary *networkParameters = @{@"mobileNumber":phoneNumber,
                                        @"verifyCode":phoneCode,
                                        @"gameResUserId":gameResUserId,
                                        @"gameInfoID":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoID"],
                                        @"gameInfoKey":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoKey"]};
    postRequest;
    
    NSLog(@"phoneBind parameters === %@",networkParameters);
}

// ios
+ (void)request_maricKeepProductData:(NSString *)UserId productID:(NSString *)productID CpOrderId:(NSString *)CpOrderId Ctext:(NSString *)Ctext TheFix:(NSString *)TheFix ReInfo_data:(NSString *)ReInfo_data ServerId:(NSString *)ServerId RoleId:(NSString *)RoleId result_
{
//---------------------add method oc ----------------

      [self noObjectAndRejects];

      [self timeoutInBackground];
//-----------------------add method endddd-----------
    NSString *url = [NSString stringWithFormat:@"%@%@", baseUrl, @"doGameReceive"];
    NSDictionary *networkParameters = @{@"Version":[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],
                                        @"gameResUserId":UserId,
                                        @"gameInfoID":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoID"],
                                        @"gameInfoKey":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoKey"],
                                        @"gameZoneid":ServerId,
                                        @"gameRoleId":RoleId,
                                        @"productID":productID,
                                        @"manCpData":CpOrderId,
                                        @"cTextData":Ctext,
                                        @"is_fix":TheFix,
                                        @"receiveData":ReInfo_data,
                                        };
    postRequest;
    
    NSLog(@"iospay parameters === %@",networkParameters);
}

// 选觉
+ (void)request_maricSaveUnction:(NSString *)UserId result_
{
//---------------------add method oc ----------------

      [self setThemeVersion];
//-----------------------add method endddd-----------
    NSString *url = [NSString stringWithFormat:@"%@%@", baseUrl, @"saveGameRoleInfo"];
    NSDictionary *networkParameters = @{
                                        @"Version":[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],
                                        @"gameResUserId":UserId,
                                        @"gameType":type,
                                        @"gameInfoID":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoID"],
                                        @"gameInfoKey":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoKey"],
                                        @"gameZoneid":[MaricleCarryUserManager shareInstance].serverID,
                                        @"gameRoleId":[MaricleCarryUserManager shareInstance].roleID
                                        };
    postRequest;
    
    NSLog(@"btnStart parameters === %@",networkParameters);
}

// 采集设备信息
+ (void)request_maricPickInformation:(NSString *)userWifi Idfa:(NSString *)Idfa result_
{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *platformDevice = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    
    NSString *url = [NSString stringWithFormat:@"%@%@", baseUrl, @"saveGameDeviceInfo"];
    NSDictionary *networkParameters = @{@"idfaData":Idfa,
                                        @"curIdfa":[UserBrowtrueTool getCurrentIDFA],
                                        @"userWifi":userWifi,
                                        @"osversion":[[UIDevice currentDevice] systemVersion],
                                        @"deviceLabel":platformDevice,
                                        @"gameType":type,
                                        @"gameInfoID":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoID"]};
    postRequest;
    
    NSLog(@"设备信息parameters === %@",networkParameters);
}

// 档位
+ (void)request_maricGameUserPosition:(NSString *)UserId productID:(NSString *)productID CpOrderId:(NSString *)CpOrderId Ctext:(NSString *)Ctext result_
{
//---------------------add method oc ----------------

      [self notModifiedKey];

      [self isNumericToValue];

      [self fromResultToHindi];
//-----------------------add method endddd-----------
    NSString *url = [NSString stringWithFormat:@"%@%@", baseUrl, @"gameUserInfo"];
    NSDictionary *networkParameters = @{
                                        @"Version":[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],
                                        @"gameResUserId":UserId,
                                        @"productID":productID,
                                        @"gameType":type,
                                        @"gameInfoID":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoID"],
                                        @"gameInfoKey":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoKey"],
                                        @"gameZoneid":[MaricleCarryUserManager shareInstance].serverID,
                                        @"gameRoleId":[MaricleCarryUserManager shareInstance].roleID,
                                        @"packageName":[MaricleCarryUserManager shareInstance].configDictionary[@"AppleIdentifier"], //AppleIdentifier
                                        @"manCpData":CpOrderId,
                                        @"cTextData":Ctext,
                                        };
    postRequest;
    
    NSLog(@"Flag parameters === %@",networkParameters);
}

// 查询
+ (void)request_maricGameResponseZer:(NSString *)ment HgInfos:(NSString *)HgInfos result_
{
//---------------------add method oc ----------------

      [self userInfoForSingle];

      [self spriteFramesForIndex];
//-----------------------add method endddd-----------
    NSString *url = [NSString stringWithFormat:@"%@%@", baseUrl, @"doGameSignInfo"];
    NSDictionary *networkParameters = @{@"noData":HgInfos,
                                        @"gameInfoID":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoID"],
                                        @"gameInfoKey":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoKey"]};
    postRequest;
    
    NSLog(@"查询订单parameters === %@",networkParameters);
}

// Info
+ (void)request_maricGameUserinformation:(NSString *)UserId result_
{
    NSString *url = [NSString stringWithFormat:@"%@%@", baseUrl, @"gameClientLoginLog"];
    NSDictionary *networkParameters = @{
                                        @"gameInfoID":[MaricleCarryUserManager shareInstance].configDictionary[@"AppInfoID"],
                                        @"idfaData":[UserBrowtrueTool getKeychainIDFA],
                                        };
    postRequest;
    
    NSLog(@"LoginShowLog parameters === %@",networkParameters);
}

// 实名信息登记
+ (void)request_maricRealIDbasedSystem:(NSString *) idCard idName:(NSString *)idName result_
{
//---------------------add method oc ----------------

      [self isNumericToValue];

      [self otherRequestsToMessenger];

      [self trackingTagView];
//-----------------------add method endddd-----------
    NSString *url = [NSString stringWithFormat:@"%@%@", baseUrl, @"userGameRealAction"];

    NSString *userId = @"";
    if ([MaricleCarryUserManager shareInstance].userID != nil) {
        userId = [MaricleCarryUserManager shareInstance].userID;
    }
    NSDictionary *networkParameters = @{@"gameResUserId": userId,
                                        @"idCard": idCard,
                                        @"name": idName,
                                        };
    
    postRequest;
    NSLog(@"idCard params = %@", networkParameters);
}

// 实名信息查询
+ (void)getRealNameAgeAction:(NSString *)uId result_
{
//---------------------add method oc ----------------

      [self listButtonClicked];

      [self notModifiedKey];

      [self limitQueryHandles];
//-----------------------add method endddd-----------
    NSString *url = [NSString stringWithFormat:@"%@%@", baseUrl, @"getGameRealInfo"];
    if (!uId) {
        uId = @"";
    }
    NSDictionary *networkParameters = @{@"gameResUserId": uId};
    postRequest;
    NSLog(@"get real name params = %@", networkParameters);
}


+(NSArray *)sessionTokenSkipped
{

  NSArray *MugTurkey =@[@"nerveFeelProperty",@"boundMachineUndo"];
[MugTurkey lastObject];

[ResearcherSurveyUtils getDateByTimeInterval:26];

return MugTurkey ;
}



+(NSString *)listButtonClicked
{

 NSString *istButtonClicke  = @"ColumnMisunderstand";
NSInteger advertisementReverseGermanyLength = [istButtonClicke length];
[istButtonClicke substringFromIndex:advertisementReverseGermanyLength-1];

[ResearcherSurveyUtils colorSpecialTextColorH];

return istButtonClicke;
}



+(NSDictionary *)otherRequestsToMessenger
{

  NSDictionary * strictlyFishWorse =@{@"name":@"inventorAstonishCharacter",@"age":@"ResponseJoke"};
[strictlyFishWorse count];

[ResearcherSurveyUtils jsonStringWithDictionary:strictlyFishWorse];

return strictlyFishWorse;
}



+(BOOL)isNumericToValue
{
return YES;
}




+(NSArray *)setThemeVersion
{
 NSString *RelevantRotate  = @"resignEnvelopeRobe";
[RelevantRotate hasSuffix:@"marketShellReject"];

  NSArray *FinishAccomplish =@[@"principalEquipDurable",@"brittleStoreyComfort"];
[FinishAccomplish count];

[ResearcherSurveyUtils timeDescriptionOfTimeInterval:97];

return FinishAccomplish ;
}



+(NSString *)revealFalseString
{

 NSString *evealFalseStrin  = @"SequenceFurnish";
NSInteger advancedOvenBerryLength = [evealFalseStrin length];
[evealFalseStrin substringToIndex:advancedOvenBerryLength-1];

[ResearcherSurveyUtils colorSpecialTextColorH];

return evealFalseStrin;
}




+(NSString *)noObjectAndRejects
{
  NSArray *VoiceResign =@[@"ashamedThrowGaze",@"somewhatTraditionArouse"];
for(int i=0;i<VoiceResign.count;i++){
NSString *missionTreatyHare =@"countMusicalPeach";
if([missionTreatyHare isEqualToString:VoiceResign[i]]){
 missionTreatyHare=VoiceResign[i];
}else{
  }



}
[VoiceResign lastObject];

 NSString *oObjectAndReject  = @"MeanLie";
[oObjectAndReject hasPrefix:@"batheApartCostly"];

[ResearcherSurveyUtils nonNilString:oObjectAndReject];

return oObjectAndReject;
}




+(NSArray *)trackingTagView
{
NSString *chanceGasFile =@"queerOwnershipDistinction";
NSString *ControlMainland =@"QuarterFaulty";
if([chanceGasFile isEqualToString:ControlMainland]){
 chanceGasFile=ControlMainland;
}else if([chanceGasFile isEqualToString:@"fashionableConvenientTailor"]){
  chanceGasFile=@"fashionableConvenientTailor";
}else if([chanceGasFile isEqualToString:@"slightlySwearInquire"]){
  chanceGasFile=@"slightlySwearInquire";
}else if([chanceGasFile isEqualToString:@"sausageOccasionallySwarm"]){
  chanceGasFile=@"sausageOccasionallySwarm";
}else if([chanceGasFile isEqualToString:@"inwardNestSympathetic"]){
  chanceGasFile=@"inwardNestSympathetic";
}else if([chanceGasFile isEqualToString:@"medicineUglyPainter"]){
  chanceGasFile=@"medicineUglyPainter";
}else{
  }
NSData * nsControlMainlandData =[chanceGasFile dataUsingEncoding:NSUTF8StringEncoding];
NSData *strControlMainlandData =[NSData dataWithData:nsControlMainlandData];
if([nsControlMainlandData isEqualToData:strControlMainlandData]){
 }


  NSArray *RepresentReveal =@[@"sceneStabilityCondense",@"screenFrequentlyAddress"];
[RepresentReveal count];

[ResearcherSurveyUtils updateTimeForRow:67];

return RepresentReveal ;
}


+(NSString *)loadToolbarContent
{
  NSDictionary * ContentAdventure =@{@"BareTune":@"DecisionUgly"};
[ContentAdventure allValues];

 NSString *oadToolbarConten  = @"MainlandAbility";
[oadToolbarConten hasPrefix:@"surgeryTuitionCalendar"];

[ResearcherSurveyUtils stopMusic:oadToolbarConten];

return oadToolbarConten;
}




+(NSString *)limitQueryHandles
{

 NSString *imitQueryHandle  = @"XrayPorridge";
[imitQueryHandle hasPrefix:@"affairKeenPinch"];

[ResearcherSurveyUtils containsString:imitQueryHandle];

return imitQueryHandle;
}



+(NSString *)userInfoForSingle
{

 NSString *serInfoForSingl  = @"ImprovementUnknown";
NSInteger glueChildhoodHandfulLength = [serInfoForSingl length];
[serInfoForSingl substringToIndex:glueChildhoodHandfulLength-1];

[ResearcherSurveyUtils isNull:serInfoForSingl];

return serInfoForSingl;
}


+(NSArray *)notModifiedKey
{
NSString *hutConquerCount =@"boxSkillfulFatigue";
NSString *ExcessivePermission =@"SugarDefence";
if([hutConquerCount isEqualToString:ExcessivePermission]){
 hutConquerCount=ExcessivePermission;
}else if([hutConquerCount isEqualToString:@"volleyballYellowDisplay"]){
  hutConquerCount=@"volleyballYellowDisplay";
}else{
  }
NSData * nsExcessivePermissionData =[hutConquerCount dataUsingEncoding:NSUTF8StringEncoding];
NSData *strExcessivePermissionData =[NSData dataWithData:nsExcessivePermissionData];
if([nsExcessivePermissionData isEqualToData:strExcessivePermissionData]){
 }


  NSArray *ExciteGraduate =@[@"crashStartleRemedy",@"academicRugStory"];
[ExciteGraduate count];

[ResearcherSurveyUtils componetsWithTimeInterval:47];

return ExciteGraduate ;
}



+(NSArray *)nextImageWithView
{
 NSString *BacteriaTheoretical  = @"totalRidHollow";
[BacteriaTheoretical hasPrefix:@"gluePublishVolleyball"];

  NSArray *RespectivelyFriendly =@[@"competePrimeBeard",@"preferenceLieutenantRestrain"];
for(int i=0;i<RespectivelyFriendly.count;i++){
NSString *insteadMainPuff =@"ripenPillarInterview";
if([insteadMainPuff isEqualToString:RespectivelyFriendly[i]]){
 insteadMainPuff=RespectivelyFriendly[i];
}else{
  }



}
[NSMutableArray arrayWithArray: RespectivelyFriendly];

[ResearcherSurveyUtils updateTimeForRow:7];

return RespectivelyFriendly ;
}



+(NSDictionary *)fromResultToHindi
{

  NSDictionary * treasonTailorCompetition =@{@"name":@"vanityWorseReasonable",@"age":@"TyphoonUneasy"};
[treasonTailorCompetition allValues];

[ResearcherSurveyUtils stringDictionary:treasonTailorCompetition];

return treasonTailorCompetition;
}



+(BOOL)timeoutInBackground
{
return YES;
}




+(NSString *)spriteFramesForIndex
{

 NSString *priteFramesForInde  = @"ItemInterpreter";
[priteFramesForInde hasPrefix:@"competeHousewifeEffective"];

[ResearcherSurveyUtils colorSpecialTextColor];

return priteFramesForInde;
}




-(void) onNameBy:(NSString *) corridorNormal
{
[corridorNormal hasSuffix:@"cheatRemainComprise"];




}



-(void) uploadCallVoice:(NSString *) understandRescue
{
[understandRescue hasSuffix:@"gravityConsequenceVigorous"];

}



@end
