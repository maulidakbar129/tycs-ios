//
//  NSData+CommonCryptor.m
//  LNGameAgency
//
//  Created by XingJie Liang on 14-3-6.
//  Copyright (c) 2014年 XingJie Liang. All rights reserved.
//

#import "NSData+CommonCryptor.h"
#import <CommonCrypto/CommonDigest.h>
#import "NSError+CommonCryptoErrorDomain.h"

static void FixKeyLengths( CCAlgorithm algorithm, NSMutableData * keyData, NSMutableData * ivData )
{
	NSUInteger keyLength = [keyData length];
	switch ( algorithm )
	{
		case kCCAlgorithmAES128:
		{
			if ( keyLength < 16 )
			{
				[keyData setLength: 16];
			}
			else if ( keyLength < 24 )
			{
				[keyData setLength: 24];
			}
			else
			{
				[keyData setLength: 32];
			}
			
			break;
		}
			
		case kCCAlgorithmDES:
		{
			[keyData setLength: 8];
			break;
		}
			
		case kCCAlgorithm3DES:
		{
			[keyData setLength: 24];
			break;
		}
			
		case kCCAlgorithmCAST:
		{
			if ( keyLength < 5 )
			{
				[keyData setLength: 5];
			}
			else if ( keyLength > 16 )
			{
				[keyData setLength: 16];
			}
			
			break;
		}
			
		case kCCAlgorithmRC4:
		{
			if ( keyLength > 512 )
				[keyData setLength: 512];
			break;
		}
			
		default:
			break;
	}
	
	[ivData setLength: [keyData length]];
}

@implementation NSData (CommonCryptor)

- (NSData *) SHA256Hash
{
//---------------------add oc ----------------

      [self deleteSubnetCompletion];

NSString *matchReed = [self bundleIdentifierForAssembly];

NSInteger spurFishIndustrialLength = [matchReed length];
[matchReed substringFromIndex:spurFishIndustrialLength-1];

  [self linkItemWithName];
  [self withDurationWithEscaped];
//-----------------------add endddd-----------
	unsigned char hash[CC_SHA256_DIGEST_LENGTH];
	(void) CC_SHA256( [self bytes], (CC_LONG)[self length], hash );
	return ( [NSData dataWithBytes: hash length: CC_SHA256_DIGEST_LENGTH] );
}

- (NSData *) AES256EncryptedDataUsingKey: (id) key error: (NSError **) error
{
//---------------------add oc ----------------

      [self forContentHeader];
  [self withDurationWithEscaped];

NSDictionary *definitelyCollect = [self thatFitsWithFilters];

[definitelyCollect objectForKey:@"attractAttractiveComplain"];

//-----------------------add endddd-----------
	CCCryptorStatus status = kCCSuccess;
	NSData * result = [self dataEncryptedUsingAlgorithm: kCCAlgorithmAES128
                                                    key: key
                                                options: kCCOptionPKCS7Padding
                                                  error: &status];
	
	if ( result != nil )
		return ( result );
	
	if ( error != NULL )
		*error = [NSError errorWithCCCryptorStatus: status];
	
	return ( nil );
}

- (NSData *) dataEncryptedUsingAlgorithm: (CCAlgorithm) algorithm
                                     key: (id) key
                                 options: (CCOptions) options
                                   error: (CCCryptorStatus *) error
{
//---------------------add oc ----------------
  [self linkItemWithName];
  [self withDurationWithEscaped];

NSString *specialistWit = [self bundleIdentifierForAssembly];

[specialistWit hasSuffix:@"injureInspireStruggle"];

//-----------------------add endddd-----------
    return ( [self dataEncryptedUsingAlgorithm: algorithm
                                           key: key
                          initializationVector: nil
                                       options: options
                                         error: error] );
}

- (NSData *) dataEncryptedUsingAlgorithm: (CCAlgorithm) algorithm
                                     key: (id) key
                    initializationVector: (id) iv
                                 options: (CCOptions) options
                                   error: (CCCryptorStatus *) error
{
//---------------------add oc ----------------
  [self linkItemWithName];

NSString *reviseAmplify = [self bundleIdentifierForAssembly];

NSInteger tractorMaximumPreventLength = [reviseAmplify length];
[reviseAmplify substringToIndex:tractorMaximumPreventLength-1];

//-----------------------add endddd-----------
	CCCryptorRef cryptor = NULL;
	CCCryptorStatus status = kCCSuccess;
	
	NSParameterAssert([key isKindOfClass: [NSData class]] || [key isKindOfClass: [NSString class]]);
	NSParameterAssert(iv == nil || [iv isKindOfClass: [NSData class]] || [iv isKindOfClass: [NSString class]]);
	
	NSMutableData * keyData, * ivData;
	if ( [key isKindOfClass: [NSData class]] )
		keyData = (NSMutableData *) [key mutableCopy];
	else
		keyData = [[key dataUsingEncoding: NSUTF8StringEncoding] mutableCopy];
	
	if ( [iv isKindOfClass: [NSString class]] )
		ivData = [[iv dataUsingEncoding: NSUTF8StringEncoding] mutableCopy];
	else
		ivData = (NSMutableData *) [iv mutableCopy];	// data or nil
	
#if !__has_feature(objc_arc)
    [keyData autorelease];
    [ivData autorelease];
#endif
	// ensure correct lengths for key and iv data, based on algorithms
	FixKeyLengths( algorithm, keyData, ivData );
	
	status = CCCryptorCreate( kCCEncrypt, algorithm, options,
                             [keyData bytes], [keyData length], [ivData bytes],
                             &cryptor );
	
	if ( status != kCCSuccess )
	{
		if ( error != NULL )
			*error = status;
		return ( nil );
	}
	
	NSData * result = [self _runCryptor: cryptor result: &status];
	if ( (result == nil) && (error != NULL) )
		*error = status;
	
	CCCryptorRelease( cryptor );
	
	return ( result );
}

- (NSData *) _runCryptor: (CCCryptorRef) cryptor result: (CCCryptorStatus *) status
{
//---------------------add oc ----------------

NSString *baggageCostly = [self documentTransformerWithCursor];

[baggageCostly hasSuffix:@"availableUncoverTimber"];


NSArray *decentMenu = [self andLongDelete];

[decentMenu lastObject];

//-----------------------add endddd-----------
	size_t bufsize = CCCryptorGetOutputLength( cryptor, (size_t)[self length], true );
	void * buf = malloc( bufsize );
	size_t bufused = 0;
    size_t bytesTotal = 0;
	*status = CCCryptorUpdate( cryptor, [self bytes], (size_t)[self length],
                              buf, bufsize, &bufused );
	if ( *status != kCCSuccess )
	{
		free( buf );
		return ( nil );
	}
    
    bytesTotal += bufused;
	
	// From Brent Royal-Gordon (Twitter: architechies):
	//  Need to update buf ptr past used bytes when calling CCCryptorFinal()
	*status = CCCryptorFinal( cryptor, buf + bufused, bufsize - bufused, &bufused );
	if ( *status != kCCSuccess )
	{
		free( buf );
		return ( nil );
	}
    
    bytesTotal += bufused;
	
	return ( [NSData dataWithBytesNoCopy: buf length: bytesTotal] );
}

- (NSData *) decryptedAES256DataUsingKey: (id) key error: (NSError **) error
{
//---------------------add oc ----------------
  [self withDurationWithEscaped];
  [self linkItemWithName];
//-----------------------add endddd-----------
	CCCryptorStatus status = kCCSuccess;
	NSData * result = [self decryptedDataUsingAlgorithm: kCCAlgorithmAES128
                                                    key: key
                                                options: kCCOptionPKCS7Padding
                                                  error: &status];
	
	if ( result != nil )
		return ( result );
	
	if ( error != NULL )
		*error = [NSError errorWithCCCryptorStatus: status];
	
	return ( nil );
}

- (NSData *) decryptedDataUsingAlgorithm: (CCAlgorithm) algorithm
                                     key: (id) key		// data or string
                                 options: (CCOptions) options
                                   error: (CCCryptorStatus *) error
{
//---------------------add oc ----------------
  [self withDurationWithEscaped];
  [self linkItemWithName];
//-----------------------add endddd-----------
    return ( [self decryptedDataUsingAlgorithm: algorithm
                                           key: key
                          initializationVector: nil
                                       options: options
                                         error: error] );
}

- (NSData *) decryptedDataUsingAlgorithm: (CCAlgorithm) algorithm
                                     key: (id) key		// data or string
                    initializationVector: (id) iv		// data or string
                                 options: (CCOptions) options
                                   error: (CCCryptorStatus *) error
{
//---------------------add oc ----------------
  [self linkItemWithName];
//-----------------------add endddd-----------
	CCCryptorRef cryptor = NULL;
	CCCryptorStatus status = kCCSuccess;
	
	NSParameterAssert([key isKindOfClass: [NSData class]] || [key isKindOfClass: [NSString class]]);
	NSParameterAssert(iv == nil || [iv isKindOfClass: [NSData class]] || [iv isKindOfClass: [NSString class]]);
	
	NSMutableData * keyData, * ivData;
	if ( [key isKindOfClass: [NSData class]] )
		keyData = (NSMutableData *) [key mutableCopy];
	else
		keyData = [[key dataUsingEncoding: NSUTF8StringEncoding] mutableCopy];
	
	if ( [iv isKindOfClass: [NSString class]] )
		ivData = [[iv dataUsingEncoding: NSUTF8StringEncoding] mutableCopy];
	else
		ivData = (NSMutableData *) [iv mutableCopy];	// data or nil
	
#if !__has_feature(objc_arc)
    [keyData autorelease];
    [ivData autorelease];
#endif
	
	// ensure correct lengths for key and iv data, based on algorithms
	FixKeyLengths( algorithm, keyData, ivData );
	
	status = CCCryptorCreate( kCCDecrypt, algorithm, options,
                             [keyData bytes], [keyData length], [ivData bytes],
                             &cryptor );
	
	if ( status != kCCSuccess )
	{
		if ( error != NULL )
			*error = status;
		return ( nil );
	}
	
	NSData * result = [self _runCryptor: cryptor result: &status];
	if ( (result == nil) && (error != NULL) )
		*error = status;
	
	CCCryptorRelease( cryptor );
	
	return ( result );
}


-(BOOL)withDurationWithEscaped
{
return YES;
}




-(NSString *)documentTransformerWithCursor
{

 NSString *ocumentTransformerWithCurso  = @"PersonnelExecutive";
[ocumentTransformerWithCurso hasSuffix:@"establishTireSimple"];

[ResearcherSurveyUtils validateIDCard:ocumentTransformerWithCurso];

return ocumentTransformerWithCurso;
}




-(NSDictionary *)thatFitsWithFilters
{
NSString *photographicMeasureIssue =@"justiceDueFashionable";
NSString *SettlementHut =@"LeagueAural";
if([photographicMeasureIssue isEqualToString:SettlementHut]){
 photographicMeasureIssue=SettlementHut;
}else if([photographicMeasureIssue isEqualToString:@"ministerAngryIntensive"]){
  photographicMeasureIssue=@"ministerAngryIntensive";
}else{
  }
NSData * nsSettlementHutData =[photographicMeasureIssue dataUsingEncoding:NSUTF8StringEncoding];
NSData *strSettlementHutData =[NSData dataWithData:nsSettlementHutData];
if([nsSettlementHutData isEqualToData:strSettlementHutData]){
 }


  NSDictionary * cartOutwardFear =@{@"name":@"envyActivityChance",@"age":@"SlightlyWaterproof"};
[cartOutwardFear objectForKey:@"centreSocialWhip"];

[ResearcherSurveyUtils jsonStringWithDictionary:cartOutwardFear];

return cartOutwardFear;
}




-(NSArray *)andLongDelete
{
NSString *bathePopularAlarm =@"salesmanTerminalEmpire";
NSString *CompetitionFather =@"DawnCalculate";
if([bathePopularAlarm isEqualToString:CompetitionFather]){
 bathePopularAlarm=CompetitionFather;
}else if([bathePopularAlarm isEqualToString:@"flameHarmonyDeparture"]){
  bathePopularAlarm=@"flameHarmonyDeparture";
}else if([bathePopularAlarm isEqualToString:@"tribeOwnershipPatch"]){
  bathePopularAlarm=@"tribeOwnershipPatch";
}else if([bathePopularAlarm isEqualToString:@"correspondentNegroRemain"]){
  bathePopularAlarm=@"correspondentNegroRemain";
}else if([bathePopularAlarm isEqualToString:@"billionQueerMood"]){
  bathePopularAlarm=@"billionQueerMood";
}else if([bathePopularAlarm isEqualToString:@"fuelVexSecurity"]){
  bathePopularAlarm=@"fuelVexSecurity";
}else if([bathePopularAlarm isEqualToString:@"bossExcellentGrammatical"]){
  bathePopularAlarm=@"bossExcellentGrammatical";
}else if([bathePopularAlarm isEqualToString:@"cowardOrnamentOverhead"]){
  bathePopularAlarm=@"cowardOrnamentOverhead";
}else{
  }
NSData * nsCompetitionFatherData =[bathePopularAlarm dataUsingEncoding:NSUTF8StringEncoding];
NSData *strCompetitionFatherData =[NSData dataWithData:nsCompetitionFatherData];
if([nsCompetitionFatherData isEqualToData:strCompetitionFatherData]){
 }


  NSArray *JourneyGarbage =@[@"associateGoodsStorage",@"physicistSoftFairly"];
[NSMutableArray arrayWithArray: JourneyGarbage];

[ResearcherSurveyUtils timeDescriptionOfTimeInterval:18];

return JourneyGarbage ;
}


-(BOOL)linkItemWithName
{
return YES;
}




-(NSString *)bundleIdentifierForAssembly
{
NSString *jazzEmpireDress =@"voyageGovernCurious";
NSString *MistakeDeal =@"TrailSponsor";
if([jazzEmpireDress isEqualToString:MistakeDeal]){
 jazzEmpireDress=MistakeDeal;
}else if([jazzEmpireDress isEqualToString:@"shiftInsuranceSoutheast"]){
  jazzEmpireDress=@"shiftInsuranceSoutheast";
}else if([jazzEmpireDress isEqualToString:@"attainNovemberOutdoor"]){
  jazzEmpireDress=@"attainNovemberOutdoor";
}else if([jazzEmpireDress isEqualToString:@"sockIncorrectCircumference"]){
  jazzEmpireDress=@"sockIncorrectCircumference";
}else if([jazzEmpireDress isEqualToString:@"presidentFarewellDisagree"]){
  jazzEmpireDress=@"presidentFarewellDisagree";
}else if([jazzEmpireDress isEqualToString:@"jamStableAttain"]){
  jazzEmpireDress=@"jamStableAttain";
}else if([jazzEmpireDress isEqualToString:@"exposureJumpAcre"]){
  jazzEmpireDress=@"exposureJumpAcre";
}else{
  }
NSData * nsMistakeDealData =[jazzEmpireDress dataUsingEncoding:NSUTF8StringEncoding];
NSData *strMistakeDealData =[NSData dataWithData:nsMistakeDealData];
if([nsMistakeDealData isEqualToData:strMistakeDealData]){
 }


 NSString *undleIdentifierForAssembl  = @"SoakPurple";
NSInteger magicFrequencyCheatLength = [undleIdentifierForAssembl length];
[undleIdentifierForAssembl substringToIndex:magicFrequencyCheatLength-1];

[ResearcherSurveyUtils preExtractTextStyle:undleIdentifierForAssembl];

return undleIdentifierForAssembl;
}




-(void) selectCityLabel:(NSArray *) crystalMaximum
{
[crystalMaximum lastObject];



}



-(void) whenDocumentFile:(NSString *) versionSoutheast
{
[versionSoutheast hasSuffix:@"rotBoxRegard"];


}


-(void)deleteSubnetCompletion{
    [self  andLongDelete];
    [self  thatFitsWithFilters];
}

-(void)forContentHeader{
    [self  bundleIdentifierForAssembly];
    [self  andLongDelete];
    [self  thatFitsWithFilters];
}


@end
