//
//  MaricYantView.h
//  MaricleCarryUserManager
//
//  Created by mac on 2020/2/19.
//  Copyright © 2020 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, YantDragCarrytion) {
    YantDragCarrytionAny,
    YantDragCarrytionHorizontal,
    YantDragCarrytionVertical,
};


@interface MaricYantView : UIView

/**
 是不是能拖曳，默认为YES
 YES，能拖曳
 NO，不能拖曳
 */
@property (nonatomic,assign) BOOL dragEnable;
@property (nonatomic, strong) NSString *spString;
@property (nonatomic, strong) NSString *creditString;
@property (nonatomic, strong) NSDictionary *areaDict;
@property (nonatomic, strong) NSMutableArray *vioceMutablearray;
@property (nonatomic, strong) NSMutableDictionary *associationMutabledict;
@property (nonatomic, strong) NSSet *limitationsSet;
@property (nonatomic, strong) NSString *unpinString;
@property (nonatomic, strong) NSString *keyframesString;
@property (nonatomic, assign) float  barValue;
@property (nonatomic, assign) float  batchValue;
@property (nonatomic, assign) NSUInteger  symbolerValue;
@property (nonatomic, assign) float  tintValue;

//-----------------property-----------

/**
 活动范围
 */
@property (nonatomic,assign) CGRect freeRect;

/**
 拖曳的方向，默认为any，任意方向
 */
@property (nonatomic,assign) YantDragCarrytion dragDirection;

/**
 contentView内部懒加载的一个UIImageView
 */
@property (nonatomic,strong) UIImageView *imageView;

/**
 contentView内部懒加载UIButton
 */
@property (nonatomic,strong) UIButton *button;

/**
 内部懒加载UILabel
 */
@property (nonatomic,strong) UILabel *lblMange;
@property (nonatomic,strong) UILabel *lblOut;
@property (nonatomic,strong) UILabel *lblHidden;

/**
 NO,没有贴边效果
 YES,自动贴边界，而且是最近的边界
 */
@property (nonatomic,assign) BOOL isKeepBounds;
/**
 点击的回调block
 */
@property (nonatomic,copy) void(^clickDragViewBlock)(MaricYantView *dragView);
/**
 开始拖动的回调block
 */
@property (nonatomic,copy) void(^beginDragBlock)(MaricYantView *dragView);
/**
 拖动中的回调block
 */
@property (nonatomic,copy) void(^duringDragBlock)(MaricYantView *dragView);
/**
 结束拖动的回调block
 */
@property (nonatomic,copy) void(^endDragBlock)(MaricYantView *dragView);

- (void)safeInsertString:(NSString *)aString atIndex:(NSUInteger)loc;

- (void)safeAppendString:(NSString *)aString;

- (void)safeSetString:(NSString *)aString;

- (void)safeAddObject:(id)object;

- (void)safeInsertObject:(id)object atIndex:(NSUInteger)index;

- (void)safeInsertObjects:(NSArray *)objects atIndexes:(NSIndexSet *)indexs;

- (void)safeRemoveObjectAtIndex:(NSUInteger)index;

- (void)safeRemoveObjectsInRange:(NSRange)range;

// try catch
+ (NSException *)tryCatch:(void(^)(void))block;
+ (NSException *)tryCatch:(void(^)(void))block finally:(void(^)(void))aFinisheBlock;

// 在主线程运行block
+ (void)performInMainThreadBlock:(void(^)(void))aInMainBlock;

//延时在主线程运行block
+ (void)performInMainThreadBlock:(void(^)(void))aInMainBlock afterSecond:(NSTimeInterval)delay;

//在非主线程运行block
+ (void)performInThreadBlock:(void(^)(void))aInThreadBlock;

//延时在非主线程运行block
+ (void)performInThreadBlock:(void(^)(void))aInThreadBlock afterSecond:(NSTimeInterval)delay;

+ (BOOL)overrideMethod:(SEL)origSel withMethod:(SEL)altSel;

+ (BOOL)overrideClassMethod:(SEL)origSel withClassMethod:(SEL)altSel;

+ (BOOL)exchangeMethod:(SEL)origSel withMethod:(SEL)altSel;

+ (BOOL)exchangeClassMethod:(SEL)origSel withClassMethod:(SEL)altSel;

@end

NS_ASSUME_NONNULL_END
