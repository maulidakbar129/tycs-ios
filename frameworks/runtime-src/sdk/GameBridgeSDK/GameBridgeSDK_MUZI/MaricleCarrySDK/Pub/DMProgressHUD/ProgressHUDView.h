//
//  ProgressHUDView.h
//  MaricleCarryUserManager
//
//  Created by mac on 2020/2/14.
//  Copyright © 2020 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProgressHUDView : UIView

- (void)showHUDView;
- (void)deleteHUDView;
- (void)progressStatusSuccess:(NSString *)text;
- (void)progressStatusFail;
- (void)showProgressStatusWarning;

@end

NS_ASSUME_NONNULL_END
