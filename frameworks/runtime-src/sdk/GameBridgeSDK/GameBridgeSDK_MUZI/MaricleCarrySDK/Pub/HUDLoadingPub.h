//
//  HUDLoadingPub.h
//  BesandHundSKY
//
//  Created by pearl on 2019/11/13.
//  Copyright © 2019年 Single. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HUDLoadingPub : NSObject

+ (void)hudLoading;

+ (void)hudDismiss;

+ (void)hudLoadingErrorMsg:(NSString *)string;

+ (void)hudLoadingSuccMsg:(NSString *)string;

+ (void)hudLoadingInfoMsg:(NSString *)string;

+ (BOOL)validateInfoWrite:(NSString *)string;

+ (BOOL)validateMobile:(NSString *)mobile;

+ (BOOL)judgeIdentityStringValid:(NSString *)identityString;

+ (BOOL)isBankCard:(NSString *)cardNum;

// 身份证号码验证
+ (BOOL)validateIDCard:(NSString *)value;

// 手机号
+ (BOOL)validateContactNumber:(NSString *)mobileNumber;

// 车牌
+ (BOOL) validateCarNo:(NSString *)carNoNumber;

// 身份证
+ (BOOL) validateIdentityCard: (NSString *)identityCard;

+ (NSString*)iphoneType;

//实现定时器中的方法
+ (void)getMaricCode:(UIButton *)btn;
+ (UIColor *)colorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue;

/** 从十六进制字符串获取颜色,默认alpha为1 */
+ (UIColor *)colorWithHexString:(NSString *)color;

/** 从十六进制字符串获取颜色，alpha需要自己传递 color:支持@“#123456”、 @“0X123456”、 @“123456”三种格式 */
+ (UIColor *)colorWithHexString:(NSString *)color alpha:(CGFloat)alpha;


@end

NS_ASSUME_NONNULL_END
