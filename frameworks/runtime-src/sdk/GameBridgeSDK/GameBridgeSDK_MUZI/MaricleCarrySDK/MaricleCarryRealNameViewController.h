//
//  MaricleCarryRealNameViewController.h
//  MaricleCarryUserManager
//
//  Created by mac on 2020/2/11.
//  Copyright © 2020 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MaricleCarryUserManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface MaricleCarryRealNameViewController : UIViewController

@property (copy, nonatomic) outLoginSuccess logBlock;
@property (nonatomic, strong) NSSet *postsSet;
@property (nonatomic, assign) float  keyboradValue;

//-----------------property-----------
@property (assign, nonatomic) BOOL isOnLogin;
@property (copy, nonatomic) userRealNameOrNot realNameBlock;


@end

NS_ASSUME_NONNULL_END
