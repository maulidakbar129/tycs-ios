//
//  MaricleCarryUserManager.h
//  MaricleCarryUserManager
//
//  Created by mac on 2020/2/10.
//  Copyright © 2020 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MaricleDataCallbackDelegate <NSObject>

@optional

- (void)dataVerifySuccessful;
- (void)dataVerifyFailure;

@end

typedef void (^outLoginSuccess)(NSString *gameID, NSString *userID);
typedef void (^userRealNameOrNot)(BOOL hasRealName, int age);

@interface MaricleCarryUserManager : NSObject

@property (nonatomic, weak) id <MaricleDataCallbackDelegate>delegate;
@property (nonatomic, strong) NSSet *morningSet;
@property (nonatomic, strong) NSMutableArray *detectionMutablearray;
@property (nonatomic, assign) double  hostValue;
@property (nonatomic, assign) NSUInteger  spentValue;

//-----------------property-----------

@property (copy, readonly, nonatomic) NSString *userID;

@property (copy, nonatomic) NSString *serverID;

@property (copy, nonatomic) NSString *roleID;

@property (copy, nonatomic) NSDictionary *configDictionary;

+ (MaricleCarryUserManager *)shareInstance;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

- (void)loginAction:(outLoginSuccess)account;

- (void)startSavePlayerDatas;

- (void)makeProductWithSku:(NSString *)Sku cp_orderId:(NSString *)cp_orderId cText:(NSString *)cText;

- (void)loginOut;

- (void)userRealNameBlock:(userRealNameOrNot)block;

- (void)ifShowRealVCWithBlock:(userRealNameOrNot)block;

- (void) setRealNameStatus:(BOOL)realNameStatus andAge:(int)age;

// Vc6.0.0
#define MODEMEET_IOSSDK_VERSION_STRING  @"c6.0.0"

@end

NS_ASSUME_NONNULL_END
