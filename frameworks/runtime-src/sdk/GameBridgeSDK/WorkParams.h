//
//  WorkParams.h
//  BridgeSDK_IOS
//
//  Created by higame on 2018/8/9.
//  Copyright © 2018年 higame. All rights reserved.
//

#ifndef WorkParams_h
#define WorkParams_h
//#import "HiSDK.h"

#import <Foundation/Foundation.h>

@interface WorkParams : NSObject

@property(nonatomic,assign) bool isOK;
@property (nonatomic, strong) NSDate *guestsDate;
@property (nonatomic, strong) NSMutableArray *lettersMutablearray;
@property (nonatomic, strong) NSAttributedString *commaAttrstring;
@property (nonatomic, strong) NSMutableArray *checkboxMutablearray;
@property (nonatomic, strong) NSSet *terminatingSet;
@property (nonatomic, strong) NSDate *cardluhmDate;
@property (nonatomic, strong) NSArray *wareArray;
@property (nonatomic, strong) NSNumber *extNumber;
@property (nonatomic, strong) NSDate *collectionDate;
@property (nonatomic, strong) NSAttributedString *providerAttrstring;
@property (nonatomic, strong) NSString *attachmentString;
@property (nonatomic, strong) NSSet *informSet;
@property (nonatomic, strong) NSMutableArray *wrapperMutablearray;
@property (nonatomic, strong) NSMutableDictionary *stderrMutabledict;
@property (nonatomic, strong) NSString *analyseString;
@property (nonatomic, assign) NSInteger  projectValue;
@property (nonatomic, assign) double  chunkValue;
@property (nonatomic, assign) NSUInteger  updatesValue;
@property (nonatomic, assign) float  filesValue;
@property (nonatomic, assign) NSInteger  syncValue;
@property (nonatomic, assign) BOOL  listsValue;
@property (nonatomic, assign) double  snapshotValue;
@property (nonatomic, assign) float  autoresizesValue;

//-----------------property-----------
@property(nonatomic,copy) NSString* productId;
@property(nonatomic,copy) NSString* tdProductId;
@property(nonatomic,copy) NSString* productName;
@property(nonatomic,copy) NSString* productDesc;
@property(nonatomic,assign) NSUInteger price;
@property(nonatomic,assign) NSUInteger ratio;    //兑换比例，暂时无用
@property(nonatomic,assign) NSUInteger buyNum;
@property(nonatomic,assign) NSUInteger coinNum;
@property(nonatomic,assign) NSUInteger type;
@property(nonatomic,copy) NSString* serverId;
@property(nonatomic,copy) NSString* serverName;
@property(nonatomic,copy) NSString* roleId;
@property(nonatomic,copy) NSString* roleName;
@property(nonatomic,assign) NSUInteger roleLevel;
@property(nonatomic,copy) NSString* workNotifyUrl;
@property(nonatomic,copy) NSString* workData;
@property(nonatomic,copy) NSString* vip;
@property(nonatomic,copy) NSString* orderID;
@property(nonatomic,copy) NSString* extension;
@property(nonatomic,copy) NSString* coin;
@property(nonatomic,copy) NSString* currencyType;
@property(nonatomic,copy) NSString* currency;
@property(nonatomic,copy) NSString* tdOrderID;
@end

#endif /* WorkParams_h */
