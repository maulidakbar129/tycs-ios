//
//  SDKTools.h
//  BridgeSDK_IOS
//
//  Created by higame on 2018/8/10.
//  Copyright © 2018年 higame. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "HiSDK.h"

@interface SDKTools : NSObject

+(NSString *)getTime;
+(NSString *)dict2JsonString:(NSDictionary*)dict;
+(NSDictionary*)jsonString2Dict:(NSString*)json;
+(NSString *)objectToJson:(id)obj;
+(NSString*)deviceModelName;
+(NSString*)getIDFA;
+(NSString *)decodedURLString:(NSString *)str;
+(NSString *)encodedURLString:(NSString *)str;
+(NSString *)getWifiSSIDInfo;

@end
