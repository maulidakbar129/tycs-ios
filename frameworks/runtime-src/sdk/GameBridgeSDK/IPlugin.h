//
//  IPlugin.m
//  BridgeSDK_IOS
//
//  Created by higame on 2018/8/7.
//  Copyright © 2018年 higame. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "HiSDK.h"

#import "UserExtraData.h"
#import "WorkParams.h"
#import "ShareParams.h"
#import "BridgeAntiData.h"




typedef enum {
	PLUGIN_TYPE_USER = 1,			//用户登录等插件
	PLUGIN_TYPE_WORK = 2,			//支付插件
	PLUGIN_TYPE_PUSH = 3,			//推送插件
	PLUGIN_TYPE_SHARE = 4,			//分享插件
	PLUGIN_TYPE_ANALYTICS = 5,		//分析统计插件
	PLUGIN_TYPE_DOWNLOAD = 6,		//下载(更新)插件
	PLUGIN_TYPE_ADTRACKING = 7,		//广告追踪
    PLUGIN_TYPE_GENERALPLUGIN = 8,        //广告追踪
    PLUGIN_TYPE_ANTI_ADDiCTION = 9,        //
}PLUGIN_TYPE;


@protocol IPlugin <NSObject>
-(bool) isSupportMethod:(NSString*) methodName;
-(void) callFunction:(int) funcType data:(NSString*) data;
-(NSString*) getPluginDetails;
@end

@protocol IUser <IPlugin>
-(void) loginCustom:(NSString*) customData;
-(void) switchLogin;
-(void) showAccountCenter;
-(void) logout;
-(void) submitExtraData:(UserExtraData*) data;
-(void) exit;
-(void) realNameRegister;
-(void) queryAntiAddiction;
@end



@protocol IWork <IPlugin>
-(int) getWorkType;
-(void) setWorkParams:(NSString*)params;
-(void) work:(WorkParams*) params;
@end


@protocol IAntiAddiction <IPlugin>
-(void) onAntiAddictionData:(BridgeAntiData*)antiData;
-(void) onRealNameAuthenticationResult:(bool)isSupport isAuth:(bool)isAuth age:(int)age uid:(NSString*)uid other:(NSString*)other;
@end
