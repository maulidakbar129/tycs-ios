#pragma once
#include <vector>
using namespace std;

#include "cocos2d.h"
USING_NS_CC;
class ChwCommMapData: public Ref
{

public:
	ChwCommMapData();

	~ChwCommMapData();

	// 初始化地图边界
	void initMap(unsigned int bound);
	void createMap(unsigned int bound);
	void setElementType(int x, int y, int elementType);

		// 获取地图元素类型
	short getElementType(int x, int y);
	//设置点的该类型中的id
	void setElementID(int x, int y, int elementID);

	// 获取地图元素在类型中的id
	short getElementID(int x, int y);

	// 获取地图元素类型
	unsigned int getMapBound();

	// 读取地图信息
	bool loadMapDataWithFile(const std::string& filename);

	// 读取地图信息
	bool saveMapDataToFile(const std::string& filename);


private:


	unsigned char* m_mapData; //地图数据，点的类型
	unsigned char* m_mapID; //地图数据，点在类型中的id
	unsigned int m_dataSize;
	unsigned int m_mapBound;
	unsigned int m_realBound;

};

