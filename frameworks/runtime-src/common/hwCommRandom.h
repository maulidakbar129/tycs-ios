#pragma once

#include "cocos2d.h"
#include <random>
USING_NS_CC;


class ChwCommRandom: public Ref
{
protected:

	std::mt19937             m_generator;
public:

    ChwCommRandom();
    
    void Seed(unsigned int dwSeed);
    
    int Int(int nMin, int nMax);
    double Double(double dMin, double dMax);
	
	std::string GenUUID();
};