#pragma once
#include <vector>
using namespace std;

#include "cocos2d.h"
USING_NS_CC;

const int MAP_BOUND = -1;
const int MAP_WALL = 1;
const int IS_UNKNOWN = 0;
const int IS_OPEN = 1;
const int IS_CLOSE = 2;

#define POSITIVE(x,y) ((((x) - (y))>0)? ((x)-(y)):((y)-(x)))

class Coordinate
{
public:
	int sx;
	int sy;
	Coordinate()
	{
		sx = 0;
		sy = 0;
	}

	int getX() {return sx;}
	int getY() {return sy;}
};

struct PathNode
{
	int iHeapPosition;  //此节点在堆中的位置
	int iG;
	int iH;
	int iF;
	int iColor;  //检测是否可以通过
	int isOpen; //检测是否在开启列表中
	int id;//在该color中的id
	Coordinate father;

	PathNode()
	{
		iHeapPosition = -1;
		iG = -1;
		iH = -1;
		iF = -1;
		iColor = -1;
		isOpen = IS_UNKNOWN;
	}
	int getF()
	{
		return iH + iG;
	}
};

class CHeap
{
public:
	CHeap();
	void setMapData( PathNode** &mapData ){ this->mapData = mapData; } // 设置地图数据
	void removeFront(); //移除堆中的第一个元素
	void push_back(Coordinate element); //往堆中添加元素
	void newHeap(int position);    //当F值改变时，更新堆
	int getSize();
	int getMinF();
	bool empty();
	void clear();
	Coordinate begin();
private:
	vector<Coordinate> v;  
	PathNode** mapData;
};

class ChwCommAStar: public Ref
{

public:
	ChwCommAStar();

	~ChwCommAStar();

	// 初始化地图边界
	void initMapBound( int xLen, int yLen);

	// 设置障碍信息
	void setObstacle( int x, int y);

	void setElementType(int x, int y, int elementType);

	// 清除障碍信息
	void clearObstacle();

	void initSTL ();
	void initMap ();

	// 开始寻路
	void findPath (int Xstart, int Ystart, int Xend, int YEnd, int maxLength);

	// 获取路径长度
	int getPathLength() {return m_iRightRoad.size();}

	// 获取路径点
	Coordinate *getPathPos(int index) {return &m_iRightRoad[index];}

	// 读取地图信息
	bool loadMapData(int xBound, int yBound, const std::string& filename);
	// 获取地图元素类型
	short getElementType(int x, int y);

	//设置点的该类型中的id
	void setElementID(int x, int y, int elementID);

	// 获取地图元素在类型中的id
	short getElementID(int x, int y);
	
	int getPath(lua_State *L);
private:
	int m_StartX;
	int m_StartY;
	int m_EndX;
	int m_EndY;
	bool m_isFind;	// 检测是否发现了正确的路径

	int m_XBound;	// 地图X边界
	int m_YBound;	// 地图Y边界

	PathNode** m_MapData; // 地图数据

	CHeap m_iOpenList; //开启列表
	std::vector<Coordinate> m_iRightRoad; //储存找到的路径
};

