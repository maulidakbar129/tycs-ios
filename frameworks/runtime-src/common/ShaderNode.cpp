#include "ShaderNode.h"

#include "cocos2d.h"

USING_NS_CC;

///---------------------------------------
// 
// ShaderNode
// 
///---------------------------------------

ShaderNode::ShaderNode()
:_resolution(Vec2(0.0f, 0.0f))
,_time(0.0f)
,_texture2d(nullptr)
{
}

ShaderNode::~ShaderNode()
{
}

ShaderNode* ShaderNode::shaderNodeWithVertex(const std::string &vert, const std::string& frag, cocos2d::Texture2D* _texture)
{
    auto node = new (std::nothrow) ShaderNode();
    node->initWithVertex(vert, frag);
	node->setTexture2D(_texture);
    node->autorelease();

    return node;
}

bool ShaderNode::initWithVertex(const std::string &vert, const std::string &frag)
{
    _vertFileName = vert;
    _fragFileName = frag;

    loadShaderVertex(vert, frag);

    _time = 0;
	
	auto glView = Director::getInstance()->getOpenGLView();
	auto frameSize = glView->getFrameSize();

	_resolution = Vec2(frameSize.width, frameSize.height);

    scheduleUpdate();

	auto screenSize = Director::getInstance()->getWinSize();

    setContentSize(Size(screenSize.width, screenSize.height));
    setAnchorPoint(Vec2(0.5f, 0.5f));
    
    return true;
}

void ShaderNode::setResolution(int width, int height)
{
	_resolution = Vec2(width, height);
}

void ShaderNode::loadShaderVertex(const std::string &vert, const std::string &frag)
{
    auto fileUtiles = FileUtils::getInstance();

    // frag
    auto fragmentFilePath = fileUtiles->fullPathForFilename(frag);
    auto fragSource = fileUtiles->getStringFromFile(fragmentFilePath);

    // vert
    auto vertexFilePath = fileUtiles->fullPathForFilename(vert);
    auto vertSource = fileUtiles->getStringFromFile(vertexFilePath);
    
    auto glprogram = GLProgram::createWithByteArrays(vertSource.c_str(), fragSource.c_str());
    auto glprogramstate = GLProgramState::getOrCreateWithGLProgram(glprogram);
    setGLProgramState(glprogramstate);
}

void ShaderNode::update(float dt)
{
    _time += dt;
}

void ShaderNode::setPosition(const Vec2 &newPosition)
{
    Node::setPosition(newPosition);
}

void ShaderNode::setTexture2D(Texture2D* texture2d)
{
	_texture2d = texture2d;
}

void ShaderNode::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
    _customCommand.init(_globalZOrder, transform, flags);
    _customCommand.func = CC_CALLBACK_0(ShaderNode::onDraw, this, transform, flags);
    renderer->addCommand(&_customCommand);
}

void ShaderNode::onDraw(const Mat4 &transform, uint32_t flags)
{
	auto size = getContentSize();
    float w = size.width, h = size.height;
    GLfloat vertices[12] = {0,0, w,0, w,h, 0,0, 0,h, w,h};

    auto glProgramState = getGLProgramState();

	glProgramState->setVertexAttribPointer("a_position", 2, GL_FLOAT, GL_FALSE, 0, vertices);

    glProgramState->setUniformVec2("resolution", _resolution);
	glProgramState->setUniformFloat("time", _time);

	glProgramState->setUniformTexture("tex0", _texture2d);

    glProgramState->apply(transform);

    glDrawArrays(GL_TRIANGLES, 0, 6);
    
    CC_INCREMENT_GL_DRAWN_BATCHES_AND_VERTICES(1,6);
}

