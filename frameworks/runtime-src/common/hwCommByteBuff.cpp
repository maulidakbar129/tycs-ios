#include "hwCommByteBuff.h"

ChwCommByteBuff::ChwCommByteBuff(unsigned int size):
	m_dwSize(size),
	m_dwCapacity(0),
	m_dwReadIndex(0),
	m_dwWriteIndex(0),
	m_pBuffer(new char[size])
{

}

ChwCommByteBuff::ChwCommByteBuff(const ChwCommByteBuff& buf)
{
	m_dwCapacity = buf.m_dwCapacity;
	m_dwReadIndex = buf.m_dwReadIndex;
	m_dwSize = buf.m_dwSize;
	m_dwWriteIndex = buf.m_dwWriteIndex;

	m_pBuffer = new char[buf.m_dwSize];
	memcpy(m_pBuffer, buf.m_pBuffer, buf.m_dwSize);
}

ChwCommByteBuff::ChwCommByteBuff():
	m_dwSize(0),
	m_dwCapacity(0),
	m_dwReadIndex(0),
	m_dwWriteIndex(0),
	m_pBuffer(nullptr)
{

}

ChwCommByteBuff::ChwCommByteBuff(const char* pBuf, unsigned int dwLength):
	m_dwCapacity(0),
	m_dwReadIndex(0),
	m_dwWriteIndex(0)
{

	m_dwSize = dwLength;
	m_pBuffer = new char[dwLength];
	memcpy(m_pBuffer, pBuf, dwLength);
}

ChwCommByteBuff::~ChwCommByteBuff()
{
	if (m_pBuffer != nullptr)
	{	
		delete[] m_pBuffer;
		m_pBuffer = nullptr;
	}
}

void ChwCommByteBuff::ReadIndex(unsigned int index)
{
	m_dwReadIndex = index;
}

void ChwCommByteBuff::WriteIndex(unsigned int index)
{
	m_dwWriteIndex = index;
}

unsigned int ChwCommByteBuff::GetSize()
{
	return m_dwSize;
}

void ChwCommByteBuff::ReSize(unsigned int size)
{
	if(size <= 0)  
	{  
		return;  
	}  

	if(m_dwSize > size)
	{
		return;
	}  

	char* pNewBuff = new char[size];  //为改变大小后的新数组分配空间, 多分配一些 
	memset(pNewBuff, 0, size);

	auto tmpSize = std::min(m_dwSize, size);
	memcpy(pNewBuff, m_pBuffer, tmpSize);

	delete[] m_pBuffer; 	//释放原数组占用的空间  
	m_pBuffer = pNewBuff;  	//设置重新分配后的新数组  
	m_dwSize = size;
	m_dwCapacity = size;
}

unsigned int ChwCommByteBuff::Read(void* pBuff, unsigned int dwLength)
{
	if(m_dwReadIndex + dwLength > m_dwSize)
	{
		ReSize(m_dwReadIndex + dwLength);
	}

	memcpy(pBuff, m_pBuffer + m_dwReadIndex, dwLength);
	m_dwReadIndex += dwLength;
	return dwLength;
}

void ChwCommByteBuff::ReadBuf(ChwCommByteBuff* pBuf)
{
	unsigned int bufLength = ReadDWORD();
	pBuf->ReSize(bufLength);
	Read(pBuf->m_pBuffer, bufLength);
}

unsigned char ChwCommByteBuff::ReadBYTE(void)
{
	unsigned char chValue = 0;
	Read(&chValue, sizeof(unsigned char));
	return chValue;
}

char ChwCommByteBuff::ReadChar(void)
{
	char cValue = 0;
	Read(&cValue, sizeof(char));
	return cValue;
}

unsigned short ChwCommByteBuff::ReadWORD(void)
{
	unsigned short wValue = 0;
	Read(&wValue, sizeof(unsigned short));
	return wValue;
}

unsigned int ChwCommByteBuff::ReadDWORD(void)
{
	unsigned int dwValue = 0;
	Read(&dwValue, sizeof(unsigned int));
	return dwValue;
}

unsigned long ChwCommByteBuff::ReadLong(void)
{
	unsigned long dwValue = 0;
	Read(&dwValue, sizeof(unsigned long));
	return dwValue;
}

int ChwCommByteBuff::ReadInt(void)
{
	int nValue = 0;
	Read(&nValue, sizeof(int));
	return nValue;
}

float ChwCommByteBuff::ReadFloat(void)
{
	float fValue = 0;
	Read(&fValue, sizeof(float));
	return fValue;
}

double ChwCommByteBuff::ReadDouble(void)
{
	double dValue = 0;
	Read(&dValue, sizeof(double));
	return dValue;
}

std::string ChwCommByteBuff::ReadString(void)
{
	std::string strValue;
	unsigned char byLength = ReadBYTE();
	if(byLength == 0)
	{
		return strValue;
	}
	strValue.resize(byLength);
	Read(&strValue[0], byLength);
	return strValue;
}

std::string ChwCommByteBuff::ReadText(void)
{
	std::string strValue;
	unsigned int iLength = ReadDWORD();
	if(iLength == 0)
	{
		return strValue;
	}
	strValue.resize(iLength);
	Read(&strValue[0], iLength);
	return strValue;
}

void ChwCommByteBuff::Write(const char* pBuffer, unsigned int dwLength)
{
	if(!pBuffer)
	{
		return;
	}

	if(m_dwWriteIndex + dwLength > m_dwSize)
	{
		ReSize(m_dwWriteIndex + dwLength);
	}

	memcpy(m_pBuffer + m_dwWriteIndex, pBuffer, dwLength);
	m_dwWriteIndex += dwLength;
}

void ChwCommByteBuff::WriteBuf(ChwCommByteBuff* pBuf)
{
	WriteDWORD(pBuf->m_dwSize);
	Write(pBuf->m_pBuffer, pBuf->m_dwSize);
}

void ChwCommByteBuff::WriteBYTE(unsigned char byValue)
{
	Write((char*)&byValue, sizeof(unsigned char));
}

void ChwCommByteBuff::WriteChar(char cValue)
{
	Write((char*)&cValue, sizeof(char));
}

void ChwCommByteBuff::WriteWORD(unsigned short wValue)
{
	Write((char*)&wValue, sizeof(unsigned short));
}

void ChwCommByteBuff::WriteDWORD(unsigned int dwValue)
{
	Write((char*)&dwValue, sizeof(unsigned int));
}

void ChwCommByteBuff::WriteLong(unsigned long dwValue)
{
	Write((char*)&dwValue, sizeof(unsigned long));
}

void ChwCommByteBuff::WriteInt(int nValue)
{
	Write((char*)&nValue, sizeof(int));
}

void ChwCommByteBuff::WriteFloat(float fValue)
{
	Write((char*)&fValue, sizeof(float));
}

void ChwCommByteBuff::WriteDouble(double dValue)
{
	Write((char*)&dValue, sizeof(double));
}

void ChwCommByteBuff::WriteString(const char* pString)
{
	if(!pString)
	{
		return;
	}

	unsigned char byLength = (unsigned char)strlen(pString);
	WriteBYTE(byLength);
	Write(pString, byLength);
}

void ChwCommByteBuff::WriteText(const char* pString)
{
	if(!pString)
	{
		return;
	}

	unsigned int iLength = (unsigned int)strlen(pString);
	WriteDWORD(iLength);
	Write(pString, iLength);
}
