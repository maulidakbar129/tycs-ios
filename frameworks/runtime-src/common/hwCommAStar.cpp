#include "hwCommAStar.h"
#include <algorithm>

extern "C" {
#include "lua.h"
#include "tolua++.h"
#include "lualib.h"
#include "lauxlib.h"
}

using namespace std;

//构造函数
CHeap::CHeap()
{
}

//判断二叉堆是否为空
bool CHeap::empty ()
{
	if (v.size () == 0)
		return true;
	else
		return false;
}

//清空二叉堆
void CHeap::clear()
{
	if (v.size () != 0)
		v.clear ();
}

//返回二叉堆中的第一个元素（即F值最低的元素）
Coordinate CHeap::begin()
{
	return v[0];
}

//删除二叉堆的堆首元素（即F值最低的元素）
void CHeap::removeFront ()
{
	if (v.size() == 0)
		return ;
	
	mapData[v[v.size() - 1].sx][v[v.size() - 1].sy].iHeapPosition=0;
	mapData[v[0].sx][v[0].sy].iHeapPosition=-1;

	v[0] = v[v.size() - 1];      //用最后一个元素把第一个元素覆盖掉，即为删除
	v.pop_back();                //删除容器尾巴元素

	

	int currentIndex = 0;
	while (currentIndex < v.size()) //把新的堆首元素放在堆中适当的位置
	{
		int leftChildIndex = 2 * currentIndex + 1;
		int rightChildIndex = 2 * currentIndex + 2;

		//已经到最底层，结束
		if (rightChildIndex > v.size())
			break;
		int minIndex = leftChildIndex;
		
		//有两个孩子，找出两个孩子节点中F值最低的元素
		if (rightChildIndex<v.size() && (mapData[v[minIndex].sx][v[minIndex].sy].getF()  > mapData[v[rightChildIndex].sx][v[rightChildIndex].sy].getF()))
		{
			minIndex = rightChildIndex;
		}
		
		//如果当前节点的F值 大于 他孩子节点的F值，则交换
		if (mapData[v[currentIndex].sx][v[currentIndex].sy].getF() > mapData[v[minIndex].sx][v[minIndex].sy].getF())
		{
			Coordinate temp = v[minIndex];
			v[minIndex] = v[currentIndex];
			v[currentIndex] = temp;

			//同步保存地图中该坐标在堆中的最新位置
			mapData[v[currentIndex].sx][v[currentIndex].sy].iHeapPosition = currentIndex; 
            mapData[v[minIndex].sx][v[minIndex].sy].iHeapPosition = minIndex;
			currentIndex = minIndex;
		}
		else
		{
			break;
		}
	}
}

//返回堆中的最小F值
int CHeap::getMinF() 
{
	if (v.size() > 0)
		return mapData[v[0].sx][v[0].sx].getF();
	else
		return 0;
}


//往堆中添加新的元素（节点）
void CHeap::push_back (Coordinate element)
{
	v.push_back(element);//把新节点添加到堆的末尾
	int currentIndex = v.size() - 1;
	mapData[v[currentIndex].sx][v[currentIndex].sy].iHeapPosition = currentIndex; //保存该坐标在堆中的位置

	while (currentIndex > 0) //不断的与他的父节点比较，直到该新节点的F值大于他的父节点的F值为止 或者 该新节点到了堆首
	{
		int parentIndex = (currentIndex - 1) / 2;
		if (mapData[v[currentIndex].sx][v[currentIndex].sy].getF() < mapData[v[parentIndex].sx][v[parentIndex].sy].getF())
		{
			Coordinate temp = v[currentIndex];
			v[currentIndex] = v[parentIndex];
			v[parentIndex] = temp;

			//同步保存地图中该坐标在堆中的最新位置
			mapData[v[currentIndex].sx][v[currentIndex].sy].iHeapPosition = currentIndex;
			mapData[v[parentIndex].sx][v[parentIndex].sy].iHeapPosition = parentIndex;
			currentIndex = parentIndex;
			continue;
		}
		else
		{
			break;
		}
	}
}

//当堆中某元素的F值发生改变时，更新该元素在堆中的位置
void CHeap::newHeap (int position)
{
	int currentIndex = position;
	int parentIndex;
	//while (currentIndex > 0) //如果该元素新的F值比他的父节点的F值小，交换
	//{
	//	parentIndex = (currentIndex - 1) / 2;
	//	if (mapData[v[currentIndex].sx][v[currentIndex].sy].getF()  < mapData[v[parentIndex].sx][v[parentIndex].sy].getF())
	//	{
	//		Coordinate temp = v[currentIndex];
	//		v[currentIndex] = v[parentIndex];
	//		v[parentIndex] = temp;
	//		mapData[v[currentIndex].sx][v[currentIndex].sy].iHeapPosition = parentIndex;
	//		mapData[v[parentIndex].sx][v[parentIndex].sy].iHeapPosition = currentIndex;
	//		currentIndex = parentIndex;
	//		continue;
	//	}
	//	else
	//	{
	//		break;
	//	}
	//}
	Coordinate temp;
	//如果上面的循环没有执行，则无法判断该节点的最新F值的相对大小，
	//所以，此时需要把该节点移动到堆首删除掉，然后再在堆末尾添加该节点，最后程序再把该节点移动到堆中适当的位置处
	if (currentIndex == position) 
	{
		while (currentIndex > 0) //把该节点移动到堆首
		{
			parentIndex = (currentIndex - 1) / 2;
			temp = v[currentIndex];
			v[currentIndex] = v[parentIndex];
			v[parentIndex] = temp;
			mapData[v[currentIndex].sx][v[currentIndex].sy].iHeapPosition = currentIndex;
			mapData[v[parentIndex].sx][v[parentIndex].sy].iHeapPosition = parentIndex;
			currentIndex = parentIndex;
		}
	}
	temp = v[0];
	removeFront (); //删除该节点
	push_back (temp);//重新在堆中插入该节点
}

//返回堆中元素的个数
int CHeap::getSize ()
{
	return v.size();
}

//构造函数
ChwCommAStar::ChwCommAStar()
{
	m_StartX = m_StartY = 0;
	m_EndX = m_EndY = 0;
	m_XBound = m_YBound = 0;
	m_MapData = NULL; 
}

// 析构函数
ChwCommAStar::~ChwCommAStar()
{
	if (m_MapData!=NULL)
	{
		// 删除旧的
		for (int i = 0; i <m_XBound; ++i)
		{
			delete[] m_MapData[i];
		}
		delete[] m_MapData;
	}
	m_MapData = NULL;
}


void ChwCommAStar::initMapBound( int xLen, int yLen)
{
	if (m_MapData!=NULL)
	{
		// 删除旧的
		for (int i = 0; i <m_XBound; ++i)
		{
			delete[] m_MapData[i];
		}
		delete[] m_MapData;
	}

	// 初始化新的
	m_XBound = xLen;
	m_YBound = yLen;
	m_MapData = new PathNode*[xLen];
	for (int i = 0; i <xLen; ++i)
	{
		m_MapData[i] = new PathNode[yLen];
	}
	m_iOpenList.setMapData(m_MapData);
}

// 设置障碍信息
void ChwCommAStar::setObstacle( int x, int y)
{
	if (x<m_XBound && y<m_YBound)
	{
		m_MapData[x][y].iColor = MAP_WALL;
	}
}

// 清除障碍信息
void ChwCommAStar::clearObstacle()
{
	for (int x = 0; x < m_XBound; ++x)
	{
		for (int y = 0; y < m_YBound; ++y)
		{
			m_MapData[x][y].iColor = 0;
		}
	}
}

//初始化寻路需要用到的STL容器
void ChwCommAStar::initSTL ()
{
	if (!m_iOpenList.empty()) //清空开启列表
		m_iOpenList.clear();

	if (!m_iRightRoad.empty()) //清空储存的路径
		m_iRightRoad.clear();
}


//每次寻路前，清空地图中每个格子储存的相关数据
void ChwCommAStar::initMap () 
{
	for (int x = 0; x < m_XBound; ++x)
	{
		for (int y = 0; y < m_YBound; ++y)
		{
			m_MapData[x][y].iHeapPosition = -1;
			m_MapData[x][y].iH = -1;
			m_MapData[x][y].iG = -1;
			m_MapData[x][y].iF = -1;
			m_MapData[x][y].isOpen = IS_UNKNOWN;
			m_MapData[x][y].father.sx = -1;
            m_MapData[x][y].father.sy = -1;
		}
	}
}

//选路算法
void ChwCommAStar::findPath (int Xstart, int Ystart, int Xend, int Yend, int maxLength)
{
	initSTL ();
	initMap ();
    Coordinate coordinate;
	Coordinate minCoord;
	coordinate.sx = Xstart;
	coordinate.sy = Ystart; 
	m_MapData[Xstart][Ystart].iH = (POSITIVE(Xend, Xstart)*10) + (POSITIVE(Yend, Ystart)*10);
	m_MapData[Xstart][Ystart].iG = 0;
	m_MapData[Xstart][Ystart].iF = m_MapData[Xstart][Ystart].getF();

	// 1. 把起始格添加到开启列表
	m_iOpenList.push_back(coordinate); 
	m_MapData[Xstart][Ystart].isOpen = IS_OPEN; 

	//2.重复如下工作
	int minF = m_MapData[Xstart][Ystart].iF ; //该变量表示最小F值
    m_isFind = false; //该变量，用来检测目标位置是否被添加进了关闭列表

	int maxF = maxLength*10;

    while(!m_iOpenList.empty()) //当开启列表为空时， 循环停止
    {
	    //a).寻找开启列表 中F最低的格子，我们称它为当前格
		minCoord = m_iOpenList.begin();
		auto pos = m_MapData[minCoord.sx][minCoord.sy];
		minF = pos.getF();
		if ((minCoord.sx == Xend) && (minCoord.sy == Yend)) //如果目标位置被添加到了关闭容器，直接跳出循环
		{
			m_isFind = true;
			break; 
		}
		if ( minF > maxF )
		{
			break;
		}

		//b).把遍历得到的当前格切换到关闭列表
		m_iOpenList.removeFront(); //删除开启列表中的F值最低的格子
		m_MapData[minCoord.sx][minCoord.sy].isOpen = IS_CLOSE;//把该格子的标记设置为“在关闭列表中”


		int i = -1, j = -1;
		int x = 0, y = 0;
		for (i = -1; i < 2; ++i)  //遍历当前格子周围的其他8个格子
		{
			for (j = -1; j < 2; ++j)
			{
				x = minCoord.sx + i;
				y = minCoord.sy + j;

				if ((x < 0) || (y < 0) || (x > m_XBound - 1) || (y > m_YBound - 1)) //* 坐标超界，略过
					continue;
				if (m_MapData[x][y].iColor == MAP_WALL) //* 格子为墙，不能通过，略过
					continue;
				if (m_MapData[x][y].isOpen == IS_CLOSE) //* 格子已经在关闭列表中了，略过
					continue;
				if (m_MapData[x][y].isOpen == IS_UNKNOWN) //* 如果该格子未加入到开启列表，
				{
					coordinate.sx = x;
					coordinate.sy = y;
					m_MapData[x][y].father = minCoord;  //当前格作为这一格的父节点
					m_MapData[x][y].iH = (POSITIVE(Xend, x)*10) + (POSITIVE(Yend, y)*10);
					if((i == 0) || (j == 0))
					{
						m_MapData[x][y].iG = m_MapData[minCoord.sx][minCoord.sy].iG + 10;  
					}
					else
					{
						m_MapData[x][y].iG = m_MapData[minCoord.sx][minCoord.sy].iG + 14; 
					}
					m_MapData[x][y].iF = m_MapData[x][y].getF();
					m_MapData[x][y].isOpen = IS_OPEN;
					m_iOpenList.push_back(coordinate);  //把他加入到开启列表中
					continue;
				}
				if (m_MapData[x][y].isOpen == IS_OPEN) //* 如果该格子已经在开启列表中
				{
					int newG;
					if((i == 0) || (j == 0))  //计算新的G值
					{
						newG = m_MapData[minCoord.sx][minCoord.sy].iG + 10;   
					}
					else
					{
						newG = m_MapData[minCoord.sx][minCoord.sy].iG + 14; 
					}
					if (newG < m_MapData[x][y].iG) //如果新的G值比先前的更好
					{
						m_MapData[x][y].father = minCoord; //把该格的父节点改为当前格
						int position = m_MapData[x][y].iHeapPosition; //得到该节点在堆中的位置
						m_MapData[x][y].iG = newG;
						m_MapData[x][y].iF = m_MapData[x][y].getF();    
						m_iOpenList.newHeap(position);             //由于F值发生了变化，更新此节点在堆中的位置
					}
				}
			}
		}	
	}
	if (m_isFind) //3).如果找到路径了，保存路径
	{
		PathNode tempNode;
		tempNode.father = minCoord;
		while (true)
		{
			if ((tempNode.father.sx == -1) && (tempNode.father.sy == -1)){
				break;
			}
			m_iRightRoad.push_back(tempNode.father);
            tempNode.father = m_MapData[tempNode.father.sx][tempNode.father.sy].father;
		}

		// 保存的路径是反向的，因此需要反转回来
		reverse(m_iRightRoad.begin(),m_iRightRoad.end());
	}
	else
	{
		m_StartX = m_StartY = -1;
		m_EndX = m_EndY = -1;
	}	
}

bool ChwCommAStar::loadMapData(int xBound, int yBound, const std::string& filename)
{
	// 初始化表
	initMapBound(xBound, yBound);

	do
	{
		Data data = FileUtils::getInstance()->getDataFromFile(filename.c_str());
		if (data.getSize() < xBound * xBound)
		{
			return false;
		}
		unsigned char* buffer = data.getBytes();
		unsigned char *map = buffer + 1;
		int bound = *buffer;
		int dataSize = (2*bound+1) * (2*bound+1);
		for (int i = 0; i < m_XBound; i++)
		{
			for (int j = 0; j < m_YBound; j++)
			{
				m_MapData[i][j].iColor = (int)*(map + i*m_YBound + j);
				if(data.getSize() == (dataSize * 2 + 1))
					m_MapData[i][j].id     = (int)*(map + dataSize + i*m_YBound + j);
				else
					m_MapData[i][j].id     = 1;
			}
		}
	} while (0);

	return true;
}

void ChwCommAStar::setElementType(int x, int y, int elementType)
{
	if (x < m_XBound && y < m_YBound)
	{
		m_MapData[x][y].iColor = elementType;
	}
}

short ChwCommAStar::getElementType(int x, int y)
{
	if(x < 0 || x >= m_XBound ||
		y < 0 || y >= m_XBound)
	{
		return MAP_BOUND;
	}
	return m_MapData[x][y].iColor;
}

void ChwCommAStar::setElementID(int x, int y, int elementID)
{
	if (x < m_XBound && y < m_YBound)
	{
		m_MapData[x][y].id = elementID;
	}
}

short ChwCommAStar::getElementID(int x, int y)
{
	if(x < 0 || x >= m_XBound ||
		y < 0 || y >= m_XBound)
	{
		return MAP_BOUND;
	}
	return m_MapData[x][y].id;
}

int ChwCommAStar::getPath(lua_State *L)
{
	lua_newtable(L);   
	for (unsigned int i = 0; i < m_iRightRoad.size(); ++i)
	{
		lua_pushnumber(L, i + 1);
		
		lua_newtable(L); 
		lua_pushstring(L, "x");                             /* L: table key */
		lua_pushnumber(L, (lua_Number)m_iRightRoad[i].getX());               /* L: table key value*/
		lua_rawset(L, -3);                                  /* table[key] = value, L: table */
		lua_pushstring(L, "y");                             /* L: table key */
		lua_pushnumber(L, (lua_Number)m_iRightRoad[i].getY());               /* L: table key value*/
		lua_rawset(L, -3);

		lua_rawset(L, -3);
	}
	return 1;
}