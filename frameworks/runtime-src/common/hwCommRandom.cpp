#include "hwCommRandom.h"

#include "native/CCNative.h"

using namespace std;

ChwCommRandom::ChwCommRandom()
{	
	// ���ó�ʼ����
	std::random_device rd;
	m_generator.seed(rd());
}

void ChwCommRandom::Seed(unsigned int dwSeed)
{
    m_generator.seed(dwSeed);
}

int ChwCommRandom::Int(int nMin, int nMax)
{
	std::uniform_int_distribution<> rd(nMin, nMax);
	return rd(m_generator);
}

double ChwCommRandom::Double(double dMin, double dMax)
{
	std::uniform_real_distribution<> rd(dMin, dMax);
	return rd(m_generator);
}

std::string ChwCommRandom::GenUUID()
{
	return (string)cocos2d::extra::Native::getUDID();
}
