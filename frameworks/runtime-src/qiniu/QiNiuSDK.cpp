#include "HW_QiNiuSDK.h"

#include "cocos2d.h"
using namespace cocos2d;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include "json/rapidjson.h"
#include "json/writer.h"
#include "json/document.h"
#include "json/prettywriter.h"
#include "json/stringbuffer.h"
#include <stdlib.h>
#include<iostream>
#include<sstream>
#include "base/ccUTF8.h"

#include "HiGameSDK.h"
#include "CCLuaEngine.h"
#include "platform/android/jni/JniHelper.h"
#include <jni.h>

#define QiNiuClassName "org/cocos2dx/utils/QiNiu"


using namespace cocos2d;

extern "C" {

	JNIEXPORT void JNICALL Java_org_cocos2dx_utils_QiNiu_QiNiuUpdateCallback(JNIEnv *, jclass, jint juploadid, jint jresultCode, jstring jdata)
	{

		std::string data = StringUtils::getStringUTFCharsJNI(JniHelper::getEnv(), jdata);
		int resultCode = jresultCode;
		int uploadid = juploadid;
		QiNiuSDK::getInstance()->onCallback(uploadid, resultCode, data);
	}
}

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#include "QiNiuSDK_Interface.h"



#else

#include "io.h"
#include "rs.h"
#include <stdlib.h>


#endif


UploadCallback::UploadCallback(const onUploadCallback& callback)
{
    m_callback = callback;
}

void UploadCallback::onCallback(int callbackCode, int resultCode, const char *data)
{
    if(m_callback){
        std::string d = std::string(data);
        m_callback(callbackCode, resultCode, d);
    }
}


static QiNiuSDK* sharedQiNiuSDK = nullptr;
QiNiuSDK* QiNiuSDK::getInstance()
{
	if (!sharedQiNiuSDK) {
		sharedQiNiuSDK = new (std::nothrow) QiNiuSDK();
	}
	return sharedQiNiuSDK;
}

void QiNiuSDK::destroyInstance()
{
	if (sharedQiNiuSDK != nullptr)
	{
		CC_SAFE_DELETE(sharedQiNiuSDK);
	}
}

QiNiuSDK::QiNiuSDK():
	_uploadid(0)
	,_maxFileSize(1024*1024)
	,_minCompress(50)
	,_maxWidth(1136)
	,_maxHeight(640)
	,_compressFormat(0)
    ,_UploadCallback(nullptr)
{

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    _UploadCallback = new UploadCallback([=](int uploadid, int retcode, std::string key) {
        
        onCallback(uploadid, retcode, key);

    });
    QiNiuSDK_Interface::setUploadCallback(_UploadCallback);
    
#endif
}

QiNiuSDK::~QiNiuSDK()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    if(_UploadCallback){
        delete _UploadCallback;
    }
    
    QiNiuSDK_Interface::setUploadCallback(nullptr);
    
#endif
}

void QiNiuSDK::uploadImage(std::string& token, int type, std::string& filePath, uploadCallback callback)
{
	_uploadid += 1;
	_callbacks[_uploadid] = callback;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniHelper::callStaticVoidMethod(QiNiuClassName, "startUploadImageFile", _uploadid, token, type, filePath);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    QiNiuSDK_Interface::uploadImage(_uploadid, token, type, filePath);
    
#else

	Qiniu_Global_Init(-1);

	Qiniu_Io_PutRet putRet;
	Qiniu_Client client;
	Qiniu_RS_PutPolicy putPolicy;
	Qiniu_Io_PutExtra putExtra;


	char *accessKey = "1BBinQG69GF20vePEvWB-VC67SLoQFGGp0XjjQVJ";
	char *secretKey = "pGEyuuI-6r3dMUZK8DBySLora8ea7mferHi-OJ2y";

	Qiniu_Mac mac;
	mac.accessKey = accessKey;
	mac.secretKey = secretKey;

	Qiniu_Zero(putExtra);

	Qiniu_Use_Zone_Huadong(Qiniu_True);

	Qiniu_Client_InitMacAuth(&client, 1024, &mac);
	Qiniu_Error error = Qiniu_Io_PutFile(&client, &putRet, token.c_str(), NULL, filePath.c_str(), &putExtra);
	if (error.code != 200) {
		onCallback(_uploadid, error.code, std::string(""));
	}
	else {
		printf("key:\t%s\n", putRet.key);
		printf("hash:\t%s\n", putRet.hash);
		onCallback(_uploadid, 0, std::string(putRet.key));
	}

	Qiniu_Client_Cleanup(&client);

#endif
}


void QiNiuSDK::onCallback(unsigned int uploadid, int retCode, std::string& msg)
{
	if (_callbacks[uploadid]) {

		Scheduler *sched = Director::getInstance()->getScheduler();
		sched->performFunctionInCocosThread([=]() {

			_callbacks[uploadid](retCode, msg);
			_callbacks.erase(uploadid);
		});
	}
}


void QiNiuSDK::setMaxFileSize(int maxFileSize)
{
	_maxFileSize = maxFileSize;
	updateOptions();
}

int QiNiuSDK::getMaxFileSize()
{
	return _maxFileSize;
}

void QiNiuSDK::setMinCompress(int minCompress)
{
	_minCompress = minCompress;
	updateOptions();
}

int QiNiuSDK::getMinCompress()
{
	return _minCompress;
}

void QiNiuSDK::setMaxWidth(int maxWidth)
{
	_maxWidth = maxWidth;
	updateOptions();
}

int QiNiuSDK::getMaxWidth()
{
	return _maxWidth;
}

void QiNiuSDK::setMaxHeight(int maxHeight)
{
	_maxHeight = maxHeight;
	updateOptions();
}

int QiNiuSDK::getMaxHeight()
{
	return _maxHeight;
}

void QiNiuSDK::setCompressFormat(int compressFormat)
{
	_compressFormat = compressFormat;
	updateOptions();
}

int QiNiuSDK::getCompressFormat()
{
	return _compressFormat;
}

void QiNiuSDK::updateOptions()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniHelper::callStaticVoidMethod(QiNiuClassName, "setUploadOptions", getMaxFileSize(), getMinCompress(), getMaxWidth(), getMaxHeight(), getCompressFormat());
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    QiNiuSDK_Interface::setUploadOptions(getMaxFileSize(), getMinCompress(), getMaxWidth(), getMaxHeight(), getCompressFormat());
#else

#endif
}
