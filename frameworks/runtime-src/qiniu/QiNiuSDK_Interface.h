//
//  UpImageSDKiOS.h
//  libhigame
//
//  Created by higame on 2018/11/2.
//  Copyright © 2018年 higame. All rights reserved.
//

#ifndef QiNiuSDKInterface_h
#define QiNiuSDKInterface_h

#include <string>
#include "HW_QiNiuSDK.h"

class QiNiuSDK_Interface
{
    
public:

    static void uploadImage(int uploadid, std::string token, int optype, std::string filePath);
    static void setUploadCallback(UploadCallback* callback);
    static void setUploadOptions(int maxFileSize, int minCompress, int maxWidth, int maxHeight, int compressFormat);
};
    


#endif /* UpImageSDKiOS_h */
