#include "hw3D_RPtcl.h"
#include "hw3D_Camera.h"
#include "hw3D_Sprite.h"

void Chw3D_RPtcl::Build(Chw3D_Sprite* pSprite)
{
	Chw3D_A* pA = pSprite->m_pA;
	unsigned int startFrame = 0;
	unsigned int endFrame = 0;
	if (pA)
	{
		startFrame = pA->m_nStartFrame;
		endFrame = pA->m_nEndFrame;
	}


	// 开始构建顶点
	Chw3D_Camera* pCamera = pSprite->m_pCamera;
	Vec3 XDir, YDir, ZDir;
	pCamera->XDir(&XDir);
	pCamera->YDir(&YDir);
	pCamera->ZDir(&ZDir);

	if (m_pPtcl->m_bLoop && pSprite->m_nFrame - startFrame > (endFrame - startFrame) / 2)
	{
		m_dwTracks = 2;
	}

	//
	m_dwCount = 0;
	for(unsigned int j = 0; j < m_dwTracks; ++j)
	{
		int nFrame = pSprite->m_nFrame - startFrame;
		if (j != 0)
		{
			nFrame += (endFrame - startFrame) / 2;
			nFrame %= (endFrame - startFrame);
		}

		Chw3D_Ptcl_Frame* pFrame = m_pPtcl->m_frames[nFrame];
		for(unsigned int n = 0; n < pFrame->m_dwCount; ++n)
		{
			// 算最终中心点
			Vec3 Center;
			//Vec3Transform(&Center, &pFrame->m_Poses[n], &pSprite->m_Matrix);
			pSprite->m_matrix.transformPoint(pFrame->m_poses[n], &Center);

			// 计算颜色
			Vec4 Color (1.0f, 1.0f, 1.0f, 1.0f);
			m_pPtcl->m_material.m_timeColorTracks.Query(pFrame->m_ages[n], &Color);
			unsigned int dwColor = ((unsigned int)(255 * Color.w) << 24) | ((unsigned int)(255 * Color.z) << 16) | ((unsigned int)(255 * Color.y) << 8) | (unsigned int)(255 * Color.x);

			// 旋转
			float fRadian = 0;
			if(m_pPtcl->m_bDirect)
			{
				// 换一种思路
				// 求摄像机视线和方向的垂直向量
				Vec3 Vec;
				//Vec3Add(&Vec, &pFrame->m_Poses[n], &pFrame->m_Velocitys[n]);
				Vec3::add(pFrame->m_poses[n], pFrame->m_velocitys[n], &Vec);
				//Vec3Transform(&Vec, &Vec, &pSprite->m_Matrix);
				pSprite->m_matrix.transformPoint(Vec, &Vec);

				//Vec3Subtract(&Vec, &Vec, &Center);
				Vec3::subtract(Vec, Center, &Vec);
				//Vec3Cross(&Vec, &ZDir, &Vec);
				Vec3::cross(ZDir, Vec, &Vec);
				//Vec3Normalize(&Vec, &Vec);
				Vec.normalize();
				// 求和摄像机上方向的夹角
				//fRadian = acos(Vec3Dot(&YDir, &Vec));
				fRadian = acos(Vec3::dot(YDir, Vec));
				fRadian = fRadian - 1.57f;
				// 求是不是大于180度
				if(Vec3::dot(Vec, XDir) > 0.0f)
				{
					fRadian = 3.14f - fRadian;
				}
			}
			else
			{
				// 旋转粒子
				fRadian = pFrame->m_radians[n];
			}

			Mat4 RotateMatrix;
			//Mat4RotationAxisAngle(&RotateMatrix, &ZDir, fRadian);
			Mat4::createRotation(ZDir, fRadian, &RotateMatrix);
			Vec3 X, Y;
			//Vec3TransformNormal(&X, &XDir, &RotateMatrix);
			RotateMatrix.transformVector(XDir, &X);
			//Vec3TransformNormal(&Y, &YDir, &RotateMatrix);
			RotateMatrix.transformVector(YDir, &Y);
			// 缩放
			X *= pSprite->m_fScale;
			Y *= pSprite->m_fScale;

			// 计算uv
			unsigned int dwImageCount = m_pPtcl->m_dwImageCol * m_pPtcl->m_dwImageRow;

			unsigned int dwImageIndex = (unsigned int)(pFrame->m_ages[n] / (1.0f / dwImageCount));
			dwImageIndex = min(dwImageIndex, (dwImageCount - 1));

			unsigned int dwCol = dwImageIndex % m_pPtcl->m_dwImageCol;
			unsigned int dwRow = dwImageIndex / m_pPtcl->m_dwImageCol;

			float fUStart = (float)dwCol / m_pPtcl->m_dwImageCol;
			float fUEnd = (float)(dwCol + 1) / m_pPtcl->m_dwImageCol;
			float fVStart = (float)dwRow / m_pPtcl->m_dwImageRow;
			float fVEnd = (float)(dwRow + 1) / m_pPtcl->m_dwImageRow;

			// 计算位移
			Vec3 XOffset, YOffset;

			//Vec3Scale(&XOffset, &X, pFrame->m_Sizes[n]);
			XOffset = X * pFrame->m_sizes[n];
			//Vec3Scale(&YOffset, &Y, pFrame->m_Sizes[n]);
			YOffset = Y * pFrame->m_sizes[n];

			// 生成四个顶点
			Vec3 Temp;
			unsigned int dwIndex = (m_dwCount + n ) * 4;

			// 0
			//Vec3Subtract(&Temp, &Center, &YOffset);
			Vec3::subtract(Center, YOffset, &Temp);

			//Vec3Subtract(&m_Poses[dwIndex], &Temp, &XOffset);
			Vec3::subtract(Temp, XOffset, &m_poses[dwIndex]);
			m_UVs[dwIndex].x = fUStart;
			m_UVs[dwIndex].y = fVEnd;
			m_colors[dwIndex] = dwColor;

			// 1
			//Vec3Add(&m_Poses[dwIndex + 1], &Temp, &XOffset);
			Vec3::add(Temp, XOffset, &m_poses[dwIndex + 1]);
			m_UVs[dwIndex + 1].x = fUEnd;
			m_UVs[dwIndex + 1].y = fVEnd;
			m_colors[dwIndex + 1] = dwColor;

			// 2
			//Vec3Add(&Temp, &Center, &YOffset);
			Vec3::add(Center, YOffset, &Temp);


			//Vec3Add(&m_Poses[dwIndex + 2], &Temp, &XOffset);
			Vec3::add(Temp, XOffset, &m_poses[dwIndex + 2]);
			m_UVs[dwIndex + 2].x = fUEnd;
			m_UVs[dwIndex + 2].y = fVStart;
			m_colors[dwIndex + 2] = dwColor;

			// 3
			//Vec3Subtract(&m_Poses[dwIndex + 3], &Temp, &XOffset);
			Vec3::subtract(Temp, XOffset, &m_poses[dwIndex + 3]);
			m_UVs[dwIndex + 3].x = fUStart;
			m_UVs[dwIndex + 3].y = fVStart;
			m_colors[dwIndex + 3] = dwColor;
		}
		m_dwCount += pFrame->m_dwCount;
	}
}

Chw3D_RPtcl::Chw3D_RPtcl()
{
	m_dwTracks = 1;

	// shader program
	setGLProgram(GLProgramCache::getInstance()->getGLProgram(GLProgram::SHADER_3D_Higame3D));
	m_nUniformColor = glGetUniformLocation(getGLProgram()->getProgram(), "u_color");
	m_nUniformUVTrans = glGetUniformLocation(getGLProgram()->getProgram(), "u_uvtrans");
}
Chw3D_RPtcl::~Chw3D_RPtcl()
{
}

bool Chw3D_RPtcl::Create(Chw3D_Ptcl* pPtcl)
{
	m_pPtcl = pPtcl;

	//
	m_poses.resize(pPtcl->m_dwCountMax * 4 * 2);
	m_colors.resize(pPtcl->m_dwCountMax * 4 * 2);
	m_UVs.resize(pPtcl->m_dwCountMax * 4 * 2);

	m_tris.resize(pPtcl->m_dwCountMax * 6 * 2);
	for(unsigned int n = 0; n < m_pPtcl->m_dwCountMax * 2; ++n)
	{
		m_tris[n * 6 + 0] = (unsigned short)(n * 4 + 0);
		m_tris[n * 6 + 1] = (unsigned short)(n * 4 + 1);
		m_tris[n * 6 + 2] = (unsigned short)(n * 4 + 2);
		m_tris[n * 6 + 3] = (unsigned short)(n * 4 + 0);
		m_tris[n * 6 + 4] = (unsigned short)(n * 4 + 2);
		m_tris[n * 6 + 5] = (unsigned short)(n * 4 + 3);
	}

	return true;
}

void Chw3D_RPtcl::draw(Chw3D_Sprite* pSprite, bool bBuild)
{
	CC_PROFILER_START_CATEGORY(kCCProfilerCategorySprite, "Chw3D_RPtcl - draw");

	// 摄像机
	pSprite->m_pCamera->Active();

	// 构建
	if(bBuild)
	{
		Build(pSprite);
	}

	if(m_dwCount == 0)
	{
		// 摄像机
		pSprite->m_pCamera->Inactive();
		return;
	}
	
	// 宏CC_NODE_DRAW_SETUP之中调用getGLProgram()->setUniformsForBuiltins(_modelViewTransform)， 
	// 但由于RMesh的MV矩阵并未保存在_modelViewTransform, 会导致模型视图变换无效，因此不能使用该宏，
	// 而需要使用getGLProgram()->setUniformsForBuiltins(); 
	// CC_NODE_DRAW_SETUP();
	
	do { 
		CCASSERT(getGLProgram(), "No shader program set for this node"); 
		{ 
			getGLProgram()->use();
			getGLProgram()->setUniformsForBuiltins();
		}
	} while(0);

    GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POS_COLOR_TEX);
	
	// vertex
	glVertexAttribPointer( GLProgram::VERTEX_ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, 0, (void*)&m_poses[0]);
	// texCoods
	glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, 0, (void*)&m_UVs[0]);
    // color
	glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, 0, (void*)&m_colors[0]);

	// texture
	GL::bindTexture2D(m_pPtcl->m_material.m_pTexture->getName());

	/*
	根据material算uv
	*/
	// UV
	Vec2 UVTrans(0, 0);
	//m_pPtcl->m_Material.m_UVStepTracks.Query((float)pSprite->m_nFrame, &UVTrans);
	getGLProgram()->setUniformLocationWith2fv(m_nUniformUVTrans, (GLfloat*)&UVTrans, 1);

	/*
	根据material算颜色
	*/
	Vec4 Color(1.0f, 1.0f, 1.0f, 1.0f);
	m_pPtcl->m_material.m_colorTracks.Query((float)pSprite->m_nFrame, &Color);
	
	// 乘上环境里取出来的值
	/*Color.r = Color.r * CFBGraphics::getSingleton().m_Envionment.m_Color.r;
	Color.g = Color.g * CFBGraphics::getSingleton().m_Envionment.m_Color.g;
	Color.b = Color.b * CFBGraphics::getSingleton().m_Envionment.m_Color.b;
	Color.a = Color.a * CFBGraphics::getSingleton().m_Envionment.m_Color.a;
	m_pFx->m_pEffect->SetFloatArray("g_Color", &Color.r, 4);*/

	getGLProgram()->setUniformLocationWith4fv(m_nUniformColor, (GLfloat*)&Color, 1);

	//
	unsigned int mode = m_pPtcl->m_dwMode;
	if(Color.w < 1.0f && mode == 0)
	{
		mode = 2;
	}

	// mode
	switch(mode)
	{
	case 0:
		// normal
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);

		glDisable(GL_BLEND);
		break;
	case 1:
		// colorkey
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);

		glEnable(GL_BLEND);
		GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
		break;
	case 2:
		// alpha
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);

		glEnable(GL_BLEND);
		GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
		break;
	case 3:
		// add
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);

		glEnable(GL_BLEND);
		GL::blendFunc(GL_ONE, GL_ONE);
		break;
	case 4:
		// srcalpha
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);

		glEnable(GL_BLEND);
		//GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		break;
	}

	glDrawElements(GL_TRIANGLES, m_dwCount * 6, GL_UNSIGNED_SHORT, (void*)&m_tris[0]);

	CHECK_GL_ERROR_DEBUG();

	// 摄像机
	pSprite->m_pCamera->Inactive();

	// mode
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);

	glEnable(GL_BLEND);
	GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

	//
    CC_INCREMENT_GL_DRAWS(1);

    CC_PROFILER_STOP_CATEGORY(kCCProfilerCategorySprite, "Chw3D_RPtcl - draw");
}
