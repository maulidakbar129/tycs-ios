#include "hw3D_A.h"
#include "hwCore_String.h"

Chw3D_A_Node::Chw3D_A_Node()
{
	m_pTrackSet = nullptr;
}
Chw3D_A_Node::~Chw3D_A_Node()
{
	if(m_pTrackSet)
	{
		delete m_pTrackSet;
	}
	m_pTrackSet = nullptr;
}

void Chw3D_A_Node::Load(ChwCore_Stream* pStream)
{ 
	//
	pStream->Read(&m_type, sizeof(TYPE));

	// 名字
	unsigned int dwLen;
	pStream->Read(&dwLen, sizeof(unsigned int));
	m_name.resize(dwLen);
	pStream->Read(&m_name[0], sizeof(char) * dwLen);

		//CCLOG("\tChw3D_A_Node::Load:%s, m_Type=%d", &m_fileName[0], m_Type); 
	if (m_pTrackSet)
	{
		delete m_pTrackSet;
	}
	m_pTrackSet = nullptr;
	//
	switch(m_type)
	{
	case T_BONE:
		{
			m_pTrackSet = new Chw3D_BoneTrackSet;
		}
		break;
	case T_MESH:
		{
			m_pTrackSet = new Chw3D_MeshTrackSet;
		}
		break;
	case T_PTCL:
		{
			m_pTrackSet = new Chw3D_PtclTrackSet;
		}
		break;
	case T_PPTCL:
		{
			m_pTrackSet = new Chw3D_PPtclTrackSet;
		}
		break;
	case T_LINE:
		{
			m_pTrackSet = new Chw3D_LineTrackSet;
		}
		break;
	}

	m_pTrackSet->Load(pStream);

	//
	pStream->Read(&m_initMatrix, sizeof(Mat4));


	//CCLOG( "m_InitMatrix:\n"  );

	//CCLOG( "[" );

	//for(int x=0; x<4; x++)
	//{


	//	CCLOG( "%f,%f,%f,%f\n", m_InitMatrix.m[x*4+0], m_InitMatrix.m[x*4+1], m_InitMatrix.m[x*4+2], m_InitMatrix.m[x*4+3] );


	//	if (x==3)
	//	{
	//		CCLOG( "]\n" );
	//	}

	//}


}

Chw3D_A_CameraNode::Chw3D_A_CameraNode()
{
	
}
Chw3D_A_CameraNode::~Chw3D_A_CameraNode()
{
	
}

void Chw3D_A_CameraNode::Load(ChwCore_Stream* pStream)
{
	//名字
	unsigned int dwLen;
	pStream->Read(&dwLen, sizeof(unsigned int));
	m_fileName.resize(dwLen);
	pStream->Read(&m_fileName[0], sizeof(char) * dwLen);
	
	pStream->Read(&m_aspectRatio, sizeof(float));

	//帧数
	pStream->Read(&m_dwKeys, sizeof(unsigned int));

	m_pCameraTrackSet = new CameraInfo[m_dwKeys];
	for (unsigned int n = 0; n < m_dwKeys; ++n)
	{
		pStream->Read(&m_pCameraTrackSet[n].x, sizeof(float));
		pStream->Read(&m_pCameraTrackSet[n].y, sizeof(float));
		pStream->Read(&m_pCameraTrackSet[n].z, sizeof(float));

		pStream->Read(&m_pCameraTrackSet[n].tx, sizeof(float));
		pStream->Read(&m_pCameraTrackSet[n].ty, sizeof(float));
		pStream->Read(&m_pCameraTrackSet[n].tz, sizeof(float));

		pStream->Read(&m_pCameraTrackSet[n].focal, sizeof(float));
		pStream->Read(&m_pCameraTrackSet[n].nearplane, sizeof(float));
		pStream->Read(&m_pCameraTrackSet[n].farplane, sizeof(float));

	}
}

CameraInfo* Chw3D_A_CameraNode::GetCameraInfo(unsigned int nframe)
{
	if (nframe < m_dwKeys)
	{
		return &m_pCameraTrackSet[nframe];
	}
	return nullptr;
}

void Chw3D_A::TreeLoad(tree<pair<string, unsigned int>>::iterator it, ChwCore_Stream* pStream)
{
	pair<string, unsigned int> Pair;
	
	unsigned int dwLen;
	pStream->Read(&dwLen, sizeof(unsigned int));
	Pair.first.resize(dwLen);
	pStream->Read(&Pair.first[0], sizeof(char) * dwLen);
	pStream->Read(&Pair.second, sizeof(unsigned int));



	if(Pair.second == UINT_MAX)
	{
		it = m_tree.insert(m_tree.begin(), Pair);
	}
	else
	{
		it = m_tree.append_child(it, Pair);
	}

	unsigned int dwChildrens;
	pStream->Read(&dwChildrens, sizeof(unsigned int));
	
	//	CCLog("TreeLoad:%s (%d) dwChildrens=%d" ,&Pair.first[0], Pair.second, dwChildrens);  
	for(unsigned int n = 0; n < dwChildrens; n++)
	{
		TreeLoad(it, pStream);
	}
}

Chw3D_A::Chw3D_A(const char*  fileName):
	m_bonesCount(0),
	m_nStartFrame(0),
	m_nEndFrame(0),
	m_isLoadedData(false),
	m_fileName(fileName)
{

}

Chw3D_A::Chw3D_A():
	m_bonesCount(0),
	m_nStartFrame(0),
	m_isLoadedData(false),
	m_nEndFrame(0)
{

}
Chw3D_A::~Chw3D_A()
{
	for(unsigned int n = 0; n <m_nodes.size(); ++n)
	{
		delete m_nodes[n];
	}

	for (auto it=m_frameMatrixs.begin(); it!=m_frameMatrixs.end(); it++)
	{
		delete[] it->second;
	}
	m_frameMatrixs.clear();

}

void Chw3D_A::LoadData(cocos2d::Data* data)
{
	ChwCore_Stream Stream(data->getBytes(), data->getSize(), 0);

	Stream.Read(&m_nStartFrame, sizeof(int));
	Stream.Read(&m_nEndFrame, sizeof(int));
	unsigned int dwNodes;
	Stream.Read(&dwNodes, sizeof(unsigned int));
	for (unsigned int n = 0; n < dwNodes; n++)
	{
		Chw3D_A_Node* pNode = new Chw3D_A_Node;
		pNode->Load(&Stream);
		m_nodes.push_back(pNode);

		if (pNode->m_type == Chw3D_A_Node::TYPE::T_BONE)
		{
			m_bonesCount++;
		}
	}
	TreeLoad(0, &Stream);

	//摄像机数据
	unsigned int dwCameraNodes = 0;
	Stream.Read(&dwCameraNodes, sizeof(unsigned int));

	for (unsigned int n = 0; n < dwCameraNodes; n++)
	{
		Chw3D_A_CameraNode* pNode = new Chw3D_A_CameraNode;
		pNode->Load(&Stream);
		m_cameraNodes.push_back(pNode);
	}
}


void Chw3D_A::LoadFileAsync(const char* pFilename, std::function<void(Chw3D_A*)>& callback)
{
	m_fileName = pFilename;
	m_isLoadedData = true;
	FileUtils::getInstance()->getDataFromFile(pFilename, [=](Data data)
	{
		if (data.getSize() == 0)
		{
			delete this;
			callback(nullptr);
		}
		else
		{
			m_fileName = pFilename;
			LoadData(&data);
			callback(this);
		}
	});
}

bool Chw3D_A::LoadFile(const char* pFilename)
{
	m_fileName = pFilename;
	Data data = FileUtils::getInstance()->getDataFromFile(pFilename);
	if (data.getSize() == 0)
	{
		//cocos2d::MessageBox(pFilename, "Chw3D_C File Miss Fatal Error");
		return false;
	}
	LoadData(&data);
	m_isLoadedData = true;
	return true;
}


unsigned int Chw3D_A::Query(const char* pName)
{
	for(unsigned int n = 0; n < m_nodes.size(); ++n)
	{
		if(strcmp(m_nodes[n]->m_name.c_str(), pName) == 0)
		{
			return n;
		}
	}
	return UINT_MAX;
}

unsigned int Chw3D_A::getBonesCount()
{
	return m_bonesCount;
}


bool Chw3D_A::isLoadedData()
{
	return m_isLoadedData;
}

Chw3D_A_CameraNode*	Chw3D_A::QueryCameraNode(const char* pName)
{
	for (unsigned int n = 0; n < m_cameraNodes.size(); ++n)
	{
		if (strcmp(m_cameraNodes[n]->m_fileName.c_str(), pName) == 0)
		{
			return m_cameraNodes[n];
		}
	}

	return nullptr;
}