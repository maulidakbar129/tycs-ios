#include "hw3D_Camera.h"

Chw3D_Camera* Chw3D_Camera::create(void)
{
	Chw3D_Camera* pRet = new Chw3D_Camera;
	pRet->autorelease();
	return pRet;
}

Chw3D_Camera::Chw3D_Camera():
	m_viewProjectionDirty(true),
	m_ray(nullptr),
	m_isEnabelGuiDepth(false),
	m_depthRatio(0)
{
	m_up.x = 0.0f;
	m_up.y = 1.0f;
	m_up.z = 0.0f;

	Set(5, 5, 5, 0, 0, 0);

	Size size = Director::getInstance()->getWinSizeInPixels();
	Perspective(45, size.width, size.height, 0.1f, 9999.99f);
	//Ortho(size.width / 100, size.height / 100, 0.1f, 9999.99f);
}

Chw3D_Camera::~Chw3D_Camera()
{
	if (m_ray)
	{
		delete m_ray;
	}
}

bool Chw3D_Camera::Load(const char* pFilename)
{
	Data data = FileUtils::getInstance()->getDataFromFile(pFilename);
	ChwCore_Stream Stream(data.getBytes(), data.getSize(), 0);

	int nStartFrame, nEndFrame;
	Stream.Read(&nStartFrame, sizeof(int));
	Stream.Read(&nEndFrame, sizeof(int));

	Vec3 From;
	for(int n = nStartFrame; n <= nEndFrame; ++n)
	{
		Stream.Read(&From, sizeof(Vec3));
	}

	Vec3 To;
	for(int n = nStartFrame; n <= nEndFrame; ++n)
	{
		Stream.Read(&To, sizeof(Vec3));
	}

	Set(From.x, From.y, From.z, To.x, To.y, To.z);

	return true;
}

void Chw3D_Camera::Set(float fFromX, float fFromY, float fFromZ, float fToX, float fToY, float fToZ)
{
	m_from.x = fFromX;
	m_from.y = fFromY;
	m_from.z = fFromZ;

	m_to.x = fToX;
	m_to.y = fToY;
	m_to.z = fToZ;
	///////////////////////////////////////////////////////////////////////////////////
	//// 计算视图变换矩阵
	//// DX使用左手坐标系和行向量
	//// DX的情况请参考 http://www.cnblogs.com/graphics/archive/2012/07/12/2476413.html 
	////             或 http://www.cnblogs.com/mikewolf2002/archive/2012/03/11/2390669.html
	//// OpenGL使用右手坐标系和列向量
	//// OpenGL的情况请参考 http://blog.csdn.net/augusdi/article/details/20450065
	//// 由于该引擎用右手坐标系和行向量，情况比较特殊, 所以与两者都不同-_-
	//// 下标顺序如下： 
	////     0   4   8   12
    ////     1   5   9   13
    ////     2   6   10  14
    ////     3   7   11  15
	////手工计算，计算方法与Mat4::createLookAt()相同
	//// 前向量
	//Vec3 frontDir = m_To-m_From;
	//frontDir.normalize();
	//// 后向量
	//Vec3 backDir = -frontDir;
	//backDir.normalize();

	//m_Up.normalize();

	//// 右向量
	//Vec3 rightDir;
	//Vec3::cross( m_Up, backDir, &rightDir);
	//frontDir.normalize();

	//// 上向量
	//Vec3 upDir;
	//Vec3::cross( backDir, rightDir, &upDir);
	//upDir.normalize();

	////Mat4Identity(&m_View);

	//// R=Right; U=Up;  B=back; F=From
	///////////////////////////////////
	////  Rx    Ux    Bx   0
	////  Ry    Uy    By   0
	////  Rz    Uz    Bz   0
	//// -F.R  -F.U  -F.B  1
	///////////////////////////////////

	//m_View.m[0] = rightDir.x;
	//m_View.m[4] = rightDir.y;
	//m_View.m[8] = rightDir.z;
	//m_View.m[12] = -Vec3::dot(m_From, rightDir );

	//m_View.m[1] = upDir.x;
	//m_View.m[5] = upDir.y;
	//m_View.m[9] = upDir.z;
	//m_View.m[13] = -Vec3::dot(m_From, upDir );

	//m_View.m[2] = backDir.x;
	//m_View.m[6] = backDir.y;
	//m_View.m[10] = backDir.z;
	//m_View.m[14] = -Vec3::dot(m_From, backDir );

	//m_View.m[3] = 0;
	//m_View.m[7] = 0;
	//m_View.m[11] = 0;
	//m_View.m[15] = 1;
	///////////////////////////////////////////////////////////////////////////////////


	Mat4::createLookAt( m_from, m_to, m_up, &m_view);
	m_viewProjectionDirty = true;
}

void Chw3D_Camera::Perspective(float fieldOfView, float aspectRatio, float nearPlane, float farPlane)
{
	m_aspectRatio = aspectRatio;
	Mat4::createPerspective(fieldOfView, aspectRatio, nearPlane, farPlane, &m_projection);
	m_viewProjectionDirty = true;
}


void Chw3D_Camera::Perspective(float fFOV, float fWidth, float fHeight, float fNear, float fFar)
{
	m_width = fWidth;
	m_height = fHeight;
	m_aspectRatio = fWidth / fHeight;
	Mat4::createPerspective(fFOV, m_aspectRatio, fNear, fFar, &m_projection);
	m_viewProjectionDirty = true;
}

void Chw3D_Camera::Ortho(float fWidth, float fHeight, float fNear, float fFar)
{
	//Mat4OrthographicProjection(&m_Projection, dwLeft, dwRight, dwTop, dwBottom, fNear, fFar);
	m_projection = Mat4::IDENTITY;
    m_projection.m[0] = 2.0f / fWidth;
    m_projection.m[5] = 2.0f / fHeight;
    m_projection.m[10] = 1.0f / (fNear - fFar);
    m_projection.m[14] = fNear / (fNear - fFar);
	m_viewProjectionDirty = true;
}

void Chw3D_Camera::XDir(Vec3* pDir)
{
	pDir->x = m_view.m[0];
	pDir->y = m_view.m[4];
	pDir->z = m_view.m[8];
}
void Chw3D_Camera::YDir(Vec3* pDir)
{
	pDir->x = m_view.m[1];
	pDir->y = m_view.m[5];
	pDir->z = m_view.m[9];
}
void Chw3D_Camera::ZDir(Vec3* pDir)
{
	pDir->x = m_view.m[2];
	pDir->y = m_view.m[6];
	pDir->z = m_view.m[10];
}

void Chw3D_Camera::Active(void)
{
	//
	Director::getInstance()->loadIdentityMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_PROJECTION);
	Director::getInstance()->multiplyMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_PROJECTION, m_projection);

	//
	Director::getInstance()->loadIdentityMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
	Director::getInstance()->multiplyMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW, m_view);
}
void Chw3D_Camera::Inactive(void)
{
	//
	Director::getInstance()->loadIdentityMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_PROJECTION);

	Mat4 orthoMatrix;
	Size size = Director::getInstance()->getWinSizeInPixels();
	//Mat4OrthographicProjection(&orthoMatrix, 0, size.width, 0, size.height, -1024, 1024);
	Mat4::createOrthographicOffCenter(0, size.width, 0, size.height, -1024, 1024, &orthoMatrix);
	Director::getInstance()->multiplyMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_PROJECTION, orthoMatrix);

	//
	Director::getInstance()->loadIdentityMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
}

Mat4& Chw3D_Camera::getViewProjectionMatrix()
{
	if (m_viewProjectionDirty)
	{
		m_viewProjectionDirty = false;
		Mat4::multiply(m_projection, m_view, &m_viewProjection);
		
	}
	return m_viewProjection;
}

Vec2 Chw3D_Camera::project(const Vec3& src)
{
	Vec2 screenPos;

	auto viewport = Director::getInstance()->getWinSize();
	Vec4 clipPos;
	getViewProjectionMatrix().transformVector(Vec4(src.x, src.y, src.z, 1.0f), &clipPos);

	CCASSERT(clipPos.w != 0.0f, "clipPos.w can't be 0.0f!");
	float ndcX = clipPos.x / clipPos.w;
	float ndcY = clipPos.y / clipPos.w;

	screenPos.x = (ndcX + 1.0f) * 0.5f * viewport.width;
	screenPos.y = (1.0f - (ndcY + 1.0f) * 0.5f) * viewport.height;
	return screenPos;
}

Vec2 Chw3D_Camera::projectGL(const Vec3& src)
{
	Vec2 screenPos;

	auto viewport = Director::getInstance()->getWinSize();
	Vec4 clipPos;
	getViewProjectionMatrix().transformVector(Vec4(src.x, src.y, src.z, 1.0f), &clipPos);

	CCASSERT(clipPos.w != 0.0f, "clipPos.w can't be 0.0f!");
	float ndcX = clipPos.x / clipPos.w;
	float ndcY = clipPos.y / clipPos.w;

	screenPos.x = (ndcX + 1.0f) * 0.5f * viewport.width;
	screenPos.y = (ndcY + 1.0f) * 0.5f * viewport.height;
	return screenPos;
}

Vec3 Chw3D_Camera::unproject(const Vec3& src)
{
	Vec3 dst;
	unproject(Director::getInstance()->getWinSize(), &src, &dst);
	return dst;
}

Vec3 Chw3D_Camera::unprojectGL(const Vec3& src)
{
	Vec3 dst;
	unprojectGL(Director::getInstance()->getWinSize(), &src, &dst);
	return dst;
}

void Chw3D_Camera::unproject(const Size& viewport, const Vec3* src, Vec3* dst)
{
	CCASSERT(src && dst, "vec3 can not be null");

	Vec4 screen(src->x / viewport.width, ((viewport.height - src->y)) / viewport.height, src->z, 1.0f);
	screen.x = screen.x * 2.0f - 1.0f;
	screen.y = screen.y * 2.0f - 1.0f;
	screen.z = screen.z * 2.0f - 1.0f;

	getViewProjectionMatrix().getInversed().transformVector(screen, &screen);
	if (screen.w != 0.0f)
	{
		screen.x /= screen.w;
		screen.y /= screen.w;
		screen.z /= screen.w;
	}

	dst->set(screen.x, screen.y, screen.z);
}

void Chw3D_Camera::unprojectGL(const Size& viewport, const Vec3* src, Vec3* dst)
{
	CCASSERT(src && dst, "vec3 can not be null");

	Vec4 screen(src->x / viewport.width, src->y / viewport.height, src->z, 1.0f);
	screen.x = screen.x * 2.0f - 1.0f;
	screen.y = screen.y * 2.0f - 1.0f;
	screen.z = screen.z * 2.0f - 1.0f;

	getViewProjectionMatrix().getInversed().transformVector(screen, &screen);
	if (screen.w != 0.0f)
	{
		screen.x /= screen.w;
		screen.y /= screen.w;
		screen.z /= screen.w;
	}

	dst->set(screen.x, screen.y, screen.z);
}

Vec3 Chw3D_Camera::screenPointToWorldPoint(Vec2 screenPoint)
{

	auto viewport = Director::getInstance()->getWinSize();
	//窗口大小

	auto nearP = Vec3(screenPoint.x, screenPoint.y, 0.0f);//近平面
	auto farP = Vec3(screenPoint.x, screenPoint.y, 1.0f);//远平面

	unprojectGL(viewport, &nearP, &nearP);//反投影
	unprojectGL(viewport, &farP, &farP);

	auto dir = farP - nearP;//得到射线方向
	dir.normalize();//归一化向量
	auto ray = Ray(nearP, dir);//从近平面射出的一条射线

	Plane p(Vec3(0, 1, 0), Vec3(0, 0, 0));
	return ray.intersects(p);
}

void Chw3D_Camera::setEnabelGuiDepth(bool isEnabel)
{
	m_isEnabelGuiDepth = isEnabel;
}

bool Chw3D_Camera::isEnabelGuiDepth()
{
	return m_isEnabelGuiDepth;
}

void Chw3D_Camera::setDepthRatio(float depthRatio)
{
	m_depthRatio = depthRatio;
}

float Chw3D_Camera::getDepthRatio()
{
	return m_depthRatio;
}
