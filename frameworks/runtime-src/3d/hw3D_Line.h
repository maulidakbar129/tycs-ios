#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include "hwCore_Stream.h"
#include "hw3D_Material.h"

class Chw3D_Line
{
public:
	string												m_fileName;				// 名字
	unsigned											m_dwMode;			// 模式
	unsigned int										m_dwNodeIndex;		// 这个Sub使用骨骼索引，同父模型

	Vec3												m_knots[2];
	double												m_dMaxLife;			// 最大生命
	float												m_fWidth;			// 宽度（不是拖尾模式使用）

	Chw3D_Material										m_material;			// 材质

public:
										Chw3D_Line();
	virtual								~Chw3D_Line();

	void								LoadEx(ChwCore_Stream* pStream, bool isAsync=false);
	Chw3D_Material*						getMaterial();
};