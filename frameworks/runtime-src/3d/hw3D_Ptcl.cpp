#include "hw3D_Ptcl.h"

Chw3D_Ptcl_Frame::Chw3D_Ptcl_Frame()
{
}
Chw3D_Ptcl_Frame::~Chw3D_Ptcl_Frame()
{
}

Chw3D_Ptcl::Chw3D_Ptcl()
{
}
Chw3D_Ptcl::~Chw3D_Ptcl()
{
    int count = m_frames.size();
    for (int i = 0; i < count;i++)
    {
        delete m_frames[i];
    }
	m_frames.clear();
}

void Chw3D_Ptcl::LoadEx(ChwCore_Stream* pStream, bool isAsync)
{
	// 名字
	unsigned int dwLen;
	pStream->Read(&dwLen, sizeof(unsigned int));
	if(dwLen)
	{
		m_fileName.resize(dwLen);
		pStream->Read(&m_fileName[0], sizeof(char) * dwLen);
	}

	// 模式
	pStream->Read(&m_dwMode, sizeof(unsigned int));

	// 这个Sub使用骨骼索引，同父模型
	pStream->Read(&m_dwNodeIndex, sizeof(unsigned int));

	//
	pStream->Read(&m_dwCountMax, sizeof(unsigned int));

	unsigned int dwFrames;
	pStream->Read(&dwFrames, sizeof(unsigned int));
	for(unsigned int n = 0; n < dwFrames; ++n)
	{
		Chw3D_Ptcl_Frame* pFrame = new Chw3D_Ptcl_Frame;

		pStream->Read(&pFrame->m_dwCount, sizeof(unsigned int));
		if(pFrame->m_dwCount > 0)
		{
			pFrame->m_poses.resize(pFrame->m_dwCount);
			pStream->Read(&pFrame->m_poses[0], sizeof(Vec3) * pFrame->m_dwCount);

			pFrame->m_velocitys.resize(pFrame->m_dwCount);
			pStream->Read(&pFrame->m_velocitys[0], sizeof(Vec3) * pFrame->m_dwCount);

			pFrame->m_ages.resize(pFrame->m_dwCount);
			pStream->Read(&pFrame->m_ages[0], sizeof(float) * pFrame->m_dwCount);

			pFrame->m_sizes.resize(pFrame->m_dwCount);
			pStream->Read(&pFrame->m_sizes[0], sizeof(float) * pFrame->m_dwCount);

			pFrame->m_radians.resize(pFrame->m_dwCount);
			pStream->Read(&pFrame->m_radians[0], sizeof(float) * pFrame->m_dwCount);
		}

		m_frames.push_back(pFrame);
	}

	pStream->Read(&m_fRotate, sizeof(float));
	pStream->Read(&m_bDirect, sizeof(unsigned int));
	pStream->Read(&m_bLoop, sizeof(unsigned int));

	pStream->Read(&m_dwImageCol, sizeof(unsigned int));
	pStream->Read(&m_dwImageRow, sizeof(unsigned int));

	// 材质
	m_material.LoadEx(pStream, isAsync);
}

Chw3D_Material*	Chw3D_Ptcl::getMaterial()
{
	return &m_material;
}