#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include "hwCore_Stream.h"
#include "hw3D_Material.h"

class Chw3D_PPtcl
{
public:
	string								m_fileName;							// 名字
	unsigned int						m_dwMode;						// 模式
	unsigned int						m_dwNodeIndex;					// 这个Sub使用骨骼索引，同父模型

	Vec3								m_pos;							// 发射器位置

	bool								m_bDirect;						// 指向性

	unsigned int						m_dwImageCol;					// 一行有多少个 （x 的个数）
	unsigned int						m_dwImageRow;					// 一共有多少行 （y 的个数）

	unsigned int						m_dwRate;						// 速率
	float								m_fSpeed;						// 速度
	double								m_dLife;						// 寿命
	float								m_fSizeStart;					// 开始大小
	float								m_fSizeEnd;						// 结束大小

	int									m_nDegreeMin, m_nDegreeMax;		// 初始度数
	int									m_nRotateMin, m_nRotateMax;		// 每秒旋转度数

	Chw3D_Material						m_material;						// 材质


public:
										Chw3D_PPtcl();
	virtual								~Chw3D_PPtcl();

	void								LoadEx(ChwCore_Stream* pStream, bool isAsync=false);

	Chw3D_Material*						getMaterial();
};
