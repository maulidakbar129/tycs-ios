#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include "hw3D_Mesh.h"

static unsigned int SKINNING_JOINT_COUNT = 50;
class Chw3D_Sprite;
class Chw3D_RMesh_Sub
{
public:

	Vec3*								m_Poses;
	Vec3*								m_Normals;
	Chw3D_RMesh_Sub();
	~Chw3D_RMesh_Sub();

};

class Chw3D_RMesh : public Node
{
public:
	//
	Chw3D_Mesh*							m_pMesh;
	vector<Chw3D_RMesh_Sub*>			m_subs;
	GLProgram*							m_glProgram;
	//
	GLint								m_nUniformColor;
	GLint								m_nUniformUVTrans;
	GLint								m_nUniformBones;
	bool								m_isInitPoses;
public:
										Chw3D_RMesh();
	virtual								~Chw3D_RMesh();

	bool								Create(Chw3D_Mesh* pMesh);

	void								draw(Chw3D_Sprite* pSprite, bool bBuild, Vec4 color);
	Vec4								getPosition(Vec3 position, Vec4 weight, Vec4 Indexs, Vec4* matrixPalette);
	void								skiningMesh(Chw3D_Sprite* pSprite, Chw3D_Mesh_Sub* pSub, Chw3D_RMesh_Sub* pRSub);
	Vec3*								getNormalLines(size_t count, Chw3D_Mesh_Sub* pSub, Vec3* pPoses);

	
private:
	void								cacleVertPos(Vec3* srcPos, Vec3* outPos, float* weight, unsigned char* index, Mat4* matrixs);
	
	//void								getPositionAndNormal(Vec4 &position);
	//void								Build(CFBGraphics_RA* pRA);

	/*void								Prepare(CFBGraphics_RA* pRA);
	void								Draw(CFBGraphics_RA* pRA, DWORD dwIndex0, DWORD dwIndex1);*/
};
