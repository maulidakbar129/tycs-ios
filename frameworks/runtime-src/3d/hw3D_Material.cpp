#include "hw3D_Material.h"
#include "hwCore_String.h"
#include "hw3D_Mgr.h"

Chw3D_Material::Chw3D_Material():
	m_textureFileName(""),
	m_pTexture(nullptr),
	m_pSpecularTexture(nullptr),
	m_pOpacityTexture(nullptr),
	m_pFilterTexture(nullptr),
	m_pFlowLightTexture(nullptr),
	m_hasFlowLight(false)
{
	m_texParams.minFilter = GL_LINEAR;
	m_texParams.magFilter = GL_LINEAR;
	m_texParams.wrapS = GL_REPEAT;
	m_texParams.wrapT = GL_REPEAT;

}
Chw3D_Material::~Chw3D_Material()
{
	Chw3D_Mgr::getInstance()->m_reloadMaterials.erase(m_textureFileName);
	
	if (m_textureFileName != "")
	{
		auto textureCache = Director::getInstance()->getTextureCache();
		textureCache->unbindImageAsync(m_textureFileName);
	}

	if(m_pTexture)
	{
		m_pTexture->release();
		m_pTexture = 0;
	}
}

Texture2D*	Chw3D_Material::getTextureWithType(MaterialmTexType type)
{
	Texture2D* targetTexture = nullptr;
	switch (type)
	{
	case MaterialmTexType::Diffuse: {
		targetTexture = m_pTexture;
		break;
	}
	case MaterialmTexType::Specular: {
		targetTexture = m_pSpecularTexture;
		break;
	}
	case MaterialmTexType::Opacity: {
		targetTexture = m_pOpacityTexture;
		break;
	}
	case MaterialmTexType::Filter: {
		targetTexture = m_pFilterTexture;
		break;
	}
	case MaterialmTexType::SpecularLv: {
		targetTexture = m_pFlowLightTexture;
		break;
	}								   

	default:
		break;
	}
	return targetTexture;
}

void Chw3D_Material::setTextureWithType(MaterialmTexType type, Texture2D* tex)
{
	if (tex) {
		//tex->generateMipmap();
	}
	
	switch (type)
	{
	case MaterialmTexType::Diffuse: {
		m_pTexture = tex;
		break;
	}
	case MaterialmTexType::Specular: {
		m_pSpecularTexture = tex;
		break;
	}
	case MaterialmTexType::Opacity: {
		m_pOpacityTexture = tex;
		break;
	}
	case MaterialmTexType::Filter: {
		m_pFilterTexture = tex;
		break;
	}
	case MaterialmTexType::SpecularLv: {
		m_pFlowLightTexture = tex;
		break;
	}
	default:
		break;
	}

}

void Chw3D_Material::loadTexture(MaterialmTexType type, Texture2D* texture)
{
	if (getTextureWithType(type))
	{
		//getTextureWithType(type)->release();
		setTextureWithType(type, nullptr);
	}
	
	if (!texture)
	{	
		auto textureCache = Director::getInstance()->getTextureCache();
		texture = textureCache->addImage(Chw3D_Mgr::getInstance()->getDefaultMaterialPath());
	}

	if (!texture)
	{
		texture = new (std::nothrow) Texture2D();
	}

	texture->retain();
	texture->setTexParameters(m_texParams);
	setTextureWithType(type, texture);
}

void Chw3D_Material::loadTexture(std::string& texFileName, bool isAsync)
{
	MaterialmTexType type = m_fileMaterialmTexTypes[texFileName];
	auto textureCache = Director::getInstance()->getTextureCache();
	if (isAsync)
	{
		loadTexture(type, nullptr);
		auto callback = [=](Texture2D* texture) {
			loadTexture(type, texture);
		};
		textureCache->addImageAsync(texFileName.c_str(), callback);
	}
	else
	{
		auto texture = textureCache->addImage(texFileName.c_str());

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		if (Chw3D_Mgr::getInstance()->isDebug())
		{
			if (!texture)
			{
				cocos2d::MessageBox(texFileName.c_str(), "Chw3D_Material File Miss Fatal Error");
			}
		}
#endif
		if (!texture)
		{
			Chw3D_Mgr::getInstance()->m_reloadMaterials[texFileName] = this;
			Chw3D_Mgr::getInstance()->handleMissMaterial(texFileName);
		}

		loadTexture(type, texture);
	}

}

void Chw3D_Material::LoadEx(ChwCore_Stream* pStream, bool isAsync)
{

	//
	auto textureCache = Director::getInstance()->getTextureCache();
	if (m_textureFileName != "")
	{
		textureCache->unbindImageAsync(m_textureFileName);
	}

	if (pStream->Version() >= 1) {

		unsigned int dwCount;
		pStream->Read(&dwCount, sizeof(unsigned int));
		for (int i = 0; i < dwCount; i++) {

			unsigned int texType;
			pStream->Read(&texType, sizeof(unsigned int));

			unsigned int dwLen;
			pStream->Read(&dwLen, sizeof(unsigned int));
			string textureFilename;
			textureFilename.resize(dwLen);
			pStream->Read(&textureFilename[0], sizeof(char) * dwLen);
			
			m_fileMaterialmTexTypes[textureFilename] = (MaterialmTexType)texType;
			loadTexture(textureFilename, isAsync);
			
		}
	}
	else {

		unsigned int dwLen;
		pStream->Read(&dwLen, sizeof(unsigned int));
		string textureFilename;
		textureFilename.resize(dwLen);
		pStream->Read(&textureFilename[0], sizeof(char) * dwLen);
		m_textureFileName = textureFilename;
		m_texParams = { GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT };
		m_fileMaterialmTexTypes[textureFilename] = MaterialmTexType::Diffuse;
		loadTexture(textureFilename, isAsync);

	}





	/*
	for(unsigned int n = 0; n < dwTextureFilenames; ++n)
	{
		float fFrame;
		pFile->Read(&fFrame, sizeof(float));

		pFile->Read(&dwLen, sizeof(unsigned int));
		wstring TextureFilename;
		TextureFilename.resize(dwLen);
		pFile->Read(&TextureFilename[0], sizeof(WCHAR) * dwLen);

		m_TextureFilenameTracks().m_Keys[fFrame] = TextureFilename;
	}*/

	//
	unsigned int dwDraws;
	pStream->Read(&dwDraws, sizeof(unsigned int));
	for(unsigned int n = 0; n < dwDraws; ++n)
	{
		float fFrame;
		pStream->Read(&fFrame, sizeof(float));

		unsigned int bDraw;
		pStream->Read(&bDraw, sizeof(unsigned int));

		m_drawTracks.m_keys[fFrame] = (bool)bDraw;
	}

	//
	unsigned int dwColors;
	pStream->Read(&dwColors, sizeof(unsigned int));
	for(unsigned int n = 0; n < dwColors; ++n)
	{
		float fFrame;
		pStream->Read(&fFrame, sizeof(float));

		Vec4 Color;
		pStream->Read(&Color, sizeof(Vec4));

		m_colorTracks.m_keys[fFrame] = Color;
	}

	//
	unsigned int dwTimeColors;
	pStream->Read(&dwTimeColors, sizeof(unsigned int));
	for(unsigned int n = 0; n < dwTimeColors; ++n)
	{
		float fFrame;
		pStream->Read(&fFrame, sizeof(float));

		Vec4 TimeColor;
		pStream->Read(&TimeColor, sizeof(Vec4));

		m_timeColorTracks.m_keys[fFrame] = TimeColor;
	}

	//
	pStream->Read(&m_uvTrans, sizeof(Vec2));

	//
	pStream->Read(&m_dwTileX, sizeof(unsigned int));
	pStream->Read(&m_dwTileY, sizeof(unsigned int));

	unsigned int dwTiles;
	pStream->Read(&dwTiles, sizeof(unsigned int));
	for(unsigned int n = 0; n < dwTiles; ++n)
	{
		float fFrame;
		pStream->Read(&fFrame, sizeof(float));

		unsigned int dwTile;
		pStream->Read(&dwTile, sizeof(unsigned int));

		m_tileTracks.m_keys[fFrame] = dwTile;
	}

	if (pStream->Version() >= 1) 
	{
		pStream->Read(&m_hasFlowLight, sizeof(bool));
		if (m_hasFlowLight) 
		{
			pStream->Read(&m_lightColor, sizeof(Vec4));
			pStream->Read(&m_lightSpeed, sizeof(Vec2));
			pStream->Read(&m_lightWidth, sizeof(float));
		}

		pStream->Read(&m_specular, sizeof(float));
		
	}

}
