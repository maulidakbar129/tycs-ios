#pragma once

#include "hwCore_Stream.h"
#include "hw3D_Mesh.h"
#include "hw3D_Ptcl.h"
#include "hw3D_PPtcl.h"
#include "hw3D_Line.h"
#include "cocos2d.h"

class Chw3D_C : public Node
{
public:
	vector<Chw3D_Mesh*>					m_meshs;				// 模型
	vector<Chw3D_Ptcl*>					m_ptcls;				// 粒子
	vector<Chw3D_PPtcl*>				m_pptcls;				// 程序粒子
	vector<Chw3D_Line*>					m_lines;				// 线体
	//vector<Chw3D_Bone>				m_Bones;				// 骨骼
	
	std::string							m_fileName;				// 文件名
	bool								m_isLoadedData;
public:
									Chw3D_C();
									Chw3D_C(const char*  fileName);
	virtual							~Chw3D_C();

	bool							LoadFile(const char* pFilename);
	void							LoadFileAsync(const char* pFilename, std::function<void(Chw3D_C*)>& callback);
	void							LoadData(cocos2d::Data* data, bool isAsync=false);
	bool							isLoadedData();
};