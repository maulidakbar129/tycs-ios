#include "hwCore_String.h"

int
utf8_mbtowc (unsigned int *pwc, const unsigned char *s, int n)
{
  unsigned char c = s[0];

  if (c < 0x80) {
    *pwc = c;
    return 1;
  } else if (c < 0xc2) {
    return 0;
  } else if (c < 0xe0) {
    if (n < 2)
      return 0;
    if (!((s[1] ^ 0x80) < 0x40))
      return 0;
    *pwc = ((unsigned int) (c & 0x1f) << 6)
           | (unsigned int) (s[1] ^ 0x80);
    return 2;
  } else if (c < 0xf0) {
    if (n < 3)
      return 0;
    if (!((s[1] ^ 0x80) < 0x40 && (s[2] ^ 0x80) < 0x40
          && (c >= 0xe1 || s[1] >= 0xa0)))
      return 0;
    *pwc = ((unsigned int) (c & 0x0f) << 12)
           | ((unsigned int) (s[1] ^ 0x80) << 6)
           | (unsigned int) (s[2] ^ 0x80);
    return 3;
  } else if (c < 0xf8 && sizeof(unsigned int)*8 >= 32) {
    if (n < 4)
      return 0;
    if (!((s[1] ^ 0x80) < 0x40 && (s[2] ^ 0x80) < 0x40
          && (s[3] ^ 0x80) < 0x40
          && (c >= 0xf1 || s[1] >= 0x90)))
      return 0;
    *pwc = ((unsigned int) (c & 0x07) << 18)
           | ((unsigned int) (s[1] ^ 0x80) << 12)
           | ((unsigned int) (s[2] ^ 0x80) << 6)
           | (unsigned int) (s[3] ^ 0x80);
    return 4;
  } else if (c < 0xfc && sizeof(unsigned int)*8 >= 32) {
    if (n < 5)
      return 0;
    if (!((s[1] ^ 0x80) < 0x40 && (s[2] ^ 0x80) < 0x40
          && (s[3] ^ 0x80) < 0x40 && (s[4] ^ 0x80) < 0x40
          && (c >= 0xf9 || s[1] >= 0x88)))
      return 0;
    *pwc = ((unsigned int) (c & 0x03) << 24)
           | ((unsigned int) (s[1] ^ 0x80) << 18)
           | ((unsigned int) (s[2] ^ 0x80) << 12)
           | ((unsigned int) (s[3] ^ 0x80) << 6)
           | (unsigned int) (s[4] ^ 0x80);
    return 5;
  } else if (c < 0xfe && sizeof(unsigned int)*8 >= 32) {
    if (n < 6)
      return 0;
    if (!((s[1] ^ 0x80) < 0x40 && (s[2] ^ 0x80) < 0x40
          && (s[3] ^ 0x80) < 0x40 && (s[4] ^ 0x80) < 0x40
          && (s[5] ^ 0x80) < 0x40
          && (c >= 0xfd || s[1] >= 0x84)))
      return 0;
    *pwc = ((unsigned int) (c & 0x01) << 30)
           | ((unsigned int) (s[1] ^ 0x80) << 24)
           | ((unsigned int) (s[2] ^ 0x80) << 18)
           | ((unsigned int) (s[3] ^ 0x80) << 12)
           | ((unsigned int) (s[4] ^ 0x80) << 6)
           | (unsigned int) (s[5] ^ 0x80);
    return 6;
  } else
    return 0;
}

void ChwCore_String::UTF82Unicode(const char* pString, wstring* pUnicode)
{
	unsigned int dwSize = strlen(pString);
	unsigned int dwProcessed = 0;

	while(dwSize != dwProcessed)
	{
		unsigned int unicode;
		unsigned int dwRet = utf8_mbtowc(&unicode, (const unsigned char*)pString + dwProcessed, dwSize - dwProcessed);
		if(dwRet == 0)
		{
			return;
		}

		pUnicode->push_back(unicode);
		dwProcessed += dwRet;
	}
}
bool ChwCore_String::Unicode2UTF8(void* pString, int nSize, string* pResult)
{	
	unsigned long wc = 0;
	unsigned char r[6];
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    nSize *= 4;
	for(int n = 0; n < nSize; n += 4)
#else
    nSize *= 2;
    for(int n = 0; n < nSize; n += 2)
#endif
	{
		memcpy((unsigned char*)&wc, (unsigned char*)pString + n, 2);

		int count;
		if (wc < 0x80)
			count = 1;
		else if (wc < 0x800)
			count = 2;
		else if (wc < 0x10000)
			count = 3;
		else if (wc < 0x200000)
			count = 4;
		else if (wc < 0x4000000)
			count = 5;
		else if (wc <= 0x7fffffff)
			count = 6;
		else
			return false;

		switch (count) { /* note: code falls through cases! */
		case 6: r[5] = 0x80 | (wc & 0x3f); wc = wc >> 6; wc |= 0x4000000;
		case 5: r[4] = 0x80 | (wc & 0x3f); wc = wc >> 6; wc |= 0x200000;
		case 4: r[3] = 0x80 | (wc & 0x3f); wc = wc >> 6; wc |= 0x10000;
		case 3: r[2] = 0x80 | (wc & 0x3f); wc = wc >> 6; wc |= 0x800;
		case 2: r[1] = 0x80 | (wc & 0x3f); wc = wc >> 6; wc |= 0xc0;
		case 1: r[0] = wc;
		}

		unsigned int dwSize = pResult->size();
		pResult->resize(dwSize + count);
		memcpy(&(*pResult)[dwSize], r, count);
	}

	//(*pResult) += '\0';
	
	return true;
}

ChwCore_String::ChwCore_String()
{
}
bool ChwCore_String::init(void)
{
	return true;
}

const unsigned char* ChwCore_String::Unicode2UTF8(void* pString, int nSize)
{
	m_Buffer.clear();
	
	unsigned long wc = 0;
	unsigned char r[6];
	for(int n = 2; n < nSize; n += 2)
	{
		memcpy((unsigned char*)&wc, (unsigned char*)pString + n, 2);

		int count;
		if (wc < 0x80)
			count = 1;
		else if (wc < 0x800)
			count = 2;
		else if (wc < 0x10000)
			count = 3;
		else if (wc < 0x200000)
			count = 4;
		else if (wc < 0x4000000)
			count = 5;
		else if (wc <= 0x7fffffff)
			count = 6;
		else
			return 0;

		switch (count) { /* note: code falls through cases! */
		case 6: r[5] = 0x80 | (wc & 0x3f); wc = wc >> 6; wc |= 0x4000000;
		case 5: r[4] = 0x80 | (wc & 0x3f); wc = wc >> 6; wc |= 0x200000;
		case 4: r[3] = 0x80 | (wc & 0x3f); wc = wc >> 6; wc |= 0x10000;
		case 3: r[2] = 0x80 | (wc & 0x3f); wc = wc >> 6; wc |= 0x800;
		case 2: r[1] = 0x80 | (wc & 0x3f); wc = wc >> 6; wc |= 0xc0;
		case 1: r[0] = wc;
		}

		unsigned int dwSize = m_Buffer.size();
		m_Buffer.resize(dwSize + count);
		memcpy(&m_Buffer[dwSize], r, count);
	}

	m_Buffer.push_back('\0');
	return &m_Buffer[0];
}  
#ifdef WIN32
string ChwCore_String::UTF8ToGBK(const std::string& strUTF8)
{  
    int len = MultiByteToWideChar(CP_UTF8, 0, strUTF8.c_str(), -1, NULL, 0);  
    WCHAR * wszGBK = new WCHAR[len + 1];  
    memset(wszGBK, 0, len * 2 + 2);  
    MultiByteToWideChar(CP_UTF8, 0, strUTF8.c_str(), -1, wszGBK, len);  
  
    len = WideCharToMultiByte(CP_ACP, 0, wszGBK, -1, NULL, 0, NULL, NULL);  
    char *szGBK = new char[len + 1];  
    memset(szGBK, 0, len + 1);  
    WideCharToMultiByte(CP_ACP,0, wszGBK, -1, szGBK, len, NULL, NULL);  
    //strUTF8 = szGBK;   
    std::string strTemp(szGBK);  
    delete[]szGBK;  
    delete[]wszGBK;  
    return strTemp;  
}  

string ChwCore_String::GBKToUTF8(const std::string& strGBK)  
{  
    string strOutUTF8 = "";  
    WCHAR * str1;  
    int n = MultiByteToWideChar(CP_ACP, 0, strGBK.c_str(), -1, NULL, 0);  
    str1 = new WCHAR[n];  
    MultiByteToWideChar(CP_ACP, 0, strGBK.c_str(), -1, str1, n);  
    n = WideCharToMultiByte(CP_UTF8, 0, str1, -1, NULL, 0, NULL, NULL);  
    char * str2 = new char[n];  
    WideCharToMultiByte(CP_UTF8, 0, str1, -1, str2, n, NULL, NULL);  
    strOutUTF8 = str2;  
    delete[]str1;  
    str1 = NULL;  
    delete[]str2;  
    str2 = NULL;  
    return strOutUTF8;  
}

#endif