#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include "hw3D_PPtcl.h"
#include "hw3D_A.h"

class Chw3D_Sprite;
class Chw3D_RPPtcl_CNode
{
public:
	Vec3							m_pos;
	Vec3							m_velocity;
	double							m_dAge;
	float							m_fRadian;
	float							m_fRadianSpeed;
};

class Chw3D_RPPtcl : public Node
{
public:
	Chw3D_PPtcl*					m_pPPtcl;

	unsigned int					m_dwMaxCount;
	double							m_dTotalSec;
	vector<Chw3D_RPPtcl_CNode>		m_nodes;

	vector<Vec3>					m_poses;
	vector<unsigned int>			m_colors;
	vector<Vec2>					m_uvs;

	vector<unsigned short>			m_tris;

	//
	GLint							m_nUniformColor;
	GLint							m_nUniformUVTrans;

protected:
	void							Build(Chw3D_Sprite* pSprite);

public:
									Chw3D_RPPtcl();
	virtual							~Chw3D_RPPtcl();

	bool							Create(Chw3D_PPtcl* pPPtcl);

	void							draw(Chw3D_Sprite* pSprite, bool bBuild);
};