#include "ODSocket.h"


#ifdef WIN32

	#pragma comment(lib, "wsock32")
#endif


ODSocket::ODSocket(SOCKET sock)
{
	m_sock = sock;
}

ODSocket::~ODSocket()
{
}

int ODSocket::Init()
{
#ifdef WIN32
	/*
	http://msdn.microsoft.com/zh-cn/vstudio/ms741563(en-us,VS.85).aspx

	typedef struct WSAData { 
		WORD wVersion;								//winsock version
		WORD wHighVersion;							//The highest version of the Windows Sockets specification that the Ws2_32.dll can support
		char szDescription[WSADESCRIPTION_LEN+1]; 
		char szSystemStatus[WSASYSSTATUS_LEN+1]; 
		unsigned short iMaxSockets; 
		unsigned short iMaxUdpDg; 
		char FAR * lpVendorInfo; 
	}WSADATA, *LPWSADATA; 
	*/
	WSADATA wsaData;
	//#define MAKEWORD(a,b) ((WORD) (((BYTE) (a)) | ((WORD) ((BYTE) (b))) << 8)) 
	WORD version = MAKEWORD(2, 0);
	int ret = WSAStartup(version, &wsaData);//win sock start up
	if ( ret ) {
//		cerr << "Initilize winsock error !" << endl;
		return -1;
	}
#endif
	
	return 0;
}
//this is just for windows
int ODSocket::Clean()
{
#ifdef WIN32
		return (WSACleanup());
#endif
		return 0;
}

ODSocket& ODSocket::operator = (SOCKET s)
{
	m_sock = s;
	return (*this);
}

ODSocket::operator SOCKET ()
{
	return m_sock;
}
//create a socket object win/lin is the same
// af:
bool ODSocket::Create(const char* ip, int type, int protocol)
{
    
    struct addrinfo addrin;
    struct addrinfo* pAddr = &addrin;
    int nRet = getaddrinfo(ip, nullptr, nullptr, &pAddr);
    int _ai_family;
    if(nRet != 0)
    {
        freeaddrinfo(pAddr);
        
        _ai_family = AF_UNSPEC;
        return false;
    }
    _ai_family = pAddr->ai_family;
    
    // 创建主套接字
    m_sock = socket(_ai_family, type, protocol);
    if ( m_sock == INVALID_SOCKET ) {
        
        return false;
    }
    return true;
}

bool ODSocket::ConnectWithSockaddrIn(sockaddr* svraddr, long svraddrlen, long ftimeout)
{
    if(0 == ftimeout)
    {
        int ret = connect(m_sock, (sockaddr*)svraddr, (socklen_t)svraddrlen);
        if ( ret == SOCKET_ERROR ) {
            return false;
        }
        return true;
    }
    else
    {
#ifdef _WIN32
        unsigned long flag = 1;
        struct timeval timeout;
        fd_set r;
        int ret;
        int error;
        int len = sizeof(int);
        
        if (ioctlsocket(m_sock,FIONBIO, &flag) != 0)  //…ËŒ™∑«◊Ë»˚ƒ£ Ω£¨’‚¿ÔflagŒ™1£¨ø…“‘≤Èø¥œ‡πÿ∫Ø ˝
        {
            Close();
            return false;
        }
        error = connect(m_sock, (PSOCKADDR)svraddr, svraddrlen);
        
        if (SOCKET_ERROR == error)
        {
            FD_ZERO(&r);
            FD_SET(m_sock, &r);
            
            timeout.tv_sec = ftimeout;
            timeout.tv_usec = 0;
            
            ret = select(0,0,&r,0,&timeout);
            
            if (ret > 0)
            {
                getsockopt(m_sock, SOL_SOCKET, SO_ERROR, (char*)&error, &len);
                
                if(error == 0)
                {
                    ret = true;
                }
                else
                {
                    ret = false;
                }
            }
            else
            {
                ret = false;
            }
        }
        else
        {
            ret = true;
        }
        
        if (!ret)
        {
            Close();
            return false;
        }
        
        flag = 0;
        if (ioctlsocket(m_sock, FIONBIO,(unsigned long*)&flag) == SOCKET_ERROR)  /** ◊™Œ™◊Ë»˚ƒ£ Ω,flag = 0*/
        {
            Close();
            return false;
        }
#else
        // linux…Ë÷√Ω” ’∫Õ∑¢ÀÕ≥¨ ±£¨ Ω‚æˆ¡¨Ω”≥¨ ±µƒbug  ¡¨Ω”≥…π¶∫ÛΩ´≥¨ ±…Ë÷√«Â≥˝£¨∑¿÷π‘⁄”∞œÏ∫Û–¯¬ﬂº≠
        struct timeval nTimeout = {(long)ftimeout, 0};
        if (SOCKET_ERROR == setsockopt(m_sock, SOL_SOCKET, SO_SNDTIMEO, (char*)&nTimeout, sizeof(nTimeout)))
        {
            Close();
            return false;
        } // ≥¨ ±–ﬁ∏ƒ
        
        bool ret = false;
        if(connect(m_sock, (sockaddr *)svraddr, (socklen_t)svraddrlen) == SOCKET_ERROR)
        {
            Close();
            return false;
        }
        else
        {
            ret = true;
            struct timeval nClearTimeout = {0, 0};
            if (SOCKET_ERROR == setsockopt(m_sock, SOL_SOCKET, SO_SNDTIMEO, (char*)&nClearTimeout, sizeof(nClearTimeout)))
            {
                Close();
                return false;
            }
        }
#endif
        return true;
    }
}



bool ODSocket::ConnectBoth46(const char* ip, unsigned short port, unsigned long ftimeout)
{
    std::vector<std::string> ips;
    
    struct addrinfo hints, *pAddr;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = PF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    hints.ai_protocol = IPPROTO_IP;
    
    int error = getaddrinfo(ip, nullptr, &hints, &pAddr);
    if (error != 0 )
    {
        return false;
    }
    
    if (pAddr->ai_family == AF_INET)
    {
        struct sockaddr_in* ipv4 = nullptr;
        char str[32] = {0};
        for (auto iter = pAddr; iter != nullptr; iter = iter->ai_next)
        {
            ipv4 = (struct sockaddr_in*)iter->ai_addr;
            inet_ntop(AF_INET, &ipv4->sin_addr, str, 32);
            ips.push_back(str);
        }
    }
    else if(pAddr->ai_family == AF_INET6)
    {
        struct sockaddr_in6* ipv6 = nullptr;
        char str[40] = {0};
        for (auto iter = pAddr; iter != nullptr; iter = iter->ai_next)
        {
            ipv6 = (struct sockaddr_in6*)iter->ai_addr;
            inet_ntop(AF_INET6, &ipv6->sin6_addr, str, 40);
            ips.push_back(str);
        }
    }
    
    int nReady;
    std::vector<std::string> ipVector = ips;
    if (pAddr->ai_family == AF_INET6)
    {
        for (auto iter = ipVector.begin(); iter!= ipVector.end(); iter++)
        {
            std::string ip = iter->c_str();
            sockaddr_in6 sa = {0};
            struct in6_addr addr = {0};
            inet_pton(pAddr->ai_family, ip.c_str(), &addr);
            
            sa.sin6_family = pAddr->ai_family;
            sa.sin6_port = htons(port);
            sa.sin6_addr = addr;
            
            nReady = ConnectWithSockaddrIn((sockaddr*)&sa, sizeof(sa), ftimeout);
            
            if (nReady == true) break;
        }
    }
    else
    {
        std::string ip = ipVector.at(0);
        sockaddr_in sa = {0};
        struct in_addr addr = {0};
        inet_pton(pAddr->ai_family, ip.c_str(), &addr);
        
        sa.sin_family = pAddr->ai_family;
        sa.sin_port = htons(port);
        sa.sin_addr = addr;
        
        nReady = ConnectWithSockaddrIn((sockaddr*)&sa, sizeof(sa), ftimeout);
    }
    
    freeaddrinfo(pAddr);
    
    return nReady;
    
}


bool ODSocket::Connect(const char* ip, unsigned short port, unsigned long ftimeout)
{
    struct sockaddr_in svraddr;
    svraddr.sin_family = AF_INET;
    //svraddr.sin_addr.s_addr = inet_addr(ip);
    svraddr.sin_port = htons(port);
    
    if(0 == ftimeout)
    {
        int ret = connect(m_sock, (struct sockaddr*)&svraddr, sizeof(svraddr));
        if ( ret == SOCKET_ERROR ) {
            return false;
        }
        return true;
    }
    else
    {
#ifdef _WIN32
        unsigned long flag = 1;
		struct timeval timeout;  
		fd_set r;  
		int ret;  
		int error;  
		int len = sizeof(int);  

		if (ioctlsocket(m_sock,FIONBIO, &flag) != 0)  //…ËŒ™∑«◊Ë»˚ƒ£ Ω£¨’‚¿ÔflagŒ™1£¨ø…“‘≤Èø¥œ‡πÿ∫Ø ˝  
		{  
			Close(); 
			return false;  
		}  
		error = connect(m_sock, (PSOCKADDR)&svraddr, sizeof(svraddr));  
  
		if (SOCKET_ERROR == error)  
		{  
			FD_ZERO(&r);  
			FD_SET(m_sock, &r);  
  
			timeout.tv_sec = ftimeout;  
			timeout.tv_usec = 0;  
  
			ret = select(0,0,&r,0,&timeout);  

			if (ret > 0)  
			{  
				getsockopt(m_sock, SOL_SOCKET, SO_ERROR, (char*)&error, &len);  
  
				if(error == 0)   
				{  
					ret = true;  
				}  
				else  
				{  
					ret = false;  
				}  
			}  
			else   
			{  
				ret = false;  
			}  
		}     
		else  
		{  
			 ret = true;  
		}  
  
		if (!ret)   
		{  
			Close();
			return false;  
		}  
  
		flag = 0;  
		if (ioctlsocket(m_sock, FIONBIO,(unsigned long*)&flag) == SOCKET_ERROR)  /** ◊™Œ™◊Ë»˚ƒ£ Ω,flag = 0*/  
		{  
			Close();
			return false;  
		} 
#else
		// linux…Ë÷√Ω” ’∫Õ∑¢ÀÕ≥¨ ±£¨ Ω‚æˆ¡¨Ω”≥¨ ±µƒbug  ¡¨Ω”≥…π¶∫ÛΩ´≥¨ ±…Ë÷√«Â≥˝£¨∑¿÷π‘⁄”∞œÏ∫Û–¯¬ﬂº≠
		struct timeval nTimeout = {(long)ftimeout, 0};
		if (SOCKET_ERROR == setsockopt(m_sock, SOL_SOCKET, SO_SNDTIMEO, (char*)&nTimeout, sizeof(nTimeout)))
		{
			Close();
			return false;
		} // ≥¨ ±–ﬁ∏ƒ
        
		bool ret = false;
		if(connect(m_sock, (sockaddr *)&svraddr, sizeof(svraddr)) == SOCKET_ERROR)
		{
			Close();
			return false;
		}
		else
		{
			ret = true;
			struct timeval nClearTimeout = {0, 0};
			if (SOCKET_ERROR == setsockopt(m_sock, SOL_SOCKET, SO_SNDTIMEO, (char*)&nClearTimeout, sizeof(nClearTimeout)))
			{
				Close();
				return false;
			}
		}
#endif
		return true;
	}
}

bool ODSocket::Bind(unsigned short port)
{
	struct sockaddr_in svraddr;
	svraddr.sin_family = AF_INET;
	svraddr.sin_addr.s_addr = INADDR_ANY;
	svraddr.sin_port = htons(port);

	int opt =  1;
	if ( setsockopt(m_sock, SOL_SOCKET, SO_REUSEADDR, (char*)&opt, sizeof(opt)) < 0 ) 
		return false;

	int ret = bind(m_sock, (struct sockaddr*)&svraddr, sizeof(svraddr));
	if ( ret == SOCKET_ERROR ) {
		return false;
	}
	return true;
}
//for server
bool ODSocket::Listen(int backlog)
{
	int ret = listen(m_sock, backlog);
	if ( ret == SOCKET_ERROR ) {
		return false;
	}
	return true;
}

bool ODSocket::Accept(ODSocket& s, char* fromip)
{
	struct sockaddr_in cliaddr;
	socklen_t addrlen = sizeof(cliaddr);
	SOCKET sock = accept(m_sock, (struct sockaddr*)&cliaddr, &addrlen);
	if ( sock == SOCKET_ERROR ) {
		return false;
	}

	s = sock;
	if ( fromip != NULL )
		sprintf(fromip, "%s", inet_ntoa(cliaddr.sin_addr));

	return true;
}

int ODSocket::Send(const char* buf, int len, int flags)
{
	int bytes;
	int count = 0;

	while ( count < len ) {

		bytes = send(m_sock, buf + count, len - count, flags);
		if ( bytes == -1 || bytes == 0 )
			return -1;
		count += bytes;
	} 

	return count;
}

int ODSocket::Recv(char* buf, int len, int flags)
{
	return (recv(m_sock, buf, len, flags));
}

int ODSocket::Close()
{
#ifdef WIN32
	return (closesocket(m_sock));
#else
	return (close(m_sock));
#endif
}

int ODSocket::GetError()
{
#ifdef WIN32
	return (WSAGetLastError());
#else
	return (0);
#endif
}

bool ODSocket::DnsParse(const char* domain, char* ip)
{
	struct hostent* p;
	if ( (p = gethostbyname(domain)) == NULL )
		return false;
		
	sprintf(ip, 
		"%u.%u.%u.%u",
		(unsigned char)p->h_addr_list[0][0], 
		(unsigned char)p->h_addr_list[0][1], 
		(unsigned char)p->h_addr_list[0][2], 
		(unsigned char)p->h_addr_list[0][3]);
	
	return true;
}

int ODSocket::ShutDown(int how)
{
	return shutdown(m_sock, how);
}
