#pragma once

#include <atomic>
#include <mutex>
#include <map>
#include <queue>
#include <thread>
#include "ODSocket.h"
#include "cocos2d.h"
#include "hwNetworkMsg.h"

USING_NS_CC;
class ChwNetworkClient:  public Node
{

public:
	static const int MAX_BUFSIZE = 65535 * 2;
	static const int HEADER_LENGTH = 10;

	enum STATE
	{
		S_INIT					= 0,
		S_CONNECTING,
		S_CONNECTED,
		S_ESTABLISHED,	
		S_CLOSE,							
		S_CLOSEED,	
	};

	enum CloseReason
	{
		CR_CLOSE_SELF = 0,		// 主动关闭
		CR_NET_CLOSE,		//网络错误
		CR_SERVER_CLOSE,	//服务器关闭
		CR_SEND_FAILED_ERR, //发送失败
		CR_CONNECT_FAILED_ERR,
	};

public:

	std::string		m_ip;
	unsigned short	m_wPort;

public:
	
	ChwNetworkClient();
	~ChwNetworkClient();

	bool Connect(std::string ip, unsigned short wPort);
	bool Send(unsigned short msgType, std::string msg);



	void Close(unsigned short reason = 0);
	int GetState();
	void RegisterHandler(std::string type, int luaHandle);
	void SetTimeout(float t);

private:

	void recvThread();
	void Closeed();

private:

	STATE						m_state;

	bool						m_isConnected;

	std::map<std::string, int>	m_luaHandles;

	ODSocket					m_socket;

	std::thread					m_rcveThread;
	std::mutex					m_readMutex;

	std::vector<char>			m_Buffer;        //数据缓冲数组
	unsigned int				m_unreadLength;  //未读长度
	unsigned int				m_readOffset;    //已读

	bool						m_isRecvThreadClose;
	int							m_closeReason;

	std::mutex					m_changeStateMutex;

	float						m_timeout;
	bool						m_isDebug;

public:
	void update(float delta);

};