#ifndef _WAVE_SHADER_H_
#define _WAVE_SHADER_H_
#include "ui/CocosGUI.h"

class WaveShader : public cocos2d::Node
{
public:
	
	enum class playState
	{
		unknown,
		playing,
		stop,
	};


	typedef std::function<void()> WaveShaderCallback;

	CREATE_FUNC(WaveShader);
	static WaveShader* shaderNodeWithVertex(const std::string &vert, const std::string &frag, cocos2d::Texture2D* _texture=nullptr);
	void loadShaderVertex(const std::string &vert, const std::string &frag);
	virtual void update(float dt) override;
	virtual void setPosition(const cocos2d::Vec2 &newPosition) override;
	virtual void draw(cocos2d::Renderer* renderer, const cocos2d::Mat4& transform, uint32_t flags) override;
	void setResolution(int width, int height);
	void setTexture2D(cocos2d::Texture2D* texture2d);
	void play(float endtime, const WaveShaderCallback& callback);

protected:
	WaveShader();
	~WaveShader();

	bool initWithVertex(const std::string &vert, const std::string &frag);

	void onDraw(const cocos2d::Mat4& transform, uint32_t flags);

	cocos2d::Vec2 _resolution;
	float     _time;
	float	  _endtime;
	std::string _vertFileName;
	std::string _fragFileName;
	cocos2d::CustomCommand _customCommand;
	cocos2d::Texture2D* _texture2d;
	WaveShaderCallback _callback;
	playState _state;
	
};

#endif
