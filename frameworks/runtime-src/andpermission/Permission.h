#pragma once
#include "cocos2d.h"
class Permission
{
public:
	static const std::string READ_CALENDAR;
	static const std::string WRITE_CALENDAR;

	static const std::string CAMERA;

	static const std::string READ_CONTACTS;
	static const std::string WRITE_CONTACTS;
	static const std::string GET_ACCOUNTS;

	static const std::string ACCESS_FINE_LOCATION;
	static const std::string ACCESS_COARSE_LOCATION;

	static const std::string RECORD_AUDIO;

	static const std::string READ_PHONE_STATE;
	static const std::string CALL_PHONE;
	static const std::string READ_CALL_LOG;
	static const std::string WRITE_CALL_LOG;
	static const std::string ADD_VOICEMAIL;
	static const std::string USE_SIP;
	static const std::string PROCESS_OUTGOING_CALLS;

	static const std::string BODY_SENSORS;

	static const std::string SEND_SMS;
	static const std::string RECEIVE_SMS;
	static const std::string READ_SMS;
	static const std::string RECEIVE_WAP_PUSH;
	static const std::string RECEIVE_MMS;

	static const std::string READ_EXTERNAL_STORAGE;
	static const std::string WRITE_EXTERNAL_STORAGE;

	static const std::string GROUP_CALENDAR;
	static const std::string GROUP_CAMERA;
	static const std::string GROUP_CONTACTS;
	static const std::string GROUP_LOCATION;
	static const std::string GROUP_MICROPHONE;
	static const std::string GROUP_PHONE;
	static const std::string GROUP_SENSORS;
	static const std::string GROUP_SMS;
	static const std::string GROUP_STORAGE;

	static void requestPermission(std::string permission);
	static bool hasPermission(std::string permission);
};