#include "higame_extensions.h"

#if __cplusplus
extern "C" {
#endif

// cjson
#include "lua_cjson.h"
// lsqlite3
#include "lsqlite3.h"

static luaL_Reg luax_exts[] = {
	{ "cjson", luaopen_cjson_safe },
//#if CC_USE_SQLITE
    {"lsqlite3", luaopen_lsqlite3},
//#endif
    {NULL, NULL}
};

void luaopen_higame_extensions(lua_State *L)
{
    // load extensions
    luaL_Reg* lib = luax_exts;
    lua_getglobal(L, "package");
    lua_getfield(L, -1, "preload");
    for (; lib->func; lib++)
    {
        lua_pushcfunction(L, lib->func);
        lua_setfield(L, -2, lib->name);
    }
    lua_pop(L, 2);
}

#if __cplusplus
} // extern "C"
#endif
