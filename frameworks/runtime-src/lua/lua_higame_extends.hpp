#include "base/ccConfig.h"
#ifndef __higame_extends_h__
#define __higame_extends_h__

#ifdef __cplusplus
extern "C" {
#endif
#include "tolua++.h"
#ifdef __cplusplus
}
#endif

int register_higame_module(lua_State* tolua_S);


#endif