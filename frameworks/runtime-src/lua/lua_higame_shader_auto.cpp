#include "../lua/lua_higame_shader_auto.hpp"
#include "WaveShader.h"
#include "scripting/lua-bindings/manual/tolua_fix.h"
#include "scripting/lua-bindings/manual/LuaBasicConversions.h"

int lua_higame_shader_WaveShader_setResolution(lua_State* tolua_S)
{
    int argc = 0;
    WaveShader* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"WaveShader",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (WaveShader*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_shader_WaveShader_setResolution'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        int arg0;
        int arg1;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "WaveShader:setResolution");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "WaveShader:setResolution");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_shader_WaveShader_setResolution'", nullptr);
            return 0;
        }
        cobj->setResolution(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "WaveShader:setResolution",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_shader_WaveShader_setResolution'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_shader_WaveShader_setTexture2D(lua_State* tolua_S)
{
    int argc = 0;
    WaveShader* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"WaveShader",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (WaveShader*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_shader_WaveShader_setTexture2D'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        cocos2d::Texture2D* arg0;

        ok &= luaval_to_object<cocos2d::Texture2D>(tolua_S, 2, "cc.Texture2D",&arg0, "WaveShader:setTexture2D");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_shader_WaveShader_setTexture2D'", nullptr);
            return 0;
        }
        cobj->setTexture2D(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "WaveShader:setTexture2D",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_shader_WaveShader_setTexture2D'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_shader_WaveShader_loadShaderVertex(lua_State* tolua_S)
{
    int argc = 0;
    WaveShader* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"WaveShader",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (WaveShader*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_shader_WaveShader_loadShaderVertex'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        std::string arg0;
        std::string arg1;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "WaveShader:loadShaderVertex");

        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "WaveShader:loadShaderVertex");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_shader_WaveShader_loadShaderVertex'", nullptr);
            return 0;
        }
        cobj->loadShaderVertex(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "WaveShader:loadShaderVertex",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_shader_WaveShader_loadShaderVertex'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_shader_WaveShader_shaderNodeWithVertex(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"WaveShader",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 2)
    {
        std::string arg0;
        std::string arg1;
        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "WaveShader:shaderNodeWithVertex");
        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "WaveShader:shaderNodeWithVertex");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_shader_WaveShader_shaderNodeWithVertex'", nullptr);
            return 0;
        }
        WaveShader* ret = WaveShader::shaderNodeWithVertex(arg0, arg1);
        object_to_luaval<WaveShader>(tolua_S, "WaveShader",(WaveShader*)ret);
        return 1;
    }
    if (argc == 3)
    {
        std::string arg0;
        std::string arg1;
        cocos2d::Texture2D* arg2;
        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "WaveShader:shaderNodeWithVertex");
        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "WaveShader:shaderNodeWithVertex");
        ok &= luaval_to_object<cocos2d::Texture2D>(tolua_S, 4, "cc.Texture2D",&arg2, "WaveShader:shaderNodeWithVertex");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_shader_WaveShader_shaderNodeWithVertex'", nullptr);
            return 0;
        }
        WaveShader* ret = WaveShader::shaderNodeWithVertex(arg0, arg1, arg2);
        object_to_luaval<WaveShader>(tolua_S, "WaveShader",(WaveShader*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "WaveShader:shaderNodeWithVertex",argc, 2);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_shader_WaveShader_shaderNodeWithVertex'.",&tolua_err);
#endif
    return 0;
}
int lua_higame_shader_WaveShader_create(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"WaveShader",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_shader_WaveShader_create'", nullptr);
            return 0;
        }
        WaveShader* ret = WaveShader::create();
        object_to_luaval<WaveShader>(tolua_S, "WaveShader",(WaveShader*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "WaveShader:create",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_shader_WaveShader_create'.",&tolua_err);
#endif
    return 0;
}
static int lua_higame_shader_WaveShader_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (WaveShader)");
    return 0;
}

int lua_register_higame_shader_WaveShader(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"WaveShader");
    tolua_cclass(tolua_S,"WaveShader","WaveShader","cc.Node",nullptr);

    tolua_beginmodule(tolua_S,"WaveShader");
        tolua_function(tolua_S,"setResolution",lua_higame_shader_WaveShader_setResolution);
        tolua_function(tolua_S,"setTexture2D",lua_higame_shader_WaveShader_setTexture2D);
        tolua_function(tolua_S,"loadShaderVertex",lua_higame_shader_WaveShader_loadShaderVertex);
        tolua_function(tolua_S,"shaderNodeWithVertex", lua_higame_shader_WaveShader_shaderNodeWithVertex);
        tolua_function(tolua_S,"create", lua_higame_shader_WaveShader_create);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(WaveShader).name();
    g_luaType[typeName] = "WaveShader";
    g_typeCast["WaveShader"] = "WaveShader";
    return 1;
}
TOLUA_API int register_all_higame_shader(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,"higame",0);
	tolua_beginmodule(tolua_S,"higame");

	lua_register_higame_shader_WaveShader(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

