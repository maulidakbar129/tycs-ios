#include "../lua/lua_higame_sdk_auto.hpp"
#include "HiGameSDK.h"
#include "scripting/lua-bindings/manual/tolua_fix.h"
#include "scripting/lua-bindings/manual/LuaBasicConversions.h"

int lua_higamesdk_HiGameSDK_getAppInfo(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_getAppInfo'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_getAppInfo'", nullptr);
            return 0;
        }
        std::string ret = cobj->getAppInfo();
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:getAppInfo",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_getAppInfo'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_deal(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_deal'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "HiGameSDK:deal");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_deal'", nullptr);
            return 0;
        }
        cobj->deal(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:deal",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_deal'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_getLogicChannel(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_getLogicChannel'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_getLogicChannel'", nullptr);
            return 0;
        }
        int ret = cobj->getLogicChannel();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:getLogicChannel",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_getLogicChannel'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_initSDK(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_initSDK'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_initSDK'", nullptr);
            return 0;
        }
        cobj->initSDK();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:initSDK",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_initSDK'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_switchAccount(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_switchAccount'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "HiGameSDK:switchAccount");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_switchAccount'", nullptr);
            return 0;
        }
        cobj->switchAccount(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:switchAccount",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_switchAccount'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_isInitSDK(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_isInitSDK'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_isInitSDK'", nullptr);
            return 0;
        }
        bool ret = cobj->isInitSDK();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:isInitSDK",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_isInitSDK'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_getUserID(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_getUserID'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_getUserID'", nullptr);
            return 0;
        }
        const std::string& ret = cobj->getUserID();
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:getUserID",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_getUserID'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_callOtherFunction(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_callOtherFunction'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        int arg0;
        std::string arg1;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "HiGameSDK:callOtherFunction");

        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "HiGameSDK:callOtherFunction");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_callOtherFunction'", nullptr);
            return 0;
        }
        cobj->callOtherFunction(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:callOtherFunction",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_callOtherFunction'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_onCallback(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_onCallback'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 3) 
    {
        int arg0;
        int arg1;
        std::string arg2;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "HiGameSDK:onCallback");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "HiGameSDK:onCallback");

        ok &= luaval_to_std_string(tolua_S, 4,&arg2, "HiGameSDK:onCallback");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_onCallback'", nullptr);
            return 0;
        }
        cobj->onCallback(arg0, arg1, arg2);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:onCallback",argc, 3);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_onCallback'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_exit(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_exit'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_exit'", nullptr);
            return 0;
        }
        cobj->exit();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:exit",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_exit'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_getSubChannel(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_getSubChannel'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_getSubChannel'", nullptr);
            return 0;
        }
        std::string ret = cobj->getSubChannel();
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:getSubChannel",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_getSubChannel'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_share(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_share'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "HiGameSDK:share");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_share'", nullptr);
            return 0;
        }
        cobj->share(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:share",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_share'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_getConfig(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_getConfig'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_getConfig'", nullptr);
            return 0;
        }
        const std::string& ret = cobj->getConfig();
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:getConfig",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_getConfig'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_getAppID(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_getAppID'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_getAppID'", nullptr);
            return 0;
        }
        int ret = cobj->getAppID();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:getAppID",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_getAppID'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_logout(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_logout'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "HiGameSDK:logout");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_logout'", nullptr);
            return 0;
        }
        cobj->logout(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:logout",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_logout'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_isLogined(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_isLogined'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_isLogined'", nullptr);
            return 0;
        }
        bool ret = cobj->isLogined();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:isLogined",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_isLogined'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_getValue(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_getValue'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "HiGameSDK:getValue");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_getValue'", nullptr);
            return 0;
        }
        const std::string& ret = cobj->getValue(arg0);
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:getValue",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_getValue'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_submitExtendData(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_submitExtendData'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "HiGameSDK:submitExtendData");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_submitExtendData'", nullptr);
            return 0;
        }
        cobj->submitExtendData(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:submitExtendData",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_submitExtendData'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_login(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_login'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "HiGameSDK:login");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_login'", nullptr);
            return 0;
        }
        cobj->login(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:login",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_login'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_getMainChannel(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (HiGameSDK*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higamesdk_HiGameSDK_getMainChannel'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_getMainChannel'", nullptr);
            return 0;
        }
        std::string ret = cobj->getMainChannel();
        lua_pushlstring(tolua_S,ret.c_str(),ret.length());
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:getMainChannel",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_getMainChannel'.",&tolua_err);
#endif

    return 0;
}
int lua_higamesdk_HiGameSDK_destroyInstance(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_destroyInstance'", nullptr);
            return 0;
        }
        HiGameSDK::destroyInstance();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "HiGameSDK:destroyInstance",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_destroyInstance'.",&tolua_err);
#endif
    return 0;
}
int lua_higamesdk_HiGameSDK_getInstance(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"HiGameSDK",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_getInstance'", nullptr);
            return 0;
        }
        HiGameSDK* ret = HiGameSDK::getInstance();
        object_to_luaval<HiGameSDK>(tolua_S, "HiGameSDK",(HiGameSDK*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "HiGameSDK:getInstance",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_getInstance'.",&tolua_err);
#endif
    return 0;
}
int lua_higamesdk_HiGameSDK_constructor(lua_State* tolua_S)
{
    int argc = 0;
    HiGameSDK* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higamesdk_HiGameSDK_constructor'", nullptr);
            return 0;
        }
        cobj = new HiGameSDK();
        cobj->autorelease();
        int ID =  (int)cobj->_ID ;
        int* luaID =  &cobj->_luaID ;
        toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"HiGameSDK");
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "HiGameSDK:HiGameSDK",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_higamesdk_HiGameSDK_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_higamesdk_HiGameSDK_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (HiGameSDK)");
    return 0;
}

int lua_register_higamesdk_HiGameSDK(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"HiGameSDK");
    tolua_cclass(tolua_S,"HiGameSDK","HiGameSDK","cc.Ref",nullptr);

    tolua_beginmodule(tolua_S,"HiGameSDK");
        tolua_function(tolua_S,"new",lua_higamesdk_HiGameSDK_constructor);
        tolua_function(tolua_S,"getAppInfo",lua_higamesdk_HiGameSDK_getAppInfo);
        tolua_function(tolua_S,"deal",lua_higamesdk_HiGameSDK_deal);
        tolua_function(tolua_S,"getLogicChannel",lua_higamesdk_HiGameSDK_getLogicChannel);
        tolua_function(tolua_S,"initSDK",lua_higamesdk_HiGameSDK_initSDK);
        tolua_function(tolua_S,"switchAccount",lua_higamesdk_HiGameSDK_switchAccount);
        tolua_function(tolua_S,"isInitSDK",lua_higamesdk_HiGameSDK_isInitSDK);
        tolua_function(tolua_S,"getUserID",lua_higamesdk_HiGameSDK_getUserID);
        tolua_function(tolua_S,"callOtherFunction",lua_higamesdk_HiGameSDK_callOtherFunction);
        tolua_function(tolua_S,"onCallback",lua_higamesdk_HiGameSDK_onCallback);
        tolua_function(tolua_S,"exit",lua_higamesdk_HiGameSDK_exit);
        tolua_function(tolua_S,"getSubChannel",lua_higamesdk_HiGameSDK_getSubChannel);
        tolua_function(tolua_S,"share",lua_higamesdk_HiGameSDK_share);
        tolua_function(tolua_S,"getConfig",lua_higamesdk_HiGameSDK_getConfig);
        tolua_function(tolua_S,"getAppID",lua_higamesdk_HiGameSDK_getAppID);
        tolua_function(tolua_S,"logout",lua_higamesdk_HiGameSDK_logout);
        tolua_function(tolua_S,"isLogined",lua_higamesdk_HiGameSDK_isLogined);
        tolua_function(tolua_S,"getValue",lua_higamesdk_HiGameSDK_getValue);
        tolua_function(tolua_S,"submitExtendData",lua_higamesdk_HiGameSDK_submitExtendData);
        tolua_function(tolua_S,"login",lua_higamesdk_HiGameSDK_login);
        tolua_function(tolua_S,"getMainChannel",lua_higamesdk_HiGameSDK_getMainChannel);
        tolua_function(tolua_S,"destroyInstance", lua_higamesdk_HiGameSDK_destroyInstance);
        tolua_function(tolua_S,"getInstance", lua_higamesdk_HiGameSDK_getInstance);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(HiGameSDK).name();
    g_luaType[typeName] = "HiGameSDK";
    g_typeCast["HiGameSDK"] = "HiGameSDK";
    return 1;
}
TOLUA_API int register_all_higamesdk(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,"higame",0);
	tolua_beginmodule(tolua_S,"higame");

	lua_register_higamesdk_HiGameSDK(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

