#include "../lua/lua_higame_ui_auto.hpp"
#include "ui/CocosGUI.h"
#include "UIDyListView.h"
#include "scripting/lua-bindings/manual/tolua_fix.h"
#include "scripting/lua-bindings/manual/LuaBasicConversions.h"

int lua_higame_ui_DyListViewItem_isLoaded(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListViewItem* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListViewItem",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListViewItem*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListViewItem_isLoaded'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListViewItem_isLoaded'", nullptr);
            return 0;
        }
        bool ret = cobj->isLoaded();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListViewItem:isLoaded",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListViewItem_isLoaded'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListViewItem_setLoaded(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListViewItem* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListViewItem",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListViewItem*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListViewItem_setLoaded'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "ccui.DyListViewItem:setLoaded");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListViewItem_setLoaded'", nullptr);
            return 0;
        }
        cobj->setLoaded(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListViewItem:setLoaded",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListViewItem_setLoaded'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListViewItem_create(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"ccui.DyListViewItem",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 1)
    {
        cocos2d::ui::DyListView* arg0;
        ok &= luaval_to_object<cocos2d::ui::DyListView>(tolua_S, 2, "ccui.DyListView",&arg0, "ccui.DyListViewItem:create");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListViewItem_create'", nullptr);
            return 0;
        }
        cocos2d::ui::DyListViewItem* ret = cocos2d::ui::DyListViewItem::create(arg0);
        object_to_luaval<cocos2d::ui::DyListViewItem>(tolua_S, "ccui.DyListViewItem",(cocos2d::ui::DyListViewItem*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "ccui.DyListViewItem:create",argc, 1);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListViewItem_create'.",&tolua_err);
#endif
    return 0;
}
int lua_higame_ui_DyListViewItem_constructor(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListViewItem* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListViewItem_constructor'", nullptr);
            return 0;
        }
        cobj = new cocos2d::ui::DyListViewItem();
        cobj->autorelease();
        int ID =  (int)cobj->_ID ;
        int* luaID =  &cobj->_luaID ;
        toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"ccui.DyListViewItem");
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListViewItem:DyListViewItem",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListViewItem_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_higame_ui_DyListViewItem_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (DyListViewItem)");
    return 0;
}

int lua_register_higame_ui_DyListViewItem(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"ccui.DyListViewItem");
    tolua_cclass(tolua_S,"DyListViewItem","ccui.DyListViewItem","ccui.Layout",nullptr);

    tolua_beginmodule(tolua_S,"DyListViewItem");
        tolua_function(tolua_S,"new",lua_higame_ui_DyListViewItem_constructor);
        tolua_function(tolua_S,"isLoaded",lua_higame_ui_DyListViewItem_isLoaded);
        tolua_function(tolua_S,"setLoaded",lua_higame_ui_DyListViewItem_setLoaded);
        tolua_function(tolua_S,"create", lua_higame_ui_DyListViewItem_create);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(cocos2d::ui::DyListViewItem).name();
    g_luaType[typeName] = "ccui.DyListViewItem";
    g_typeCast["DyListViewItem"] = "ccui.DyListViewItem";
    return 1;
}

int lua_higame_ui_DyListView_getItemPositionRatioInView(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_getItemPositionRatioInView'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        cocos2d::ui::Widget* arg0;

        ok &= luaval_to_object<cocos2d::ui::Widget>(tolua_S, 2, "ccui.Widget",&arg0, "ccui.DyListView:getItemPositionRatioInView");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_getItemPositionRatioInView'", nullptr);
            return 0;
        }
        cocos2d::Vec2 ret = cobj->getItemPositionRatioInView(arg0);
        vec2_to_luaval(tolua_S, ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:getItemPositionRatioInView",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_getItemPositionRatioInView'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_setItemSizeDirty(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_setItemSizeDirty'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "ccui.DyListView:setItemSizeDirty");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_setItemSizeDirty'", nullptr);
            return 0;
        }
        cobj->setItemSizeDirty(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:setItemSizeDirty",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_setItemSizeDirty'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_setItemsCount(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_setItemsCount'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        unsigned int arg0;

        ok &= luaval_to_uint32(tolua_S, 2,&arg0, "ccui.DyListView:setItemsCount");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_setItemsCount'", nullptr);
            return 0;
        }
        cobj->setItemsCount(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:setItemsCount",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_setItemsCount'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_reloadToItem(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_reloadToItem'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        ssize_t arg0;

        ok &= luaval_to_ssize(tolua_S, 2, &arg0, "ccui.DyListView:reloadToItem");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_reloadToItem'", nullptr);
            return 0;
        }
        cobj->reloadToItem(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:reloadToItem",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_reloadToItem'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_setCacheViewSize(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_setCacheViewSize'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "ccui.DyListView:setCacheViewSize");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_setCacheViewSize'", nullptr);
            return 0;
        }
        cobj->setCacheViewSize(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:setCacheViewSize",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_setCacheViewSize'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_setDefaultItemSize(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_setDefaultItemSize'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        cocos2d::Size arg0;

        ok &= luaval_to_size(tolua_S, 2, &arg0, "ccui.DyListView:setDefaultItemSize");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_setDefaultItemSize'", nullptr);
            return 0;
        }
        cobj->setDefaultItemSize(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:setDefaultItemSize",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_setDefaultItemSize'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_getItemDistanceInCurrentView(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_getItemDistanceInCurrentView'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        cocos2d::ui::Widget* arg0;

        ok &= luaval_to_object<cocos2d::ui::Widget>(tolua_S, 2, "ccui.Widget",&arg0, "ccui.DyListView:getItemDistanceInCurrentView");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_getItemDistanceInCurrentView'", nullptr);
            return 0;
        }
        double ret = cobj->getItemDistanceInCurrentView(arg0);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:getItemDistanceInCurrentView",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_getItemDistanceInCurrentView'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_isCacheItemsEnable(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_isCacheItemsEnable'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_isCacheItemsEnable'", nullptr);
            return 0;
        }
        bool ret = cobj->isCacheItemsEnable();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:isCacheItemsEnable",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_isCacheItemsEnable'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_getDatumPoint(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_getDatumPoint'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_getDatumPoint'", nullptr);
            return 0;
        }
        cocos2d::Vec2 ret = cobj->getDatumPoint();
        vec2_to_luaval(tolua_S, ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:getDatumPoint",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_getDatumPoint'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_reload(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_reload'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_reload'", nullptr);
            return 0;
        }
        cobj->reload();
        lua_settop(tolua_S, 1);
        return 1;
    }
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "ccui.DyListView:reload");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_reload'", nullptr);
            return 0;
        }
        cobj->reload(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:reload",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_reload'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_getItemLua(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_getItemLua'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        ssize_t arg0;

        ok &= luaval_to_ssize(tolua_S, 2, &arg0, "ccui.DyListView:getItemLua");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_getItemLua'", nullptr);
            return 0;
        }
        cocos2d::ui::Widget* ret = cobj->getItemLua(arg0);
        object_to_luaval<cocos2d::ui::Widget>(tolua_S, "ccui.Widget",(cocos2d::ui::Widget*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:getItemLua",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_getItemLua'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_setCacheItemsEnable(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_setCacheItemsEnable'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        bool arg0;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "ccui.DyListView:setCacheItemsEnable");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_setCacheItemsEnable'", nullptr);
            return 0;
        }
        cobj->setCacheItemsEnable(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:setCacheItemsEnable",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_setCacheItemsEnable'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_getIndexLua(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_getIndexLua'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        cocos2d::ui::Widget* arg0;

        ok &= luaval_to_object<cocos2d::ui::Widget>(tolua_S, 2, "ccui.Widget",&arg0, "ccui.DyListView:getIndexLua");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_getIndexLua'", nullptr);
            return 0;
        }
        ssize_t ret = cobj->getIndexLua(arg0);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:getIndexLua",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_getIndexLua'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_jumpToItem(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_jumpToItem'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 3) 
    {
        ssize_t arg0;
        cocos2d::Vec2 arg1;
        cocos2d::Vec2 arg2;

        ok &= luaval_to_ssize(tolua_S, 2, &arg0, "ccui.DyListView:jumpToItem");

        ok &= luaval_to_vec2(tolua_S, 3, &arg1, "ccui.DyListView:jumpToItem");

        ok &= luaval_to_vec2(tolua_S, 4, &arg2, "ccui.DyListView:jumpToItem");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_jumpToItem'", nullptr);
            return 0;
        }
        cobj->jumpToItem(arg0, arg1, arg2);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:jumpToItem",argc, 3);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_jumpToItem'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_getDatumItem(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_getDatumItem'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_getDatumItem'", nullptr);
            return 0;
        }
        cocos2d::ui::Widget* ret = cobj->getDatumItem();
        object_to_luaval<cocos2d::ui::Widget>(tolua_S, "ccui.Widget",(cocos2d::ui::Widget*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:getDatumItem",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_getDatumItem'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_isItemInView(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;
#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif
    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);
#if COCOS2D_DEBUG >= 1
    if (!cobj)
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_isItemInView'", nullptr);
        return 0;
    }
#endif
    argc = lua_gettop(tolua_S)-1;
    do{
        if (argc == 1) {
            unsigned int arg0;
            ok &= luaval_to_uint32(tolua_S, 2,&arg0, "ccui.DyListView:isItemInView");

            if (!ok) { break; }
            bool ret = cobj->isItemInView(arg0);
            tolua_pushboolean(tolua_S,(bool)ret);
            return 1;
        }
    }while(0);
    ok  = true;
    do{
        if (argc == 1) {
            cocos2d::ui::Widget* arg0;
            ok &= luaval_to_object<cocos2d::ui::Widget>(tolua_S, 2, "ccui.Widget",&arg0, "ccui.DyListView:isItemInView");

            if (!ok) { break; }
            bool ret = cobj->isItemInView(arg0);
            tolua_pushboolean(tolua_S,(bool)ret);
            return 1;
        }
    }while(0);
    ok  = true;
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n",  "ccui.DyListView:isItemInView",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_isItemInView'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_getShowedItems(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_getShowedItems'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_getShowedItems'", nullptr);
            return 0;
        }
        cocos2d::Vector<cocos2d::ui::Widget *>& ret = cobj->getShowedItems();
        ccvector_to_luaval(tolua_S, ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:getShowedItems",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_getShowedItems'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_getInnerContainerPos(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::ui::DyListView*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_higame_ui_DyListView_getInnerContainerPos'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_getInnerContainerPos'", nullptr);
            return 0;
        }
        double ret = cobj->getInnerContainerPos();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:getInnerContainerPos",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_getInnerContainerPos'.",&tolua_err);
#endif

    return 0;
}
int lua_higame_ui_DyListView_create(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"ccui.DyListView",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_create'", nullptr);
            return 0;
        }
        cocos2d::ui::DyListView* ret = cocos2d::ui::DyListView::create();
        object_to_luaval<cocos2d::ui::DyListView>(tolua_S, "ccui.DyListView",(cocos2d::ui::DyListView*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "ccui.DyListView:create",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_create'.",&tolua_err);
#endif
    return 0;
}
int lua_higame_ui_DyListView_constructor(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::ui::DyListView* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_higame_ui_DyListView_constructor'", nullptr);
            return 0;
        }
        cobj = new cocos2d::ui::DyListView();
        cobj->autorelease();
        int ID =  (int)cobj->_ID ;
        int* luaID =  &cobj->_luaID ;
        toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"ccui.DyListView");
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccui.DyListView:DyListView",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_higame_ui_DyListView_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_higame_ui_DyListView_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (DyListView)");
    return 0;
}

int lua_register_higame_ui_DyListView(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"ccui.DyListView");
    tolua_cclass(tolua_S,"DyListView","ccui.DyListView","ccui.ListView",nullptr);

    tolua_beginmodule(tolua_S,"DyListView");
        tolua_function(tolua_S,"new",lua_higame_ui_DyListView_constructor);
        tolua_function(tolua_S,"getItemPositionRatioInView",lua_higame_ui_DyListView_getItemPositionRatioInView);
        tolua_function(tolua_S,"setItemSizeDirty",lua_higame_ui_DyListView_setItemSizeDirty);
        tolua_function(tolua_S,"setItemsCount",lua_higame_ui_DyListView_setItemsCount);
        tolua_function(tolua_S,"reloadToItem",lua_higame_ui_DyListView_reloadToItem);
        tolua_function(tolua_S,"setCacheViewSize",lua_higame_ui_DyListView_setCacheViewSize);
        tolua_function(tolua_S,"setDefaultItemSize",lua_higame_ui_DyListView_setDefaultItemSize);
        tolua_function(tolua_S,"getItemDistanceInCurrentView",lua_higame_ui_DyListView_getItemDistanceInCurrentView);
        tolua_function(tolua_S,"isCacheItemsEnable",lua_higame_ui_DyListView_isCacheItemsEnable);
        tolua_function(tolua_S,"getDatumPoint",lua_higame_ui_DyListView_getDatumPoint);
        tolua_function(tolua_S,"reload",lua_higame_ui_DyListView_reload);
        tolua_function(tolua_S,"getItemLua",lua_higame_ui_DyListView_getItemLua);
        tolua_function(tolua_S,"setCacheItemsEnable",lua_higame_ui_DyListView_setCacheItemsEnable);
        tolua_function(tolua_S,"getIndexLua",lua_higame_ui_DyListView_getIndexLua);
        tolua_function(tolua_S,"jumpToItem",lua_higame_ui_DyListView_jumpToItem);
        tolua_function(tolua_S,"getDatumItem",lua_higame_ui_DyListView_getDatumItem);
        tolua_function(tolua_S,"isItemInView",lua_higame_ui_DyListView_isItemInView);
        tolua_function(tolua_S,"getShowedItems",lua_higame_ui_DyListView_getShowedItems);
        tolua_function(tolua_S,"getInnerContainerPos",lua_higame_ui_DyListView_getInnerContainerPos);
        tolua_function(tolua_S,"create", lua_higame_ui_DyListView_create);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(cocos2d::ui::DyListView).name();
    g_luaType[typeName] = "ccui.DyListView";
    g_typeCast["DyListView"] = "ccui.DyListView";
    return 1;
}
TOLUA_API int register_all_higame_ui(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,"ccui",0);
	tolua_beginmodule(tolua_S,"ccui");

	lua_register_higame_ui_DyListView(tolua_S);
	lua_register_higame_ui_DyListViewItem(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

