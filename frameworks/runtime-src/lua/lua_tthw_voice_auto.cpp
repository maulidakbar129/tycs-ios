#include "lua_tthw_voice_auto.hpp"
#include "VoiceMgr.hpp"
#include "tolua_fix.h"
#include "LuaBasicConversions.h"



int lua_tthw_voice_VoiceMgr_playRecordedFile(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_playRecordedFile'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "VoiceMgr:playRecordedFile");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_playRecordedFile'", nullptr);
            return 0;
        }
        int ret = cobj->playRecordedFile(arg0);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:playRecordedFile",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_playRecordedFile'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_pause(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_pause'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_pause'", nullptr);
            return 0;
        }
        cobj->pause();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:pause",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_pause'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_setMode(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_setMode'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        gcloud_voice::GCloudVoiceMode arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "VoiceMgr:setMode");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_setMode'", nullptr);
            return 0;
        }
        cobj->setMode(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:setMode",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_setMode'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_resume(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_resume'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_resume'", nullptr);
            return 0;
        }
        cobj->resume();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:resume",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_resume'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_stopPlayFile(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_stopPlayFile'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_stopPlayFile'", nullptr);
            return 0;
        }
        int ret = cobj->stopPlayFile();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:stopPlayFile",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_stopPlayFile'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_stopRecording(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_stopRecording'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_stopRecording'", nullptr);
            return 0;
        }
        int ret = cobj->stopRecording();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:stopRecording",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_stopRecording'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_create(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_create'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_create'", nullptr);
            return 0;
        }
        cobj->create();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:create",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_create'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_setMaxMessageLength(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_setMaxMessageLength'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        int arg0;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "VoiceMgr:setMaxMessageLength");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_setMaxMessageLength'", nullptr);
            return 0;
        }
        int ret = cobj->setMaxMessageLength(arg0);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:setMaxMessageLength",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_setMaxMessageLength'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_setAppInfo(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_setAppInfo'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 3) 
    {
        std::string arg0;
        std::string arg1;
        std::string arg2;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "VoiceMgr:setAppInfo");

        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "VoiceMgr:setAppInfo");

        ok &= luaval_to_std_string(tolua_S, 4,&arg2, "VoiceMgr:setAppInfo");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_setAppInfo'", nullptr);
            return 0;
        }
        int ret = cobj->setAppInfo(arg0, arg1, arg2);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:setAppInfo",argc, 3);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_setAppInfo'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_applyMessageKey(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_applyMessageKey'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_applyMessageKey'", nullptr);
            return 0;
        }
        int ret = cobj->applyMessageKey();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:applyMessageKey",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_applyMessageKey'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_startRecording(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_startRecording'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "VoiceMgr:startRecording");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_startRecording'", nullptr);
            return 0;
        }
        int ret = cobj->startRecording(arg0);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:startRecording",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_startRecording'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_downloadRecordedFile(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_downloadRecordedFile'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        std::string arg0;
        std::string arg1;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "VoiceMgr:downloadRecordedFile");

        ok &= luaval_to_std_string(tolua_S, 3,&arg1, "VoiceMgr:downloadRecordedFile");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_downloadRecordedFile'", nullptr);
            return 0;
        }
        int ret = cobj->downloadRecordedFile(arg0, arg1);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:downloadRecordedFile",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_downloadRecordedFile'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_getMiclevel(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_getMiclevel'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_getMiclevel'", nullptr);
            return 0;
        }
        int ret = cobj->getMiclevel();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:getMiclevel",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_getMiclevel'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_RegisterMessageNotify(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_RegisterMessageNotify'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        std::string arg0;
        int arg1;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "VoiceMgr:RegisterMessageNotify");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "VoiceMgr:RegisterMessageNotify");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_RegisterMessageNotify'", nullptr);
            return 0;
        }
        cobj->RegisterMessageNotify(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:RegisterMessageNotify",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_RegisterMessageNotify'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_speechToText(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_speechToText'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        std::string arg0;
        gcloud_voice::GCloudLanguage arg1;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "VoiceMgr:speechToText");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "VoiceMgr:speechToText");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_speechToText'", nullptr);
            return 0;
        }
        int ret = cobj->speechToText(arg0, arg1);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:speechToText",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_speechToText'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_initVoiceMgr(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_initVoiceMgr'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_initVoiceMgr'", nullptr);
            return 0;
        }
        int ret = cobj->initVoiceMgr();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:initVoiceMgr",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_initVoiceMgr'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_poll(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_poll'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_poll'", nullptr);
            return 0;
        }
        cobj->poll();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:poll",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_poll'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_setNotify(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_setNotify'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_setNotify'", nullptr);
            return 0;
        }
        cobj->setNotify();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:setNotify",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_setNotify'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_uploadRecordedFile(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"VoiceMgr",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (VoiceMgr*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_tthw_voice_VoiceMgr_uploadRecordedFile'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        std::string arg0;
        int arg1;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "VoiceMgr:uploadRecordedFile");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "VoiceMgr:uploadRecordedFile");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_uploadRecordedFile'", nullptr);
            return 0;
        }
        int ret = cobj->uploadRecordedFile(arg0, arg1);
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:uploadRecordedFile",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_uploadRecordedFile'.",&tolua_err);
#endif

    return 0;
}
int lua_tthw_voice_VoiceMgr_constructor(lua_State* tolua_S)
{
    int argc = 0;
    VoiceMgr* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_tthw_voice_VoiceMgr_constructor'", nullptr);
            return 0;
        }
        cobj = new VoiceMgr();
        tolua_pushusertype(tolua_S,(void*)cobj,"VoiceMgr");
        tolua_register_gc(tolua_S,lua_gettop(tolua_S));
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "VoiceMgr:VoiceMgr",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_tthw_voice_VoiceMgr_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_tthw_voice_VoiceMgr_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (VoiceMgr)");
    return 0;
}

int lua_register_tthw_voice_VoiceMgr(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"VoiceMgr");
    tolua_cclass(tolua_S,"VoiceMgr","VoiceMgr","",nullptr);

    tolua_beginmodule(tolua_S,"VoiceMgr");
        tolua_function(tolua_S,"new",lua_tthw_voice_VoiceMgr_constructor);
        tolua_function(tolua_S,"playRecordedFile",lua_tthw_voice_VoiceMgr_playRecordedFile);
        tolua_function(tolua_S,"pause",lua_tthw_voice_VoiceMgr_pause);
        tolua_function(tolua_S,"setMode",lua_tthw_voice_VoiceMgr_setMode);
        tolua_function(tolua_S,"resume",lua_tthw_voice_VoiceMgr_resume);
        tolua_function(tolua_S,"stopPlayFile",lua_tthw_voice_VoiceMgr_stopPlayFile);
        tolua_function(tolua_S,"stopRecording",lua_tthw_voice_VoiceMgr_stopRecording);
        tolua_function(tolua_S,"create",lua_tthw_voice_VoiceMgr_create);
        tolua_function(tolua_S,"setMaxMessageLength",lua_tthw_voice_VoiceMgr_setMaxMessageLength);
        tolua_function(tolua_S,"setAppInfo",lua_tthw_voice_VoiceMgr_setAppInfo);
        tolua_function(tolua_S,"applyMessageKey",lua_tthw_voice_VoiceMgr_applyMessageKey);
        tolua_function(tolua_S,"startRecording",lua_tthw_voice_VoiceMgr_startRecording);
        tolua_function(tolua_S,"downloadRecordedFile",lua_tthw_voice_VoiceMgr_downloadRecordedFile);
        tolua_function(tolua_S,"getMiclevel",lua_tthw_voice_VoiceMgr_getMiclevel);
        tolua_function(tolua_S,"RegisterMessageNotify",lua_tthw_voice_VoiceMgr_RegisterMessageNotify);
        tolua_function(tolua_S,"speechToText",lua_tthw_voice_VoiceMgr_speechToText);
        tolua_function(tolua_S,"initVoiceMgr",lua_tthw_voice_VoiceMgr_initVoiceMgr);
        tolua_function(tolua_S,"poll",lua_tthw_voice_VoiceMgr_poll);
        tolua_function(tolua_S,"setNotify",lua_tthw_voice_VoiceMgr_setNotify);
        tolua_function(tolua_S,"uploadRecordedFile",lua_tthw_voice_VoiceMgr_uploadRecordedFile);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(VoiceMgr).name();
    g_luaType[typeName] = "VoiceMgr";
    g_typeCast["VoiceMgr"] = "VoiceMgr";
    return 1;
}
TOLUA_API int register_all_tthw_voice(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,"higame",0);
	tolua_beginmodule(tolua_S,"higame");

	lua_register_tthw_voice_VoiceMgr(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

