/****************************************************************************
Copyright (c) 2013-2017 Chukong Technologies Inc.

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/

#include "UIDyListView.h"
#include "ui/UIHelper.h"

NS_CC_BEGIN

namespace ui {


DyListViewItem::DyListViewItem() :
	_isLoaded(false),
	_listView(nullptr)
{

}

DyListViewItem::~DyListViewItem()
{
	_listView = nullptr;
}

DyListViewItem* DyListViewItem::create(DyListView* listView)
{
	DyListViewItem* item = new (std::nothrow) DyListViewItem();
	if (item && item->init())
	{
		item->autorelease();
		item->_listView = listView;
		return item;
	}
	CC_SAFE_DELETE(item);
	return nullptr;
}

bool DyListViewItem::init()
{
	if (Layout::init())
	{
		return true;
	}
	return false;
}

bool DyListViewItem::isLoaded()
{
	return _isLoaded;
}

void DyListViewItem::setLoaded(bool loaded)
{
	_isLoaded = loaded;
}

void DyListViewItem::setContentSize(const Size & contentSize)
{
	auto size = getContentSize();
	if (size.width == contentSize.width && size.height == contentSize.height)
	{
		return;
	}
	if (_listView != nullptr) {
		_listView->setItemSizeDirty(true);
	}
	Layout::setContentSize(contentSize);
}


DyListView::DyListView():
	_itemsCount(0),
	_delegate(nullptr),
	_lastContainerPos(429496729),
	_cacheViewSize(0),
	_isItemSizeDirty(false),
	_isCacheItems(false),
	_defaultItemSize(Size(10, 10)),
	_isInitAllItems(false),
	_reloadDirection(ReloadDirection::FRONT)
{
}

DyListView::~DyListView()
{
    _listViewEventListener = nullptr;
    _listViewEventSelector = nullptr;
    _items.clear();
    CC_SAFE_RELEASE(_model);
}

DyListView* DyListView::create()
{
	DyListView* widget = new (std::nothrow) DyListView();
    if (widget && widget->init())
    {
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

bool DyListView::init()
{
    if (ListView::init())
    {
		setDirection(Direction::VERTICAL);
		setBounceEnabled(true);
		setScrollBarEnabled(false);
        return true;
    }
    return false;
}

void DyListView::jumpToBottom()
{
	switch (_direction)
	{
	case Direction::VERTICAL:
	{

		jumpToItem(_itemsCount-1, Vec2::ANCHOR_MIDDLE_BOTTOM, Vec2::ANCHOR_MIDDLE_BOTTOM);
		break;
	}
	case Direction::HORIZONTAL:
	{
		jumpToRight();
		break;
	}
	default:
		break;
	}
}

void DyListView::jumpToTop()
{
	switch (_direction)
	{
	case Direction::VERTICAL:
	{

		jumpToItem(0, Vec2::ANCHOR_MIDDLE_TOP, Vec2::ANCHOR_MIDDLE_TOP);
		break;
	}
	case Direction::HORIZONTAL:
	{
		jumpToLeft();
		break;
	}
	default:
		break;
	}
}

void DyListView::jumpToLeft()
{
	switch (_direction)
	{
	case Direction::VERTICAL:
	{
		jumpToTop();
		break;
	}
	case Direction::HORIZONTAL:
	{
		jumpToItem(0, Vec2::ANCHOR_MIDDLE_LEFT, Vec2::ANCHOR_MIDDLE_LEFT);
		break;
	}
	default:
		break;
	}
}

void DyListView::jumpToRight()
{

	switch (_direction)
	{
	case Direction::VERTICAL:
	{
		jumpToBottom();
		break;
	}
	case Direction::HORIZONTAL:
	{
		jumpToItem(_itemsCount-1, Vec2::ANCHOR_MIDDLE_RIGHT, Vec2::ANCHOR_MIDDLE_RIGHT);
		break;
	}
	default:
		break;
	}
}

void DyListView::jumpToItem(ssize_t itemIndex, const Vec2 & positionRatioInView, const Vec2 & itemAnchorPoint)
{
	Vec2 newPositionRatioInView(positionRatioInView);
	if (_delegate && itemIndex >=0 && itemIndex < _itemsCount)
	{
		auto contentSize = getContentSize();
		float up_left_surplus = 0;
		float down_right_surplus = 0;
		auto item = getItem(itemIndex);
		if (item)
		{
			auto listviewItem = (DyListViewItem*)item;
			if (!listviewItem->isLoaded())
			{
				//CCLOG("jumpToItem4");
				_delegate(this, DelegateTag::LoadItem, itemIndex, item);
				listviewItem->setLoaded(true);
				if (_cacheViewSize == 0)
				{
					if (_direction == Direction::VERTICAL)
					{
						setCacheViewSize(item->getContentSize().height);
					}
					else
					{
						setCacheViewSize(item->getContentSize().width);
					}
				}
			}
			
			auto itemSize = item->getContentSize();
			if (_direction == Direction::VERTICAL)
			{
				up_left_surplus = contentSize.height * (1 - positionRatioInView.y) - itemSize.height * (1 - itemAnchorPoint.y);
				down_right_surplus = contentSize.height * positionRatioInView.y - itemSize.height * itemAnchorPoint.y;
				//CCLOG("jumpToItem4.1 contentSize %f, positionRatioInView %f, itemSize %f itemAnchorPoint %f upleft %f downleft %f", contentSize.height, positionRatioInView.y, itemSize.height, itemAnchorPoint.y, up_left_surplus, down_right_surplus);
			}
			else
			{
				down_right_surplus = contentSize.width * (1 - positionRatioInView.x) - itemSize.width * (1 - itemAnchorPoint.x);
				up_left_surplus = contentSize.width * positionRatioInView.x - itemSize.width * itemAnchorPoint.x;
				//CCLOG("jumpToItem4.1 contentSize %f, positionRatioInView %f, itemSize %f itemAnchorPoint %f upleft %f downleft %f", contentSize.width, positionRatioInView.x, itemSize.width, itemAnchorPoint.x, up_left_surplus, down_right_surplus);
			}
		}

		down_right_surplus -= getItemsMargin();
		up_left_surplus -= getItemsMargin();

		for (size_t i = itemIndex + 1; i < 100000; i++)
		{
			if (down_right_surplus < 0)
			{
				break;
			}
			auto item = getItem(i);
			if (item)
			{			
				auto listviewItem = (DyListViewItem*)item;
				if (!listviewItem->isLoaded())
				{
					_delegate(this, DelegateTag::LoadItem, i, item);
					listviewItem->setLoaded(true);
				}
				auto itemSize = item->getContentSize();
				if (_direction == Direction::VERTICAL)
				{
					down_right_surplus = down_right_surplus - itemSize.height - getItemsMargin();
				}
				else
				{
					down_right_surplus = down_right_surplus - itemSize.width - getItemsMargin();
				}
				//CCLOG("jumpToItem6 down_right_surplus %f index %d", down_right_surplus, i);
			}
			else
			{
				//if (_direction == Direction::VERTICAL)
				//{
				//	newPositionRatioInView.y -= down_right_surplus / contentSize.height;
				//}
				//else
				//{
				//	newPositionRatioInView.x -= down_right_surplus / contentSize.width;
				//}

				break;
			}
		}
		for (size_t i = itemIndex - 1; i >= 0; i--)
		{
			if (up_left_surplus < 0)
			{
				break;
			}
			auto item = getItem(i);
			if (item)
			{
				auto listviewItem = (DyListViewItem*)item;
				if (!listviewItem->isLoaded())
				{
					_delegate(this, DelegateTag::LoadItem, i, item);
					listviewItem->setLoaded(true);
				}
				auto itemSize = item->getContentSize();
				if (_direction == Direction::VERTICAL)
				{
					up_left_surplus = up_left_surplus - itemSize.height - getItemsMargin();
				}
				else
				{
					up_left_surplus = up_left_surplus - itemSize.width - getItemsMargin();
				}
				//CCLOG("jumpToItem7 down_right_surplus %f index %d", up_left_surplus, i);
			}
			else
			{
				if (_direction == Direction::VERTICAL)
				{
					newPositionRatioInView.y += (up_left_surplus + getItemsMargin()) / contentSize.height;
				}
				else
				{
					newPositionRatioInView.x -= (up_left_surplus + getItemsMargin()) / contentSize.width;
				}
				break;
			}
		}
	}
	//forceDoLayout();
	ListView::jumpToItem(itemIndex, newPositionRatioInView, itemAnchorPoint);
}

void DyListView::setItemsCount(unsigned int count)
{
	_itemsCount = count;
}

void DyListView::setDefaultItemSize(const Size&  size)
{
	_defaultItemSize = size;
	if (_direction == Direction::VERTICAL)
	{
		setCacheViewSize(_defaultItemSize.height);
	}
	else
	{
		setCacheViewSize(_defaultItemSize.width);
	}

	
}

Vec2 DyListView::getItemPositionRatioInView(Widget * item)
{
	Vec2 ratio(0,0);
	if (item) {

		Vec2 itemPos = item->getPosition();
		Size itemSize = item->getContentSize();
		Vec2 itemAnchorPoint = item->getAnchorPoint();
		Vec2 datumPoint = getDatumPoint();
		Size viewSize = getContentSize();
		Vec2 innerContainerPos = getInnerContainerPosition();

		switch (_direction)
		{
		case Direction::VERTICAL:
		{

			float pos = itemPos.y + innerContainerPos.y;
			auto distance = pos - itemSize.height * itemAnchorPoint.y + itemSize.height * datumPoint.y;
			ratio.y = distance / viewSize.height;
			break;
		}
		case Direction::HORIZONTAL:
		{
			float pos = itemPos.x + innerContainerPos.x;
			auto distance = pos - itemSize.width * itemAnchorPoint.x + itemSize.width * datumPoint.x;
			ratio.x = distance / viewSize.width;
			//CCLOG("getItemPositionRatioInView itemPos %f innerContainerPos %f itemSize %f itemAnchorPoint %f distance %f", itemPos.x, innerContainerPos.x,);
			break;
		}
		default:
			break;
		}

	}
	return ratio;
}

void DyListView::setViewDelegate(const DyListViewDelegate & delegate)
{
	_delegate = delegate;
}

bool DyListView::isItemInView(Widget* item)
{
	if (item) {

		Vec2 itemPos = item->getPosition();
		Size itemSize = item->getContentSize();
		Vec2 itemAnchorPoint = item->getAnchorPoint();
		Size viewSize = getContentSize();
		Vec2 innerContainerPos = getInnerContainerPosition();
		
		switch (_direction)
		{
		case Direction::VERTICAL:
		{

			float pos = itemPos.y + innerContainerPos.y;
			float up = pos + itemSize.height * (1 - itemAnchorPoint.y);
			float down = pos - itemSize.height * itemAnchorPoint.y;
			if (down <= viewSize.height + _cacheViewSize && up >= 0 - _cacheViewSize)
			{
				return true;
			}
			break;
		}
		case Direction::HORIZONTAL:
		{
			float pos = itemPos.x + innerContainerPos.x;
			float right = pos + itemSize.width * (1 - itemAnchorPoint.x);
			float left = pos - itemSize.width * itemAnchorPoint.x;
			//CCLOG("isItemInView index %d viewSize.width %f  itemPos.x %f innerContainerPos.x %f itemSize.width %f right %f left %f itemAnchorPoint.x %f", getIndex(item), viewSize.width, itemPos.x, innerContainerPos.x, itemSize.width, right, left, itemAnchorPoint.x);
			if (left <= viewSize.width + _cacheViewSize && right >= 0 - _cacheViewSize)
			{
				return true;
			}
			break;
		}
		default:
			break;
		}

	}
	return false;
}

void DyListView::setCacheItemsEnable(bool enable)
{
	_isCacheItems = enable;
}

bool DyListView::isCacheItemsEnable()
{
	return _isCacheItems;
}

bool DyListView::isItemInView(unsigned int itemIndex)
{
	auto item = getItem(itemIndex);
	return isItemInView(item);

}

void DyListView::setCacheViewSize(float size)
{
	_cacheViewSize = size;
}

float DyListView::getInnerContainerPos()
{
	auto innerContainerPositionPos = getInnerContainerPosition();
	switch (_direction)
	{
	case Direction::VERTICAL:
	{

		return innerContainerPositionPos.y;
		break;
	}
	case Direction::HORIZONTAL:
	{
		return innerContainerPositionPos.x;
		break;
	}
	default:
		break;
	}

	return 0.0f;
}

void DyListView::setItemSizeDirty(bool dirty)
{
	_isItemSizeDirty = dirty;
}

float DyListView::getItemDistanceInCurrentView(Widget * item)
{
	float distance = 0;
	if (item) {

		Vec2 itemPos = item->getPosition();
		Size itemSize = item->getContentSize();
		Vec2 itemAnchorPoint = item->getAnchorPoint();
		Size viewSize = getContentSize();
		Vec2 innerContainerPos = getInnerContainerPosition();

		switch (_direction)
		{
		case Direction::VERTICAL:
		{

			float pos = itemPos.y + innerContainerPos.y;
			distance = pos - itemSize.height * itemAnchorPoint.y;
			break;
		}
		case Direction::HORIZONTAL:
		{
			float pos = itemPos.x + innerContainerPos.x;
			distance = pos + itemSize.width * itemAnchorPoint.x;
			break;
		}
		default:
			break;
		}

	}
	return distance;
}

void DyListView::reload(bool isUseOldPos)
{
	size_t jumpIndex = 0;
	Vec2 positionRatioInView(getDatumPoint());
	if (isUseOldPos)
	{
		auto datumItem = getDatumItem();
		if (datumItem)
		{
			jumpIndex = getIndex(datumItem);
			positionRatioInView = getItemPositionRatioInView(datumItem);
		}
	}
	
	removeAllChildren();
	_itemsCount = 0;
	_defaultItemSize = getContentSize();
	_lastContainerPos = 429496729;
	
	if (_delegate)
	{
		//CCLOG("DyListView::reload------------------------------>");
		_delegate(this, DelegateTag::ItemsCount, -1, nullptr);
		_delegate(this, DelegateTag::ItemSize, -1, nullptr);
		for (size_t i = 0; i < _itemsCount; i++)
		{
			auto item = DyListViewItem::create(this);
			item->setContentSize(_defaultItemSize);
			pushBackCustomItem(item);
		}
	}

	if (isUseOldPos)
	{
		//CCLOG("DyListView::reload() jumpIndex %d positionRatioInView %f %f ", jumpIndex, positionRatioInView.x, positionRatioInView.y);
		jumpToItem(jumpIndex, positionRatioInView, getDatumPoint());
	}
}

void DyListView::reloadToItem(ssize_t itemIndex)
{
	auto item = getItem(itemIndex);
	if (item)
	{
		Vec2 positionRatioInView = getItemPositionRatioInView(item);
		reload(false);
		jumpToItem(itemIndex, positionRatioInView, getDatumPoint());
	}
	else
	{
		jumpToTop(); 
	}
}

Vector<Widget*>& DyListView::getShowedItems()
{
	_showedItems.clear();
	for (auto& item : _items)
	{
		if (isItemInView(item))
		{
			_showedItems.pushBack(item);
		}
	}
	return _showedItems;
}

Widget * DyListView::getItemLua(ssize_t index) const
{
	return getItem(index);
}

ssize_t DyListView::getIndexLua(Widget * item) const
{
	return getIndex(item);
}

Widget * DyListView::getDatumItem()
{
	switch (_direction)
	{
	case Direction::HORIZONTAL:
	{
		switch (_reloadDirection)
		{
		case ReloadDirection::FRONT:
		{
			return getLeftmostItemInCurrentView();
			break;
		};
		case ReloadDirection::BACK:
		{
			return getRightmostItemInCurrentView();
			break;
		};
		default:
			break;
		}
		break;
	};
	case Direction::VERTICAL:
	{
		switch (_reloadDirection)
		{
		case ReloadDirection::FRONT:
		{
			return getTopmostItemInCurrentView();
			break;
		};
		case ReloadDirection::BACK:
		{
			return getBottommostItemInCurrentView();
			break;
		};
		default:
			break;
		}
		break;
	};
	}
	return nullptr;
}

const Vec2 DyListView::getDatumPoint()
{
	switch (_direction)
	{
	case Direction::HORIZONTAL:
	{
		switch (_reloadDirection)
		{
		case ReloadDirection::FRONT:
		{
			return Vec2::ANCHOR_MIDDLE_LEFT;
			break;
		};
		case ReloadDirection::BACK:
		{
			return Vec2::ANCHOR_MIDDLE_RIGHT;
			break;
		};
		default:
			break;
		}
		break;
	};
	case Direction::VERTICAL:
	{
		switch (_reloadDirection)
		{
		case ReloadDirection::FRONT:
		{
			return Vec2::ANCHOR_MIDDLE_TOP;
			break;
		};
		case ReloadDirection::BACK:
		{
			return Vec2::ANCHOR_MIDDLE_BOTTOM;
			break;
		};
		default:
			break;
		}
		break;
	};
	}
	return nullptr;
}

void DyListView::update(float dt)
{
	float pos = getInnerContainerPos();
	float itemsCount = _items.size();
	bool isAdjustDistance = false;
	if (abs(pos - _lastContainerPos) > 3 && _delegate && itemsCount > 0)
	{
		if (pos - _lastContainerPos > 0)
		{
			isAdjustDistance = true;
		}
		//CCLOG("update1 pos %f  _lastContainerPos %f", pos, _lastContainerPos);
		for (auto& item : _items)
		{
			auto listviewItem = (DyListViewItem*)item;
			if (_isCacheItems)
			{
				if (isItemInView(item))
				{
					if (!listviewItem->isLoaded())
					{
						_delegate(this, DelegateTag::LoadItem, getIndex(item), listviewItem);
						listviewItem->setLoaded(true);
					}
				}
			}
			else
			{
				if (isItemInView(item))
				{
					if (!listviewItem->isLoaded())
					{
						listviewItem->removeAllChildren();
						
						//CCLOG("update2 ADD_ITEM  getIndex %f", getIndex(item));
						_delegate(this, DelegateTag::LoadItem, getIndex(item), listviewItem);
						listviewItem->setLoaded(true);
					}
				}
				else
				{
					if (listviewItem->isLoaded())
					{
						listviewItem->removeAllChildren();
						_delegate(this, DelegateTag::UnloadItem, getIndex(item), listviewItem);
						listviewItem->setLoaded(false);
					}
				}
			}
		}
		_lastContainerPos = pos;
	}

	updateDirtyItemSize(isAdjustDistance);
	ListView::update(dt);

}

void DyListView::updateDirtyItemSize(bool isAdjustDistance)
{
	float itemsCount = _items.size();
	if (_isItemSizeDirty && itemsCount > 0)
	{
		auto centerItem = getCenterItemInCurrentView();
		float distance = getItemDistanceInCurrentView(centerItem);
		auto oldInnerContainerPos = getInnerContainerPosition();
		auto oldInnerContainerSize = getInnerContainerSize();
		forceDoLayout();
		auto newInnerContainerPos = getInnerContainerPosition();
		auto newInnerContainerSize = getInnerContainerSize();
		auto itemPos = centerItem->getPosition();

		switch (_direction)
		{
		case Direction::HORIZONTAL:
		{
			newInnerContainerPos.x = distance - itemPos.x;

			if (isAdjustDistance)
			{
				_autoScrollStartPosition.x += (oldInnerContainerSize.width - newInnerContainerSize.width);
			}
			break;
		};
		case Direction::VERTICAL:
		{
			newInnerContainerPos.y = distance - itemPos.y;
			if (isAdjustDistance)
			{
				_autoScrollStartPosition.y += (oldInnerContainerSize.height - newInnerContainerSize.height);
			}
			break;
		};
		}
		setInnerContainerPosition(newInnerContainerPos);
		_isItemSizeDirty = false;
	}
}


}
NS_CC_END
