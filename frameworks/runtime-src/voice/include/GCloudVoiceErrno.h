/*******************************************************************************\
 ** gcloud_voice:GCloudVoice.h
 ** Created by CZ  on 16/8/1
 **
 **  Copyright 2016 apollo. All rights reserved.
 \*******************************************************************************/

#ifndef GcloudVoiceErrno_h
#define GcloudVoiceErrno_h

namespace gcloud_voice
{
    /**
     * Destination language to translate to.
     */
    enum GCloudLanguage {
        Chinese     = 0,
        Korean      = 1,
        English     = 2,
        Japanese    = 3,
    };
    
    /**
     * Mode of the voice engine. You should set to one first.
     */
    enum GCloudVoiceMode
    {
        Unknown = -1,
        RealTime = 0,    // realtime mode for TeamRoom, NationalRoom, RangeRoom
        Messages,        // voice message mode
        Translation,     // speach to text mode
        RSTT,            // real-time speach to text mode
        HIGHQUALITY,     // high quality realtime voice mode[deprecated], will cost more network traffic
    };
    
    /**
     * Member's role in National Room.
     */
    enum GCloudVoiceMemberRole
    {
        Anchor = 1,     // member who can open/close microphone and speaker
        Audience,       // member who can only open/close speaker
    };
    
    /**
     * The effect mode of voice.
     * You can set to one of this by SetVoiceEffects method when you want to change the player's voice.
     */
    enum SoundEffects
    {
        GVSE_REVB_CHURCH = 0,      // reveb church effect
        GVSE_REVB_THEATER = 1,     // reveb theater effect
        GVSE_HELL = 2,             // voice from hell effect
        GVSE_ROBOT_1 = 3,          // robot voice effect
        GVSE_MALE_TO_FEMALE = 4,   // voice from male to female effect
        GVSE_FEMALE_TO_MALE = 5,   // voice from female to male effect
        GVSE_DRUNK = 6,            // drunk voice effect
        GVSE_PAPI_JIANG = 7,       // Chinese papi-Jiang voice effect
        GVSE_SQUIRREL = 8,         // squrrel voice effect
        GVSE_NO_EFFECT = 9,        // no voice effect
    };
    
	 /**
     * Speech Translation languages
     */
	enum SpeechLanguageType
	{
		SPEECH_LANGUAGE_ZH = 0,		// Chinese
		SPEECH_LANGUAGE_EN = 1,		// English
		SPEECH_LANGUAGE_JA = 2,		// Japanese
		SPEECH_LANGUAGE_KO = 3,		// Korean
		SPEECH_LANGUAGE_DE = 4,		// German
		SPEECH_LANGUAGE_FR = 5,		// French
		SPEECH_LANGUAGE_ES = 6,		// Spanish
		SPEECH_LANGUAGE_IT = 7,		// Italian
		SPEECH_LANGUAGE_TR = 8,		// Turkish
		SPEECH_LANGUAGE_RU = 9,		// Russian
		SPEECH_LANGUAGE_PT = 10,	// Portuguese
		SPEECH_LANGUAGE_VI = 11,	// Vietnamese
		SPEECH_LANGUAGE_ID = 12,	// Indonesian
		SPEECH_LANGUAGE_MS = 13,	// Malaysian
		SPEECH_LANGUAGE_TH = 14,	// Thai
	};

	/**
     * Speech Translation type, pip nodes: Source Speech -> Source Text -> Target Text -> Target Speech
     */
	enum SpeechTranslateType
	{
		SPEECH_TRANSLATE_STST = 0,	//Source Speech -> Source Text
		SPEECH_TRANSLATE_STTT = 1,  //Source Speech -> Source Text -> Target Text
		SPEECH_TRANSLATE_STTS = 2,	//Source Speech -> Source Text -> Target Text -> Target Speech
	};

    enum GCloudVoiceErrno
    {
        GCLOUD_VOICE_SUCC               = 0,
		
		//common base err
		GCLOUD_VOICE_PARAM_NULL         = 0x1001,	//4097, some parameter is null
		GCLOUD_VOICE_NEED_SETAPPINFO    = 0x1002,	//4098, you should call SetAppInfo first before call other api
		GCLOUD_VOICE_INIT_ERR           = 0x1003,	//4099, Init error
		GCLOUD_VOICE_RECORDING_ERR      = 0x1004,	//4100, now is recording, can't do other operator
		GCLOUD_VOICE_POLL_BUFF_ERR      = 0x1005,	//4101, poll buffer is not enough or null 
		GCLOUD_VOICE_MODE_STATE_ERR     = 0x1006,	//4102, call some api in the wrong voice engine mode, maybe you shoud call SetMode to correct it
		GCLOUD_VOICE_PARAM_INVALID      = 0x1007,	//4103, some parameter is null or its value is invalid, please use right parameters
		GCLOUD_VOICE_OPENFILE_ERR       = 0x1008,   //4104, open a file error
		GCLOUD_VOICE_NEED_INIT          = 0x1009,   //4105, you should call Init before do this operator
		GCLOUD_VOICE_ENGINE_ERR         = 0x100A,   //4106, you have not get engine instance, this common in use c# api, but not get gcloudvoice instance first
		GCLOUD_VOICE_POLL_MSG_PARSE_ERR = 0x100B,   //4107, this common in c# api, parse poll msg err
		GCLOUD_VOICE_POLL_MSG_NO        = 0x100C,   //4108, poll, no msg to update

		//realtime err
		GCLOUD_VOICE_REALTIME_STATE_ERR = 0x2001,   //8193, call some realtime api in wrong realtime voice state, such as OpenMic but you have not joined room
		GCLOUD_VOICE_JOIN_ERR           = 0x2002,   //8194, join room failed
		GCLOUD_VOICE_QUIT_ROOMNAME_ERR  = 0x2003,   //8195, the roomname is not the same as the one when join room
		GCLOUD_VOICE_OPENMIC_NOTANCHOR_ERR = 0x2004,//8196, open mic in bigroom, but not anchor role
        GCLOUD_VOICE_CREATE_ROOM_ERR    = 0x2005,   //8197, create room error
        GCLOUD_VOICE_NO_ROOM = 0x2006,              //8198, no such room
        GCLOUD_VOICE_QUIT_ROOM_ERR      = 0x2007,   //8199, quit room error
        GCLOUD_VOICE_ALREADY_IN_THE_ROOM = 0x2008,  //8200, already in the room which in JoinXxxxRoom
        
		//message err
		GCLOUD_VOICE_AUTHKEY_ERR        = 0x3001,   //12289, apply authkey api error
		GCLOUD_VOICE_PATH_ACCESS_ERR    = 0x3002,   //12290, the path can not access, maybe the file is not exist or deny to access
		GCLOUD_VOICE_PERMISSION_MIC_ERR = 0x3003,	//12291, you don't have the right to access microphone in android
		GCLOUD_VOICE_NEED_AUTHKEY       = 0x3004,	//12292, you have not gotten authkey, call ApplyMessageKey first
		GCLOUD_VOICE_UPLOAD_ERR         = 0x3005,	//12293, upload file error
		GCLOUD_VOICE_HTTP_BUSY          = 0x3006,	//12294, http is busy, maybe the last upload/download has not finished
		GCLOUD_VOICE_DOWNLOAD_ERR       = 0x3007,	//12295, download file error
		GCLOUD_VOICE_SPEAKER_ERR        = 0x3008,   //12296, tve error when open or close speaker
		GCLOUD_VOICE_TVE_PLAYSOUND_ERR  = 0x3009,   //12297, tve error when play voice file
        GCLOUD_VOICE_AUTHING            = 0x300a,   //12298, already in applying auth key process
        GCLOUD_VOICE_LIMIT              = 0x300b,   //12299, upload limit, you can not upload permanent file
		GCLOUD_VOICE_NOTHING_TO_REPORT  = 0x300c,   //12300, no sound to report

//        GCLOUD_VOICE_REPORT_UP_SUCC     = 0x4001,   //16385, interface function never receive this error code, used in Engine

        //internal err
		GCLOUD_VOICE_INTERNAL_TVE_ERR   = 0x5001,   //20481, internal TVE error, GVoice internal used
		GCLOUD_VOICE_INTERNAL_VISIT_ERR = 0x5002,	//20482, internal non-TVE err, GVoice internal used
		GCLOUD_VOICE_INTERNAL_USED      = 0x5003,   //20483, internal used, you should not get this error
        
        GCLOUD_VOICE_BADSERVER          = 0x06001,  //24577, bad server address, the server address should like: "udp://capi.xxx.xxx.com"
        
        GCLOUD_VOICE_STTING             = 0x07001,  //28673, already in speaching to text process
        
        
        //other functions in realtime err
        GCLOUD_VOICE_CHANGE_ROLE        = 0x08001,  //32769, change role error
        GCLOUD_VOICE_CHANGING_ROLE      = 0x08002,  //32770, already in changing role
        GCLOUD_VOICE_NOT_IN_ROOM        = 0x08003,  //32771, no in room
        GCLOUD_VOICE_COORDINATE         = 0x09001,  //36865, sync coordinate error
        GCLOUD_VOICE_SMALL_ROOMNAME     = 0x09002,  //36866, query with a small room name
        GCLOUD_VOICE_COORDINATE_ROOMNAME_ERROR = 0x09003, //36867, update coordinate in a non-exist room
        
        GCLOUD_VOICE_SAVEDATA_DOWNLOADING = 0x0A001, //dowloading file for lgame save voice data, need no nothing, just let userinterface know.
        GCLOUD_VOICE_SAVEDATA_INDEXNOTFOUND = 0x0A002,  //this file index not found in file map ,may not set ,have not in this video
    };
    
    enum GCloudVoiceCompleteCode
    {
        GV_ON_JOINROOM_SUCC = 1,	               //join room success
		GV_ON_JOINROOM_TIMEOUT,                    //join room timeout
		GV_ON_JOINROOM_SVR_ERR,                    //communication with svr meets some error, such as wrong data received from svr
        GV_ON_JOINROOM_UNKNOWN,                    //reserved, GVoice internal unknown error
 
		GV_ON_NET_ERR,                             //network error, maybe can't connect to network

		GV_ON_QUITROOM_SUCC,                       //quitroom success, if you have joined room success first, quit room will alway return success

		GV_ON_MESSAGE_KEY_APPLIED_SUCC,            //apply message authkey succ
		GV_ON_MESSAGE_KEY_APPLIED_TIMEOUT,		   //apply message authkey timeout
		GV_ON_MESSAGE_KEY_APPLIED_SVR_ERR,         //communication with svr meets some error, such as wrong data received
		GV_ON_MESSAGE_KEY_APPLIED_UNKNOWN,         //reserved, GVoice internal unknown error

        GV_ON_UPLOAD_RECORD_DONE,                  //upload record file success
        GV_ON_UPLOAD_RECORD_ERROR,                 //upload record file meets some error
        GV_ON_DOWNLOAD_RECORD_DONE,	               //download record file success
        GV_ON_DOWNLOAD_RECORD_ERROR,	           //download record file meets some error

        GV_ON_STT_SUCC,                            //speech to text success
        GV_ON_STT_TIMEOUT,                         //speech to text timeout
        GV_ON_STT_APIERR,                          //server's error
        
        GV_ON_RSTT_SUCC,                           //stream speech to text success
        GV_ON_RSTT_TIMEOUT,                        //stream speech to text timeout
        GV_ON_RSTT_APIERR,                         //server's error in stream speech to text
        
		GV_ON_PLAYFILE_DONE,                       //the record file have played to the end
        
        GV_ON_ROOM_OFFLINE,                        //dropped from the room
        GV_ON_UNKNOWN,
        GV_ON_ROLE_SUCC,                           //change role success
        GV_ON_ROLE_TIMEOUT,                        //change role timeout
        GV_ON_ROLE_MAX_AHCHOR,                     //too many anchors, no more than 5 anchors in the same time are allowed in a national room
        GV_ON_ROLE_NO_CHANGE,                      //the same role as before
        GV_ON_ROLE_SVR_ERROR,                      //server's error in change role
        
        GV_ON_RSTT_RETRY,                          //need retry stt
		GV_ON_JOINROOM_RETRY_FAIL,                 //join room try again fail
        GV_ON_REPORT_SUCC, // report other player succ
        GV_ON_DATA_ERROR,  // receive illegal or invalid data from serve
        GV_ON_PUNISHED, // the player is punished because of being reported
        GV_ON_NOT_PUNISHED, // the player
        GV_ON_SAVEDATA_SUCC,  //LGAME Save RecData
		GV_ON_ROOM_MEMBER_INROOM,   //member join or in room
		GV_ON_ROOM_MEMBER_OUTROOM, //member out of room
 		GV_ON_UPLOAD_REPORT_INFO_ERROR, //civilized voice reporting error
		GV_ON_UPLOAD_REPORT_INFO_TIMEOUT, //civilized voice reporting timeout
		GV_ON_ST_SUCC,								//speech translate success
		GV_ON_ST_HTTP_ERROR,						//http failed
		GV_ON_ST_SERVER_ERROR,						//server error
		GV_ON_ST_INVALID_JSON,						//parse rsp json faild.
    };
    
    /**
     * Event of GCloudVoice
     *
     */
    enum GCloudVoiceEvent
    {
        EVENT_NO_DEVICE_CONNECTED            = 0,  //no any device is connected
        EVENT_HEADSET_DISCONNECTED           = 10, //a headset device is connected
        EVENT_HEADSET_CONNECTED              = 11, //a headset device is disconnected
        EVENT_BLUETOOTH_HEADSET_DISCONNECTED = 20, //a bluetooth device is connected
        EVENT_BLUETOOTH_HEADSET_CONNECTED    = 21, //a bluetooth device is disconnected
        EVENT_MIC_STATE_OPEN_SUCC            = 30, //open microphone success
        EVENT_MIC_STATE_OPEN_ERR             = 31, //open microphone meets error
        EVENT_MIC_STATE_NO_OPEN              = 32, //microphone not open
		EVENT_MIC_STATE_OCCUPANCY            = 33, //indicates the microphone has been occupancyed by others
        EVENT_SPEAKER_STATE_OPEN_SUCC        = 40, //open speaker success
        EVENT_SPEAKER_STATE_OPEN_ERR         = 41, //open speaker meets error
        EVENT_SPEAKER_STATE_NO_OPEN          = 42, //speaker not open
		EVENT_AUDIO_INTERRUPT_BEGIN          = 50, //audio device begin to be interrupted
		EVENT_AUDIO_INTERRUPT_END            = 51, //audio device end to be interrupted
        EVENT_AUDIO_RECORDER_EXCEPTION       = 52, //indicates the recorder thread throws a exception, maybe you can resume the audio
        EVENT_AUDIO_RENDER_EXCEPTION         = 53, //indicates the render thread throws a exception, maybe you can resume the audio
		EVENT_PHONE_CALL_PICK_UP             = 54, //indicates that you picked up the phone
		EVENT_PHONE_CALL_HANG_UP             = 55, //indicates that you hanged up the phone
	};
    
    /**
     * Audio device event of GCloudVoice
     */
    enum GCloudVoiceDeviceState
    {
        AUDIO_DEVICE_UNCONNECTED            = 0,  //no any audio device is connected
        AUDIO_DEVICE_WIREDHEADSET_CONNECTED = 1,  //a wiredheadset device is connected
        AUDIO_DEVICE_BULETOOTH_CONNECTED    = 2,  //a bluetooth device is disconnected
        
    };

} //endof namespace gcloud_voice

#endif /* GcloudVoiceErrno_h */
