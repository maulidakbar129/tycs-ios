/*
 * cocos2d for iPhone: http://www.cocos2d-iphone.org
 *
 * Copyright (c) 2011 Ricardo Quesada
 * Copyright (c) 2012 Zynga Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
const char* ccHigame3D_vert = R"(

attribute vec4 a_position;
attribute vec2 a_texCoord;
attribute vec4 a_color;
attribute vec3 a_normal;

uniform	vec4 u_color;
uniform	vec2 u_uvtrans;

#ifdef GL_ES
varying lowp vec4 v_fragmentColor;
varying mediump vec2 v_texCoord;
#else
varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
#endif

#ifdef HIGAME_NORMAL_SPECULAR
varying float v_fresnel;
varying vec3 v_normal;
#endif

void main()
{
  	gl_Position = CC_MVPMatrix * a_position;
	vec4 color = a_color * u_color;
	v_fragmentColor = color * vec4(color.w, color.w, color.w, 1);
	v_texCoord = a_texCoord + u_uvtrans;

#ifdef HIGAME_NORMAL_SPECULAR

	v_normal = CC_NormalMatrix * a_normal;

	v_fresnel = abs(dot(vec3(0.0,0.0,-1.0), normalize(v_normal)));
	v_fresnel = max(0.0, v_fresnel);
	v_fresnel = pow(v_fresnel, 10.0);
	
#endif

}
)";
