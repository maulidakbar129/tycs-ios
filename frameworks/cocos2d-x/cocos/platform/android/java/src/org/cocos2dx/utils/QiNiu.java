package org.cocos2dx.utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import org.cocos2dx.lib.Cocos2dxHelper;
import org.cocos2dx.lib.Cocos2dxWebViewHelper;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager.OnActivityResultListener;
import android.provider.MediaStore;
import android.provider.MediaStore.Files;
import android.util.Log;

import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCancellationSignal;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UpProgressHandler;
import com.qiniu.android.storage.UploadManager;
import com.qiniu.android.storage.UploadOptions;

public class QiNiu {
	
    private static final int TAKE_PHOTO = 99841;
    private static final int PICK_PHOTO = 99842;
    
  
	public static int UPLOAD_OP_TYPE_FILE = 0;          //閺傚洣娆㈢捄顖氱窞
	public static int UPLOAD_OP_TYPE_TAKE_PHOTO = 1;    //閹峰秶鍙�
	public static int UPLOAD_OP_TYPE_LOCAL_PHOTO = 2;   //閺堫剙婀撮惄绋垮斀
	public static int UPLOAD_OP_TYPE_LOCAL_CAPTURESCREEN = 3;   //閹搭亜娴�
	
	public static native void QiNiuUpdateCallback(int uploadid, int retCode, String msg);
	
	
	private static int _maxFileSize = 1024 * 1024;
	private static int _minCompress = 50;
	private static int _maxWidth = 1136;
	private static int _maxHeight = 640;
	private static Bitmap.CompressFormat _compressFormat = Bitmap.CompressFormat.JPEG;
	
	private static int _uploadid = 0;
	private static String _token = null;
	private static String _filePath = null;
	private static long _filetime = 0;
	
    static UploadManager _uploadManager = new UploadManager();
    private static OnActivityResultListener _onActivityResultListener = null;
    
    
    static void uploadCallback(final int uploadid, final int retCode, final String msg)
    {
    	
    	cleanUploadData();
    	Cocos2dxHelper.runOnGLThread(new Runnable() {
            @Override
            public void run() {
            	QiNiuUpdateCallback(uploadid, retCode, msg);
            }
        });
    }
    
    static void setUploadOptions(int maxFileSize, int minCompress, int maxWidth, int maxHeight, int compressFormat){
    	
    	_maxFileSize = maxFileSize;
    	_minCompress = minCompress;
    	_maxWidth = maxWidth;
    	_maxHeight = maxHeight;
    	
    	if(compressFormat == 0){
    		
    		_compressFormat = Bitmap.CompressFormat.JPEG;
    	}
    	else if(compressFormat == 1){
    		
    		_compressFormat = Bitmap.CompressFormat.PNG;
    	}
    }
    
    public static Bitmap capture(Activity activity) {
    	
        activity.getWindow().getDecorView().setDrawingCacheEnabled(true);
        Bitmap bmp = activity.getWindow().getDecorView().getDrawingCache();
        return bmp;
    }
   
    
    public static Bitmap getBitmapFromFile(String filePath) {
    
    	Bitmap bitmap = null;
    	BitmapFactory.Options op = null;
    	try {
    		op = getScaleOptions(filePath);
    		bitmap = BitmapFactory.decodeFile(filePath, op);
    	} 
    	catch (OutOfMemoryError e) {
    		
    		op.inSampleSize += op.inSampleSize;
    		bitmap = BitmapFactory.decodeFile(filePath, op);
    	}catch (Exception e) {
    		e.printStackTrace();

    	}
    	
    	return bitmap;
    }
    
    
    /**
     * 旋转图片
     * @param angle 被旋转角度
     * @param bitmap 图片对象
     * @return 旋转后的图片
     */
    public static Bitmap rotaingImageView(int angle, Bitmap bitmap) {
        Bitmap returnBm = null;
        // 根据旋转角度，生成旋转矩阵
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        try {
            // 将原始图片按照旋转矩阵进行旋转，并得到新的图片
            returnBm = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        } catch (OutOfMemoryError e) {
        }
        if (returnBm == null) {
            returnBm = bitmap;
        }
        if (bitmap != returnBm) {
            bitmap.recycle();
        }
        return returnBm;
    }

    
    /**
     * 读取照片旋转角度
     *
     * @param path 照片路径
     * @return 角度
     */
    public static int readPictureDegree(String path) {
        int degree = 0;
        try {
            ExifInterface exifInterface = new ExifInterface(path);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return degree;
    }

    public static byte[] getCompressDataFromBitmap(Bitmap bitmap) {
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int compress = 100;
        bitmap.compress(_compressFormat, 100, baos);//鐠愩劑鍣洪崢瀣級閺傝纭堕敍宀冪箹闁诧拷100鐞涖劎銇氭稉宥呭竾缂傗晪绱濋幎濠傚竾缂傗晛鎮楅惃鍕殶閹诡喖鐡ㄩ弨鎯у煂baos娑擄拷

  
        while (baos.toByteArray().length > _maxFileSize || compress > _minCompress) {  //瀵邦亞骞嗛崚銈嗘焽婵″倹鐏夐崢瀣級閸氬骸娴橀悧鍥ㄦЦ閸氾箑銇囨禍锟�100kb,婢堆傜艾缂佈呯敾閸樺缂�
            baos.reset();//闁插秶鐤哹aos閸楄櫕绔荤粚绡礱os
            compress -= 10;//濮ｅ繑顐奸柈钘夊櫤鐏忥拷10
            bitmap.compress(_compressFormat, compress, baos);//鏉╂瑩鍣烽崢瀣級options%閿涘本濡搁崢瀣級閸氬海娈戦弫鐗堝祦鐎涙ɑ鏂侀崚鐧癮os娑擄拷
        }
        
        
        
        return baos.toByteArray();
    }
    
    static void compressAndUploadImageFile(int uploadid,  String token, String filePath){
    	
	       if(filePath != null && filePath.length() > 0 && token != null && token.length()> 0 ){
	    	    
	    	   Bitmap bitmap = getBitmapFromFile(filePath);
	    	   if(bitmap != null){
	    		   
	    		   int degree = readPictureDegree(filePath);
	    		   bitmap = rotaingImageView(degree, bitmap);
	    		   
 		    	   byte[] imageData = getCompressDataFromBitmap(bitmap);
 		    	   uploadImageData(uploadid, imageData, token);       		    		   
	    	
	    	   }
	    	   else{
	    		   uploadCallback(uploadid, -1, "filepath error");
	    	   }
	       }
	       else{
	    	   
	    	   uploadCallback(uploadid, -1, "filepath error");
	       }
    	
    	
    }
    
    static void cleanUploadData() {
   		
    	_filetime = 0;
    	_uploadid = 0;
		_token = null;
		_filePath = null;
    	
    }
    
    
    static void startUploadImageFile(final int uploadid,  final String token, final int opType, final String filePath){
    	
    	if(_uploadid == 0 && _token == null & _filePath == null){
    		
    		_filetime = System.currentTimeMillis();
    		
    		try {
				JSONObject tokenData = new JSONObject(token);
	    		_uploadid = uploadid;
	    		_token = tokenData.getString("qiniuToken");
	    		_filePath = filePath;
	    		
	          	Cocos2dxHelper.getActivity().runOnUiThread(new Runnable(){

	    				@Override
	    				public void run() {
	    					
	    					uploadImageFile(uploadid, _token, opType, filePath);
	    				}
	            	});  					
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				Log.i("QiNiu", "aaaaa:" + token);
				uploadCallback(uploadid, -4, "json error");
			}
    		
	
    		
    	}
    	else{
    		
    		if(System.currentTimeMillis() - _filetime > 1000 * 15){
    			
    			cleanUploadData();
    		}
    		
    		QiNiuUpdateCallback(uploadid, -1, "wait a moment");
    	}
    	
 
    }
    
    public static byte[] toByteArray(String filename) throws IOException {

        File f = new File(filename);
        if (!f.exists()) {
            throw new FileNotFoundException(filename);
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream((int) f.length());
        BufferedInputStream in = null;
        try {
            in = new BufferedInputStream(new FileInputStream(f));
            int buf_size = 1024;
            byte[] buffer = new byte[buf_size];
            int len = 0;
            while (-1 != (len = in.read(buffer, 0, buf_size))) {
                bos.write(buffer, 0, len);
            }
            return bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            bos.close();
        }
    }
    
    
    static void uploadImageFile(final int uploadid,  String token, int opType, String filePath){
    	
    	switch(opType){
    	
	    	case 0:{
	    		
	    		
	    		try{
	    			
	    		
		    		if(filePath.endsWith("png") || filePath.endsWith("PNG") || filePath.endsWith("JPG") || filePath.endsWith("jpg") ||
		    				filePath.endsWith("jepg") || filePath.endsWith("JEPG"))
		    		{
		    			compressAndUploadImageFile(uploadid, token, filePath);
		    		}
		    		else{
		    			
	
		    			try {
							byte[] bytes =toByteArray(filePath);
							uploadImageData(uploadid, bytes, token);
							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							uploadCallback(uploadid, -2, "error");
						}
		    		}
	    		}
	    		catch(Exception e){
	    			
	    			uploadCallback(uploadid, -2, "nonsupport");
	    		}
	    		break;
	    	}
	    	case 1:{
	    		
	    		try{

	                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); //
	                
	                String mFilePath = Environment.getExternalStorageDirectory().getPath() + "/" + Cocos2dxHelper.getActivity().getPackageName();
	                File file=new File(mFilePath); 
	                if(!file.exists()){
	                	file.mkdir();
	                }
	                
	                mFilePath += "/" + "tmpphoto.png";
	                _filePath = mFilePath;
	                
	                Uri imageuri = Uri.fromFile(new File(mFilePath));
	                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageuri);
	                Cocos2dxHelper.getActivity().startActivityForResult(intent, TAKE_PHOTO);  
	    			
	    		}
	    		catch(Exception e){
	    			
	    			uploadCallback(uploadid, -2, "nonsupport");
	    			
	    			
	    		}

  		
	    		break;
	    	}   	
	    	case 2:{

	    		try{

	                Intent intent = new Intent(Intent.ACTION_PICK, null); //閻╃鍞�
	                intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");

	                Cocos2dxHelper.getActivity().startActivityForResult(intent, PICK_PHOTO);
	    		}
	    		catch(Exception e){
	    			
	    			uploadCallback(uploadid, -2, "nonsupport");
	    			
	    			
	    		}

	    		
	    		break;
	    	}    	
	    	case 3:{
	    		
	    		try{

	    			Bitmap bitmap = capture(Cocos2dxHelper.getActivity());
			    	   if(bitmap != null){
			    		   
		 		    	   byte[] imageData = getCompressDataFromBitmap(bitmap);
		 		    	   uploadImageData(uploadid, imageData, token);       		    		   
			    	
			    	   }
			    	   else{
			    		   uploadCallback(uploadid, -1, "filepath error");
			    	   }
	    			
	    		}
	    		catch(Exception e){
	    			
	    			
	    			uploadCallback(uploadid, -2, "nonsupport");
	    			
	    		}
	    		

	    		break;
	    	}       	
    	}
    	
        
        if(_onActivityResultListener == null){
        	

        	_onActivityResultListener = new OnActivityResultListener(){

    			@Override
    			public boolean onActivityResult(int requestCode, int resultCode,
    					Intent data) {
    				
    				Log.i("QiNiu", "onActivityResult : requestCode" + requestCode);
    				if(requestCode == PICK_PHOTO || requestCode == TAKE_PHOTO){
    					
    					if(resultCode == Activity.RESULT_OK){
    						
        	   				int uploadid = _uploadid;
        	   				Log.i("QiNiu", "onActivityResult : uploadid:" + uploadid);
            				String token = _token;
            				Log.i("QiNiu", "onActivityResult : token:" + token);
            				String imageFilePath = "";
            		       if(requestCode == PICK_PHOTO){
            		    	   
                  				//闁俺绻僽ri閸滃election閺夈儴骞忛崣鏍埂鐎圭偟娈戦崶鍓у鐠侯垰绶�
    	           		        Cursor cursor = Cocos2dxHelper.getActivity().getContentResolver().query(data.getData(),null,null,null,null);
    	           		        if (cursor!=null){
    	           		            if (cursor.moveToNext()){
    	           		            	imageFilePath=  cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
    	           		            }
    	           		            cursor.close();
    	           		        }       		    	   	   
            		       }
            		       else if(requestCode == TAKE_PHOTO){
            		    	   
            		    	   imageFilePath = _filePath;
            		   
            		       }
            		       Log.i("QiNiu", "onActivityResult : imageFilePath:" + imageFilePath);
            		       compressAndUploadImageFile(uploadid, token, imageFilePath);	
    						
    					}
    					else{
    						
    						uploadCallback(uploadid, -3, "cancel");
    						
    					}
    					
    				}	
    				return true;
    			}
        	};
        	
        	Cocos2dxHelper.addOnActivityResultListener(_onActivityResultListener);
        }
    	
    }

    public static BitmapFactory.Options getScaleOptions(String path) {
        
    	
    	BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        
        int height = options.outHeight;
        int width = options.outWidth;
        int heighScale = Math.max(1, (int)(height/640.0f));
        int widthScale = Math.max(1, (int)(width/1136.0f));
        int scale = Math.max(heighScale, widthScale);
        
        options.inSampleSize = scale;      //濮濄倝銆嶉崣锟� 閺佹澘褰叉禒銉︾壌閹诡噣娓跺Ч鍌濈箻鐞涘矁顓哥粻锟�
        options.inJustDecodeBounds = false;

        
        
        
        
        return options;
    }
    
    static void uploadImageData(final int uploadid, byte[] data, String token){
    	
    	Log.i("QiNiu", "uploadImageData++++");
    	HashMap<String, String> map = new HashMap<String, String>();
    	Log.i("QiNiu", "uploadImageData: uploadid" + uploadid);
    	if(data != null){
    		Log.i("QiNiu", "uploadImageData: data:" + data.length);
    	}
    	if(token != null){
    		Log.i("QiNiu", "uploadImageData: token:" + token);
    	}
    	map.put("x:phone", uploadid+"");
    	_uploadManager.put(data, null, token,
                new UpCompletionHandler() {
                    public void complete(String key,
                                         ResponseInfo info, JSONObject res) {
 
                    	Log.i("QiNiu", "key:"+key);
                    	Log.i("QiNiu", "info:"+info);
                    	cleanUploadData();
                        if(info.isOK()==true && res != null){
                            //textview.setText(res.toString());
                        	
                        	try {
                        		
                        		
                        		uploadCallback(uploadid, 0, res.getString("key"));
								
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
                        }
                        else{
                        	
                        	uploadCallback(uploadid, info.statusCode, info.error);
                        }
                    }
                },
                null);
    	
    	
    }

    
    
    

}
